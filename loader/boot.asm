[CPU 386]
[org 0x7C00]			; The BIOS loads the boot sector into 0:7C00h.
[bits 16]                    	; 16-bit operations.

	%define KERNEL_START_SECTOR 33
	%define KERNEL_SECTOR_LENGTH 250

Start:
	JMP	Entry
	db	00
BPB:
	incbin "fat12bpb.bin"

Entry:
	MOV	AX, 0
	CLI
	MOV	SS, AX
	MOV	SP, 07C00h
	STI

	XOR	AX, AX
	MOV	DS, AX
	MOV	ES, AX

	MOV	BX, 0
	MOV	DS, BX
	
	MOV	SI, strBootloader
	CALL	PrintString

	MOV	SI, strLoading
	CALL	PrintString

;;; Load a bunch of sectors into 0:7E00h.
	CALL	LoadFAT
	CALL	LoadKernel

	MOV	SI, strGoKernel
	CALL	PrintString
	
	JMP	0000:7E00h

	CALL	HaltSystem	
	JMP	$	; infinite loop

;;; ;;;;;;;;;;;;;;;;;;
;;; Halt routine.
HaltSystem:	
	MOV	SI, strHalting
	CALL	PrintString
	RET

;;;
;;;
LoadFAT:
;;; Load the first 2 sectors into memory at 0:500h.
	PUSHA

	MOV	AH, 02H
	MOV	AL, 32
	MOV	CH, 0
	MOV	CL, 1
	MOV DH, 0
	MOV	DL, 0
	MOV	BX, 0
	MOV	ES, BX
	MOV	BX, 0500h
	INT	13h

	POPA
	RET
	
LoadKernel:
;;; Load KERNEL_SECTOR_LENGTH sectors from CHS (0,0,34) into 0:7E00h.

	;; LBA -> CHS
	;; LBA = 33
	;; head = lba % (18 * 2) / 18; head = 1
	;; track = lba / (18 * 2); track = 0
	;; sector = lba / 18 + 1; sector = 16
	
	head equ 0x7A00
	track equ 0x7A02
	sector equ 0x7A04
	lba_num equ 0x7A10
	write equ 0x7A20

	PUSHA

	MOV	word [lba_num], KERNEL_START_SECTOR
	MOV	word [write], 0x7E0
	
.loop:
	MOV	AX, [lba_num]
	CALL	LBAtoCHS
	ADD word [write], 0x20
	ADD	word [lba_num], 1
	CMP	word [lba_num], KERNEL_START_SECTOR + KERNEL_SECTOR_LENGTH
	JLE	.loop
	
	POPA
	RET

LBAtoCHS:
;; Calculate head. head = (lba % (sectors_per_track * 2)) / sectors_per_track
	MOV	AX, [lba_num]
	MOV	BX, 36
	DIV	BL			; DX = lba modulo 36
	MOV	AL, AH		; AX = modulo
	MOV	AH, 0
	MOV	BX, 18		; BX = sectors per track
	DIV 	BL		; AX = heads
	MOV	[head], AL

;; Calculate track: lba / (sectors_per_track * 2)
	MOV	AX, [lba_num]
	MOV	BX, 36		; BX = sectors per track * 2
	DIV	BL		; AX = (AX / BX)
	MOV	[track], AL	; AX = lba / (18 * 2)

;; Calculate sector: (lba % sectors_per_track + 1)
	MOV	AX, [lba_num]
	MOV	BX, 18
	DIV	BL		; DX = lba modulo 18
	ADD	AH, 1
	MOV	[sector], AH	; and store the remainder

;; Read sector
	MOV	AH, 2
	MOV	AL, 1
	MOV	CH, [track]
	MOV	CL, [sector]
	MOV DH, [head]
	MOV	DL, 0
	MOV	BX, [write]	; 07E0:0000h
	MOV	ES, BX
	MOV	BX, 0	; offset is (0200h * sector)
	INT	13h
	
	RET

	
PrintString:
	;; String address is [DS:SI]

.next:
	MOV	AL, [SI] 	; Get next character, store in AL

	INC	SI		; Increment SI
	OR	AL,AL		; Is AL a NUL?
	JZ	.done		; yes, done
	CALL	PrintChar	; no, print AL
	JMP	.next		; continue
	
.done:
	RET
	
PrintChar:
	MOV	AH, 0Eh
	MOV	BH, 0
	MOV	BL, 07h

	INT	10h

	RET

;;; Strings
	strBootloader	db "Procyon x86 Bootloader",13,10,0
	strLoading	db "Loading from disk. Please wait...",13,10,0
	strGoKernel	db "Stage 1: jumping to 07E00h.",13,10,0
	strHalting	db "Halting system.",13,10,0
	
;;; ;;;;;
;;; Magic.
;;; 
;; Pad to 512 bytes, add the magic number.
times 510 - ($ - $$) db 0
dw 0xAA55

FATRoot:
	incbin "fat12root.bin"
