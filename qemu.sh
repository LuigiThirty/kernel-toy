#!/bin/bash

if [[ "$OSTYPE" == "darwin"* ]]; then
	qemu-system-i386 -S -gdb tcp::9000 -fda procyon_1.44m.img -vga cirrus -cpu 486
else
	qemu-system-i386 -S -gdb tcp::9000 -fda procyon_1.44m.img -vga cirrus -cpu 486 -serial COM4
fi
