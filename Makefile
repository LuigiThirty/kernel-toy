all: procyon.dsk

.PHONY: all clean libc libcpp loader kernel elf-programs libprocyon

###

procyon.dsk: libc libcpp libprocyon loader kernel elf-programs
	cp ./blank_1.44m.img ./procyon_1.44m.img
	dd if=./loader/boot.bin of=./procyon_1.44m.img conv=notrunc

ifeq ($(OS),Windows_NT)
	./imgtool.exe put pc_dsk_fat procyon_1.44m.img kernel\stage2.bin STAGE2.BIN
	./imgtool.exe put pc_dsk_fat procyon_1.44m.img kernel\kernel.bin KERNEL.BIN
	./imgtool.exe put pc_dsk_fat procyon_1.44m.img elf-programs\test.elf TEST.ELF
	# ./imgtool.exe mkdir pc_dsk_fat procyon_1.44m.img JUNK
	# ./imgtool.exe put pc_dsk_fat procyon_1.44m.img kernel\readme.txt JUNK\README.TXT
else
	mcopy -i ./procyon_1.44m.img ./kernel/stage2.bin ::/
	mcopy -i ./procyon_1.44m.img ./kernel/kernel.bin ::/
	mcopy -i ./procyon_1.44m.img ./kernel/readme.txt ::/
	mcopy -i ./procyon_1.44m.img ./elf-programs/test.elf ::/

endif

libc:
	make -C ./libc

libcpp:
	make -C ./libcpp

libprocyon:
	make -C ./libprocyon

loader:
	make -C ./loader

kernel:
	make -C ./kernel

elf-programs:
	make -C ./elf-programs

clean:
	-make -C ./libc clean
	-make -C ./libcpp clean
	-make -C ./loader clean
	-make -C ./kernel clean
	-make -C ./libprocyon clean
	-rm procyon_1.44m.img
