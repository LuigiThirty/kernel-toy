#pragma once

#define SYSCALL_write   1
#define SYSCALL_read    2
#define SYSCALL_dlopen  3
#define SYSCALL_dlsym   4
#define SYSCALL_getcwd  5

#define SYSCALL_disable 6   /* interrupts off */
#define SYSCALL_enable  7   /* interrupts on  */

#define SYSCALL_fnaddr  8   /* get function address */