#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "mmio.h"
#include "types.h"
#include "scancode.h"
#include "memregions.h"

/* Parameter block that holds return values for syscalls so we can stay
    CPU-independent. */
typedef struct 
{
    uint32_t retval;
} SYSCALL_RETURN_BLOCK;

typedef struct 
{
    uint32_t param1, param2, param3;
} SYSCALL_PARAMS_BLOCK;

extern volatile SYSCALL_RETURN_BLOCK *syscall_return_block;
extern volatile SYSCALL_PARAMS_BLOCK *syscall_params_block;