#pragma once

typedef struct {
  void (*PutString)(char *str) = 0;
} ITextIO;
