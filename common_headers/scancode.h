#pragma once

/*
The scancode values come from:
- http://download.microsoft.com/download/1/6/1/161ba512-40e2-4cc9-843a-923143f3456c/scancode.doc (March 16, 2000).
- http://www.computer-engineering.org/ps2keyboard/scancodes1.html
- using MapVirtualKeyEx( VK_*, MAPVK_VK_TO_VSC_EX, 0 ) with the english us keyboard layout
- reading win32 WM_INPUT keyboard messages.
*/

enum Scancode {

    SC_escape = 0x01,
    SC_1 = 0x02,
    SC_2 = 0x03,
    SC_3 = 0x04,
    SC_4 = 0x05,
    SC_5 = 0x06,
    SC_6 = 0x07,
    SC_7 = 0x08,
    SC_8 = 0x09,
    SC_9 = 0x0A,
    SC_0 = 0x0B,
    SC_minus = 0x0C,
    SC_equals = 0x0D,
    SC_backspace = 0x0E,
    SC_tab = 0x0F,
    SC_q = 0x10,
    SC_w = 0x11,
    SC_e = 0x12,
    SC_r = 0x13,
    SC_t = 0x14,
    SC_y = 0x15,
    SC_u = 0x16,
    SC_i = 0x17,
    SC_o = 0x18,
    SC_p = 0x19,
    SC_bracketLeft = 0x1A,
    SC_bracketRight = 0x1B,
    SC_enter = 0x1C,
    SC_controlLeft = 0x1D,
    SC_a = 0x1E,
    SC_s =0x1F,
    SC_d = 0x20,
    SC_f = 0x21,
    SC_g = 0x22,
    SC_h = 0x23,
    SC_j = 0x24,
    SC_k = 0x25,
    SC_l = 0x26,
    SC_semicolon = 0x27,
    SC_apostrophe = 0x28,
    SC_grave = 0x29,
    SC_shiftLeft = 0x2A,
    SC_backslash = 0x2B,
    SC_z = 0x2C,
    SC_x = 0x2D,
    SC_c = 0x2E,
    SC_v = 0x2F,
    SC_b = 0x30,
    SC_n = 0x31,
    SC_m = 0x32,
    SC_comma = 0x33,
    SC_preiod = 0x34,
    SC_slash = 0x35,
    SC_shiftRight = 0x36,
    SC_numpad_multiply = 0x37,
    SC_altLeft = 0x38,
    SC_space = 0x39,
    SC_capsLock = 0x3A,
    SC_f1 = 0x3B,
    SC_f2 = 0x3C,
    SC_f3 = 0x3D,
    SC_f4 = 0x3E,
    SC_f5 = 0x3F,
    SC_f6 = 0x40,
    SC_f7 = 0x41,
    SC_f8 = 0x42,
    SC_f9 = 0x43,
    SC_f10 = 0x44,
    SC_numLock = 0x45,
    SC_scrollLock = 0x46,
    SC_numpad_7 = 0x47,
    SC_numpad_8 = 0x48,
    SC_numpad_9 = 0x49,
    SC_numpad_minus = 0x4A,
    SC_numpad_4 = 0x4B,
    SC_numpad_5 = 0x4C,
    SC_numpad_6 = 0x4D,
    SC_numpad_plus = 0x4E,
    SC_numpad_1 = 0x4F,
    SC_numpad_2 = 0x50,
    SC_numpad_3 = 0x51,
    SC_numpad_0 = 0x52,
    SC_numpad_period = 0x53,
    SC_alt_printScreen = 0x54, /* Alt + print screen. MapVirtualKeyEx( VK_SNAPSHOT, MAPVK_VK_TO_VSC_EX, 0 ) returns scancode 0x54. */
    SC_bracketAngle = 0x56, /* Key between the left shift and Z. */
    SC_f11 = 0x57,
    SC_f12 = 0x58,
    SC_oem_1 = 0x5a, /* VK_OEM_WSCTRL */
    SC_oem_2 = 0x5b, /* VK_OEM_FINISH */
    SC_oem_3 = 0x5c, /* VK_OEM_JUMP */
    SC_eraseEOF = 0x5d,
    SC_oem_4 = 0x5e, /* VK_OEM_BACKTAB */
    SC_oem_5 = 0x5f, /* VK_OEM_AUTO */
    SC_zoom = 0x62,
    SC_help = 0x63,
    SC_f13 = 0x64,
    SC_f14 = 0x65,
    SC_f15 = 0x66,
    SC_f16 = 0x67,
    SC_f17 = 0x68,
    SC_f18 = 0x69,
    SC_f19 = 0x6a,
    SC_f20 = 0x6b,
    SC_f21 = 0x6c,
    SC_f22 = 0x6d,
    SC_f23 = 0x6e,
    SC_oem_6 = 0x6f, /* VK_OEM_PA3 */
    SC_katakana = 0x70,
    SC_oem_7 = 0x71, /* VK_OEM_RESET */
    SC_f24 = 0x76,
    SC_sbcschar = 0x77,
    SC_convert = 0x79,
    SC_nonconvert = 0x7B, /* VK_OEM_PA1 */

    SC_media_previous = 0xE010,
    SC_media_next = 0xE019,
    SC_numpad_enter = 0xE01C,
    SC_controlRight = 0xE01D,
    SC_volume_mute = 0xE020,
    SC_launch_app2 = 0xE021,
    SC_media_play = 0xE022,
    SC_media_stop = 0xE024,
    SC_volume_down = 0xE02E,
    SC_volume_up = 0xE030,
    SC_browser_home = 0xE032,
    SC_numpad_divide = 0xE035,
    SC_printScreen = 0xE037,
    /*
    SC_printScreen:
    - make: 0xE02A 0xE037
    - break: 0xE0B7 0xE0AA
    - MapVirtualKeyEx( VK_SNAPSHOT, MAPVK_VK_TO_VSC_EX, 0 ) returns scancode 0x54;
    - There is no VK_KEYDOWN with VK_SNAPSHOT.
    */
    SC_altRight = 0xE038,
    SC_cancel = 0xE046, /* CTRL + Pause */
    SC_home = 0xE047,
    SC_arrowUp = 0xE048,
    SC_pageUp = 0xE049,
    SC_arrowLeft = 0xE04B,
    SC_arrowRight = 0xE04D,
    SC_end = 0xE04F,
    SC_arrowDown = 0xE050,
    SC_pageDown = 0xE051,
    SC_insert = 0xE052,
    SC_delete = 0xE053,
    SC_metaLeft = 0xE05B,
    SC_metaRight = 0xE05C,
    SC_application = 0xE05D,
    SC_power = 0xE05E,
    SC_sleep = 0xE05F,
    SC_wake = 0xE063,
    SC_browser_search = 0xE065,
    SC_browser_favorites = 0xE066,
    SC_browser_refresh = 0xE067,
    SC_browser_stop = 0xE068,
    SC_browser_forward = 0xE069,
    SC_browser_back = 0xE06A,
    SC_launch_app1 = 0xE06B,
    SC_launch_email = 0xE06C,
    SC_launch_media = 0xE06D,

    SC_pause = 0xE11D45,
    /*
    SC_pause:
    - make: 0xE11D 45 0xE19D C5
    - make in raw input: 0xE11D 0x45
    - break: none
    - No repeat when you hold the key down
    - There are no break so I don't know how the key down/up is expected to work. Raw input sends "keydown" and "keyup" messages, and it appears that the keyup message is sent directly after the keydown message (you can't hold the key down) so depending on when GetMessage or PeekMessage will return messages, you may get both a keydown and keyup message "at the same time". If you use VK messages most of the time you only get keydown messages, but some times you get keyup messages too.
    - when pressed at the same time as one or both control keys, generates a 0xE046 (SC_cancel) and the string for that scancode is "break".
    */
};
