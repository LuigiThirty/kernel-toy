#pragma once

typedef enum {
  H_CURHEAP = 0x01,
  H_SYSHEAP = 0x02,
  H_CLEAR   = 0x04,
  H_XMS     = 0x08,
} MEMMGR_MALLOC_FLAGS;