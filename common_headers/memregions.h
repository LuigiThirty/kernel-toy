#pragma once

/* Conventional memory regions that are predefined. */
/* On a 640K system, 0x500 to 0x7FFFF is available for use. */

#define BPB_AREA                0x500

#define FDD_SECTOR_AREA         0x1000
#define FDD_SECTOR_AREA_END     0x11FF
#define FDD_SECTOR_AREA_SIZE    (FDD_SECTOR_AREA_END - FDD_SECTOR_AREA + 1)

#define SYSCALL_AREA            0x2000
#define SYSCALL_PARAMS_AREA     0x2200

#define PROCESS_HANDLE_AREA     0x3000
#define PROCESS_HANDLE_AREA_END 0x4000

#define V86_TSS_AREA            0x7E00
#define KERNEL_TSS_AREA         0x7F00

#define PROCESS_LIST_AREA       0x8000

#define VM86_CODE_AREA          0x10000
#define VM86_CODE_AREA_END      0x10FFF
#define VM86_CODE_AREA_SIZE     (VM86_CODE_AREA_END - VM86_CODE_AREA + 1)
#define VM86_CODE_SEGMENT       (VM86_CODE_AREA >> 4)

#define VM86_STACK_AREA         0x12000
#define VM86_STACK_AREA_END     0x12FFF
#define VM86_STACK_AREA_SIZE    (VM86_STACK_AREA_END - VM86_STACK_AREA + 1)
#define VM86_STACK_SEGMENT      (VM86_STACK_AREA >> 4)

#define VGA_TEXT_AREA           0xB8000
#define VGA_TEXT_AREA_END       0xBFFFF
#define VGA_TEXT_AREA_SIZE      (VGA_TEXT_AREA_END - VGA_TEXT_AREA + 1)
