/* Kernel function prototypes */
#pragma once

#include <procyon.h>
#include <types/memmgr.h>

void (*TestFunc)(void) = 0;

CPTR (*NewPtr)(uint32_t, MEMMGR_MALLOC_FLAGS) = 0;
int (*DisposePtr)(CPTR) = 0;