#!/bin/bash

if [[ "$OSTYPE" == "darwin"* ]]; then
	i386-elf-gdb -x kernel-win.gdb
else
	winpty gdb -x kernel-win.gdb
fi
