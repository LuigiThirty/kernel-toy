#pragma once

#include <procyon.h>
#include "pic.h"

#ifdef __cplusplus
extern "C" {
#endif

#define PIT_TICKS_PER_SECOND 18
volatile extern uint32_t pit_ticks;

void _PIT_IRQHandler();

uint32_t PIT_GetTicks();
void PIT_WaitTicks(uint16_t period);

#ifdef __cplusplus
}
#endif