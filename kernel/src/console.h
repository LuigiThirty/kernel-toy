#pragma once

#include "globals.h"
#include "vga/vgatext.h"
#include "svgaconsole.h"

class Console
{
    public:
    static void Putchar(uint8_t c);
};