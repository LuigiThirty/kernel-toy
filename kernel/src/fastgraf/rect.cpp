#include "rect.h"
#include "fastgraf.h"

void FG_SetRect(FG_Rect *rect, int16_t x, int16_t y, int16_t w, int16_t h)
{
    rect->x = x;
    rect->y = y;
    rect->w = w;
    rect->h = h;
}

void FG_FrameRect(FG_Rect *rect)
{
    uint32_t topleft = rect->x + (VESAMode5F_info.width * rect->y);
    uint32_t bottomleft = rect->x + (VESAMode5F_info.width * (rect->y + rect->h));

    for(int i=topleft; i<(topleft + rect->w); i++)
    {
        MMIO8(fastgraf_state.outputBitmap.pixels + i) = fastgraf_state.forePenColor;
    }
    
    for(int i=bottomleft; i<(bottomleft + rect->w); i++)
    {
        MMIO8(fastgraf_state.outputBitmap.pixels + i) = fastgraf_state.forePenColor;
    }

    for(int i = rect->y; i < rect->y+rect->h; i++)
    {
        uint32_t offset = rect->x + (VESAMode5F_info.width * i);
        MMIO8(fastgraf_state.outputBitmap.pixels + offset) = fastgraf_state.forePenColor;

        offset = rect->x + rect->w - 1 + (VESAMode5F_info.width * i);
        MMIO8(fastgraf_state.outputBitmap.pixels + offset) = fastgraf_state.forePenColor;
    }

}

void FG_PaintRect(FG_Rect *rect)
{
    for(int y = rect->y; y < rect->y + rect->h; y++)
    {
        for(int x = rect->x; x < (rect->x + rect->w - 1); x++)
        {
            uint32_t offset = (y * VESAMode5F_info.width) + x;
            MMIO8(fastgraf_state.outputBitmap.pixels + offset) = fastgraf_state.forePenColor;
        }
    }
}