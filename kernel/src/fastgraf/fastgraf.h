#pragma once

#include <procyon.h>
#include <stdio.h>

#include "fg_types.h"
#include "rect.h"
#include "blit.h"

#include "svga.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    FG_Color forePenColor;
    FG_Color backPenColor;
    FG_Bitmap outputBitmap;
} FG_State;
extern FG_State fastgraf_state;

void FG_SetForePenColor(FG_Color color);
void FG_SetBackPenColor(FG_Color color);

void FG_SetBitmap(FG_Bitmap *bitmap, uint16_t width, uint16_t height, Pixel *pixels);

#ifdef __cplusplus
}
#endif