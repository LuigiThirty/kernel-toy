#include "fastgraf.h"

FG_State fastgraf_state;

void FG_SetForePenColor(FG_Color color) { fastgraf_state.forePenColor = color; }
void FG_SetBackPenColor(FG_Color color) { fastgraf_state.backPenColor = color; }

void FG_SetBitmap(FG_Bitmap *bitmap, uint16_t width, uint16_t height, Pixel *pixels)
{
    bitmap->height = height;
    bitmap->width = width;
    bitmap->pixels = pixels;
};