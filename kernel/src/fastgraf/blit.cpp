#include "blit.h"
#include "fastgraf.h"

void FG_BlitTextString(FG_Bitmap *destination, FG_Point destCoords, FG_Font *font, uint8_t *str)
{
    uint8_t len = strlen(str);
    uint16_t current_x = destCoords.x;

    for(int i=0; i<len; i++)
    {
        FG_BlitFont(destination, POINT(current_x, destCoords.y), font, str[i]);
        current_x += 8;
    }
}

void FG_BlitFont(FG_Bitmap *destination, FG_Point destCoords, FG_Font *font, uint8_t c)
{
    uint32_t offset;
    bool pixel_value;

    int glyph_x = 0;
    int glyph_y = 0;

    FG_Color textColor = fastgraf_state.forePenColor;

    for(int y = destCoords.y; y < destCoords.y + font->glyph_height; y++)
    {
        glyph_x = 0;
        for(int x = destCoords.x; x < destCoords.x + font->glyph_width; x++)
        {
            offset = (y * destination->width) + x;
            pixel_value = (font->characters[(16*c) + glyph_y] & (128 >> glyph_x)) > 0 ? 1 : 0;

            if(pixel_value)
            {
                MMIO8(destination->pixels + offset) = textColor;
            }

            glyph_x++;
        }

        glyph_y++;
    }
}

void FG_Blit(FG_Bitmap *destination, FG_Point destCoords, FG_Bitmap *source, FG_Point sourceCoords, FG_Point blitSize, FG_RasterOp rasterOp, FG_BlitFlags flags)
{
    uint32_t topleft, bottomleft, offset, source_offset;
    int16_t source_x, source_y;
    uint8_t source_pixel;

    switch(rasterOp)
    {
        case ROP_CLEAR:
            // Blit a rectangle of color 15 to destination of size blitSize.
            for(int y = destCoords.y; y < destCoords.y + blitSize.y; y++)
            {
                for(int x = destCoords.x; x < (destCoords.x + blitSize.x - 1); x++)
                {
                    offset = (y * destination->width) + x;
                    MMIO8(destination->pixels + offset) = fastgraf_state.backPenColor;
                }
            }
        break;
        case ROP_COPY:
            // Copy from the source bitmap to the destination bitmap.
            source_offset = (sourceCoords.x + (sourceCoords.y * source->width));

            for(int y = destCoords.y; y < destCoords.y + blitSize.y; y++)
            {
                source_x = 0;
                for(int x = destCoords.x; x < (destCoords.x + blitSize.x - 1); x++)
                {
                    offset = (y * destination->width) + x;
                    source_pixel = MMIO8(source->pixels + source_x + (source->width * source_y));

                    if(source_pixel == 0 && !(flags & BF_TRANSPARENT))
                    {
                        MMIO8(destination->pixels + offset) = MMIO8(source->pixels + source_x + (source->width * source_y));
                    }
                    else if(source_pixel == 0 && (flags & BF_TRANSPARENT))
                    {
                        
                    }
                    else
                    {
                        MMIO8(destination->pixels + offset) = MMIO8(source->pixels + source_x + (source->width * source_y));
                    }
                    source_x++;
                }
                source_y++;
            }
        break;
    }
}