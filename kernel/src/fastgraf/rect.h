#pragma once

#include "fg_types.h"

#ifdef __cplusplus
extern "C" {
#endif

void FG_FrameRect(FG_Rect *rect);
void FG_SetRect(FG_Rect *rect, int16_t x, int16_t y, int16_t w, int16_t h);
void FG_PaintRect(FG_Rect *rect);

#ifdef __cplusplus
}
#endif