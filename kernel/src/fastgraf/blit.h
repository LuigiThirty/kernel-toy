#pragma once

#include <procyon.h>
#include <string.h>

#include "svga.h"
#include "fg_types.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {  
    ROP_CLEAR,  // D = 0
    ROP_COPY,   // D = S
    ROP_AND,    // D = D & S
} FG_RasterOp;

typedef enum {
    BF_NONE = 0,
    BF_TRANSPARENT = 1,
} FG_BlitFlags;

void FG_Blit(FG_Bitmap *destination, FG_Point destCoords, FG_Bitmap *source, FG_Point sourceCoords, FG_Point blitSize, FG_RasterOp rasterOp, FG_BlitFlags flags);
void FG_BlitFont(FG_Bitmap *destination, FG_Point destCoords, FG_Font *font, uint8_t c);
void FG_BlitTextString(FG_Bitmap *destination, FG_Point destCoords, FG_Font *font, uint8_t *str);

#ifdef __cplusplus
}
#endif