#pragma once

#include <procyon.h>

typedef struct {
    int16_t x, y;
} FG_Point;
#define POINT(X,Y) ((FG_Point){X, Y})

typedef struct {
    int16_t x, y, w, h;
} FG_Rect;

typedef uint8_t FG_Color;
typedef uint8_t Pixel;

typedef struct {
    // A chunky bitmap.
    int16_t width;
    int16_t height;
    uint8_t *pixels;
} FG_Bitmap;

typedef struct {
    uint16_t glyph_width;
    uint16_t glyph_height;
    uint8_t *characters;
} FG_Font;

#define FB_DIRECT (uint8_t *)VESAMode5F_info.framebuffer
#define FB_DISPLAYMEM_BASE 0
