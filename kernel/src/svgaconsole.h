#include <procyon.h>
#include <stdlib.h>

#include "memmgr.h"
#include "fastgraf/fastgraf.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    uint8_t cursor_X;
    uint8_t cursor_Y;
    uint8_t *text_buffer;
    FG_Font *font;
    FG_Bitmap *backing_bitmap;

    uint8_t cursor_max_X;
    uint8_t cursor_max_Y;
} SCON_State;

void SCON_Init(SCON_State *state, FG_Font *font, FG_Bitmap *backing);
void SCON_WriteString(SCON_State *state, CString str);
void SCON_Redraw(SCON_State *state);
void SCON_TeletypeWrite(SCON_State *state, char c);
inline void SCON_AdvanceCursor(SCON_State *state)
{
    state->cursor_X += 1;
    
    if(state->cursor_X == state->cursor_max_X)
    {
        state->cursor_X = 0;
        state->cursor_Y += 1;
    }
}

extern SCON_State svga_console_state;

#ifdef __cplusplus
}
#endif