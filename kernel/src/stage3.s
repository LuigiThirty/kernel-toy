[CPU 386]
[BITS 32]

extern _PModeStart

global _PModeEntry
global _Font8x16
	
;; Relocate kernel to 0x100000 now that we're in protected mode.
_PModeEntry:
		
PModeEntry:	
	mov	EAX, 018h
	LTR	AX			; Set task selector to 18h

LoadIDT:
	LIDT	[IDT_Info]	; IDT loaded into IDTR
	CALL	_PModeStart
	
StopSystem:
	CLI
	HLT

	%include "src/arch/i386/idt_a.s"
