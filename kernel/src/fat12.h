#pragma once

#include <stdio.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "messages.h"
#include "memmgr.h"
#include "fileio.h"

#define SECTOR_LENGTH 512

namespace FAT12 {

  #pragma pack (push, 1)
    typedef struct {
      uint16_t bytes_per_sector;
      uint8_t sectors_per_cluster;
      uint16_t reserved_sectors;
      uint8_t fat_copies;
      uint16_t root_directory_entries;
      uint16_t total_sectors;
      uint8_t media_descriptor_type;
      uint16_t sectors_per_fat;
      uint16_t sectors_per_track;
      uint16_t heads;
      uint16_t hidden_sectors;
      uint8_t extended_fields_present;
      char volume_label[12];
      char filesystem_type[12]; //reported by the BPB, not actual fs
    } BPB;

    typedef struct {
      char allocation_or_first_letter;
      char filename[12]; // not the filename on the disk! a . was inserted
      uint8_t attributes;
      uint8_t reserved;
      uint8_t creation_time_tenths;
      uint16_t creation_time;
      uint16_t creation_date;
      uint16_t access_date;
      uint16_t first_cluster_high; /* FAT32 only */
      uint16_t modified_time;
      uint16_t modified_date;
      uint16_t first_cluster_low;
      uint32_t file_size;
    } FAT_ROOT_DIRECTORY_ENTRY;

    // TODO: Do we need both of these? They're slightly different.
    typedef struct {
      uint8_t filename[9];
      uint8_t extension[4];
      uint8_t attributes;
      uint8_t reserved;
      uint8_t create_time_fine;
      uint16_t create_time;
      uint16_t create_date;
      uint16_t last_access_date;
      uint16_t ea_index;
      uint16_t last_modified_time;
      uint16_t last_modified_date;
      uint16_t first_cluster;
      uint32_t size_bytes;
    } RootDirectoryEntry;
    #pragma pop (pack)

    /* file flags */
    typedef enum {
      FILE_FLAG_UNUSED = 0x00,
      FILE_FLAG_READ   = 0x01,
      FILE_FLAG_WRITE  = 0x02,
      FILE_FLAG_READWRITE = 0x03,
    } FileFlags;

    typedef struct {
      uint16_t id;
      FileFlags flags;
      FAT_ROOT_DIRECTORY_ENTRY *root_dir_entry;
      char path[128];
    } FileDescriptor;

    typedef enum {
    FILE_ATTR_READONLY    = 0x01,
    FILE_ATTR_HIDDEN      = 0x02,
    FILE_ATTR_SYSTEM      = 0x04,
    FILE_ATTR_VOLUMELABEL = 0x08,
    FILE_ATTR_DIRECTORY   = 0x10,
    FILE_ATTR_ARCHIVE     = 0x20, 
    // 0x40 and 0x80 are unused
    FILE_ATTR_LFN         = 0x0F, // special
  } FileAttributes;
};

class FAT12Manager {
  public:
    FAT12::FileDescriptor file_descriptor_table[FILES_LIMIT];
    FAT12::BPB *drive_bpb[DRIVES_LIMIT];
    FAT12::FAT_ROOT_DIRECTORY_ENTRY *drive_root_dir[DRIVES_LIMIT];
    uint16_t *drive_fat[DRIVES_LIMIT];

    void Initialize();
    void ReadBPB(DRIVE_LETTER drive, uint8_t *data);
    void UpdateBPB(DRIVE_LETTER drive);
    void PrintBPBInfo(DRIVE_LETTER letter);

    void GetRootDirectorySector(uint8_t *buffer, DRIVE_LETTER letter);
    void GetDirectorySector(uint8_t *buffer, DRIVE_LETTER letter, char *path);
    uint32_t GetRootDirectoryOffset(DRIVE_LETTER letter);
    uint16_t BytesPerSector(DRIVE_LETTER letter);

    bool ReadCluster(uint8_t *buffer, DRIVE_LETTER letter, int cluster_number);
    uint16_t FindNextClusterOfFile(DRIVE_LETTER letter, uint16_t cluster_number);
    uint16_t FileClustersRemaining(DRIVE_LETTER letter, uint16_t starting_cluster);

    void GetDirectoryEntryWithIndex(FAT12::RootDirectoryEntry *entry, int index, uint8_t *directory_data);
    FAT12::RootDirectoryEntry *GetDirectoryEntryWithFilename(DRIVE_LETTER letter, char *filename);
};
extern FAT12Manager gFAT12Manager;