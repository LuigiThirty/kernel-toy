#ifndef KEYBOARD_H
#define KEYBOARD_H

#pragma once

#include "procyon.h"
#include "low_level_io.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

extern "C"
{
    void _KB_IRQHandler();
}

#endif
