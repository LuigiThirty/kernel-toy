#include "globals.h"

DRIVE_LETTER current_drive;
//uint8_t current_path[256];
uint8_t svga_console_active;

void ConstructPathString(char *output)
{
    char path[512];
    char current_path[256];
    __SYS_getcwd(current_path, 256);
    sprintf(path, "%c:%s", DRIVE_INDEX_TO_DRIVE_LETTER(current_drive), current_path);
    strcpy(output, path);
}

void DoSplashScreen()
{
  printf("*** Procyon-386 Protected Mode Kernel ***\n\n");
  printf("Built on %s using GCC %s\n\n", __TIMESTAMP__, __VERSION__);
}
