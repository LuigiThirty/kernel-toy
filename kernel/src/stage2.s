; Stage 2 bootloader.
; Set up protected mode and enter.

[CPU 386]
[BITS 16]

	SECTION	.text

global _start

_start:
KernelMain:
	CALL	PrintNL
	CALL	PrintNL

	MOV	SI, strBanner
	CALL	PrintString

	CALL	EnumerateHardware

	MOV	SI, strProtectedModeEntry
	CALL	PrintString

	; Go to protected mode!
	CALL	EnableA20
	
	; Install the GDT and IDT
	CLI			; interrupts off
	LGDT	[toc] 		; GDT loaded into GDTR
	LIDT	[idt_48]	; dummy IDT
	STI

	; And now go to protected mode.
	CLI
	MOV	EAX, CR0
	OR	EAX, 1
	MOV	CR0, EAX
	
	JMP	08h:PModeSetup

	[BITS 16]
	
;;; Enable A20 line.
EnableA20:
	MOV	AX, 02401h
	INT	15h

	; Compare FFFF:7E0E to 0xAA55
	MOV BX, 0FFFFh
	MOV FS, BX
	MOV	BX, 0x7E0E
	MOV	AX, [FS:BX]
	CMP	AX, 0xAA55
	JNE	.done	; If FFFF:7E0E == AA55, A20 gate is still off.

	MOV	SI, strNoA20BIOS
	CALL	PrintString

	; That didn't work. Try it a different way.
	cli

	CALL	ReadyFor8042Command
	MOV		AL, 0xAD
	OUT		0x64, AL

	CALL	ReadyFor8042Command
	MOV		AL, 0xD0
	OUT		0x64, AL

	call    ReadyFor8042Data
    in      al,	0x60            
    push    eax     

    call    ReadyFor8042Command  ; When controller is ready for command
    mov     al,	0xD1            ; Set command 0xd1 (write to output)
    out     0x64, al            

    call    ReadyFor8042Command  ; When controller is ready for command
    pop     eax                ; Write input back, with bit #2 set
    or      al, 2
    out     0x60, al

    call    ReadyFor8042Command  ; When controller is ready for command
    mov     al, 0xAE            ; Write command 0xae (enable keyboard)
    out     0x64, al

    call    ReadyFor8042Command  ; Wait until controller is ready for command
    
	sti

	; Compare FFFF:7E0E to 0xAA55
	MOV BX, 0FFFFh
	MOV FS, BX
	MOV	BX, 0x7E0E
	MOV	AX, [FS:BX]
	CMP	AX, 0xAA55
	JNE	.done	; If FFFF:7E0E == AA55, A20 gate is still off.
	
	MOV SI, strA20StillOff
	CALL	PrintString
	CALL	HaltSystem

.done:
	RET

ReadyFor8042Command:
	CALL	IODelay
	IN		AL, 0x64
	TEST	AL, 2
	JNZ	ReadyFor8042Command
	RET

ReadyFor8042Data:
	CALL	IODelay
	in      al,0x64
  	test    al,1
  	jz      ReadyFor8042Data
  	ret

IODelay: 			; Dummy delay function.
	PUSH	AX
	MOV		AX, 0
	OUT		0x80, AL
	POP		AX
	RET
	
;;; ;;;;;;;;;;;;;;;;;;
;;; Halt routine.
HaltSystem:
	MOV	SI, strCRLF
	CALL	PrintString

	MOV     SI, strHalting
	CALL    PrintString

.loop:
	JMP		.loop

	RET

;;; ;;;;;;;;;;;
EnumerateHardware:
	PUSHA

	MOV	SI, strEnumHW
	CALL	PrintString

.EnumConventionalMemory:
	INT	012h 		; get conventional memory

	MOV	ECX, memKbAvailable
	MOV	[ECX], EAX
	MOV	BX, 10
	CALL	PrintNumberWithBase

	MOV	SI, strEnumMemory
	CALL	PrintString
	
	POPA
	RET
	
;;;;;;;;
	%include "asminc/strings.s"
	
	SECTION .text
	
strHalting 		db "Kernel is halting",13,10,0
strBanner 		db "*** Procyon-386 ", __DATE__, " ***",13,10,0
strCRLF			db 13,10,0	
strProtectedModeEntry db "Entering protected mode...",13,10,0	
strNoA20BIOS 	db "BIOS doesn't support AH=2401 to enable Gate A20. Trying alternate method.",13,10,0 
strA20StillOff	db "Gate A20 is still disabled. Halting.",13,10,0
	
	; hardware enumeration
strEnumHW db "Enumerating hardware...",13,10,0	
strEnumMemory db "KB conventional memory available",13,10,0
	
	; hardware info
memKbAvailable dd 0

	; GDT
GDTBase:
	; Each entry is 8 bytes.
	
	; null: selector 0x00
	dd	0
	dd	0

	; code: selector 0x08
	dw	0FFFFh		; limit low
	dw	0			; base low
	db	0			; base middle
	db	10011010b 	; access
	db	11001111b   ; granularity
	db	0			; base high

	; data: selector 0x10
	dw	0FFFFh
	dw	0
	db	0
	db	10010010b
	db	11001111b
	db	0

	; VM86 tss 0x10
	dw	0100h		; limit low
	dw	07E00h		; base low
	db	0			; base middle
	db	10001001b	; access
	db	01000000b	; granularity
	db	0			; base high

	; Kernel TSS 0x20
	dw	0100h		; limit low
	dw	07F00h		; base low
	db	0			; base middle
	db	10001001b	; access
	db	01000000b	; granularity
	db	0			; base high

GDTEnd:

toc:
	dw	GDTEnd - GDTBase - 1 ; size of GDT
	dd	GDTBase		     ; GDT base address

idt_48:				; dummy IDT for going into protected mode
	dw	0
	dw	0,0
	
;;;;;;;;;;;;;;;;;; 32-bit code
	section .text
	
[BITS 32]

	%define KERNEL_BYTE_LENGTH 131072

PModeSetup:	

	mov	AX, 10h		; Setting DS, SS, ES to the data selector.
	mov	DS, AX
	mov	SS, AX
	MOV	ES, AX
	mov	ESP, 0A0000h	; put a stack at 0xA0000 for now	

Relocate:
	MOV	ECX, KERNEL_BYTE_LENGTH
	MOV	EDI, 0x100000
	MOV	ESI, 0x9200
	
.copyLoop:
	MOVSB
	LOOP	.copyLoop
	
GoPMode:	
	JMP	08h:0100000h
	
StopSystem:
	CLI
	HLT

;	align 0x1000		
	KERNEL_VIRTUAL_BASE equ 0xC0000000
	KERNEL_PAGE_NUMBER equ (KERNEL_VIRTUAL_BASE >> 22)

;BootPageDirectory:
	; Identity map the first 4MB of the address space.
	; Bit 7 - Page is 4MB
	; Bit 1 - R/W
	; Bit 0 - Present
	dd 0x00000083
	times (KERNEL_PAGE_NUMBER - 1) dd 0
	; Define a 4MB page for the kernel
	dd 0x00000083
	times (1024 - KERNEL_PAGE_NUMBER - 1) dd 0
