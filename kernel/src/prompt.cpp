#include "prompt.h"
#include "globals.h"
#include "process/message.h"
#include "fat12.h"

char command_line[256];
int command_line_position = 0;
char command_prompt_string[64] = "> ";

/* Prompt */
void CMD_ResetPrompt(char *command_line)
{
  memset(command_line, 0, 256);
  command_line_position = 0;
}

void CMD_GetArgcArgv(char *command_line, int *argc, char *argv[])
{
  *argc = 0;

  char current_arg[256];
  int current_arg_pos = 0;

  printf("argv[0]: %02X%02X%02X%02X%02X%02X%02X%02X\n", command_line[0], command_line[1],command_line[2], command_line[3],
                                                        command_line[4], command_line[5], command_line[6], command_line[7]);

  // handle the command, which is arg 0
  for(size_t pos=0; pos < strlen(command_line)+1; pos++)
  {
    if(command_line[pos] == ' ' || command_line[pos] == 0)
    {
      // End of an argument. Add it to argv.
      current_arg[current_arg_pos++] = 0;
      argv[*argc] = (char *)gMemoryManager.NewPtr(current_arg_pos, H_SYSHEAP);
      strcpy(argv[*argc], current_arg);

      *argc = *argc + 1;
      current_arg_pos = 0;
    }
    else
    {
      current_arg[current_arg_pos++] = command_line[pos];
    }
  }

  if(*argc == 0)
  {
    // Only one arg, the command. Copy the command line to argv[0].
    argv[0] = (char *)gMemoryManager.NewPtr(strlen(command_line), H_SYSHEAP);
    strcpy(argv[0], command_line);
    *argc = 1;
  }
}

void CMD_PrintPrompt()
{
  // TODO: construct this dynamically
  printf("\n%c:> ", DRIVE_INDEX_TO_DRIVE_LETTER(current_drive));
}

void CMD_CleanupArgcArgv(int *argc, char *argv[])
{
  for(int arg=0; arg < *argc; arg++)
    {
      if(argv[arg] != NULL)
      {
        gMemoryManager.DisposePtr(argv[arg]);
        argv[arg] = NULL;
      }
    }
}

void CMD_Process(char *command_line)
{
  int argc = 0;
  char *argv[64];
  
  CMD_GetArgcArgv(command_line, &argc, argv);

  int found_command = 0;
  for(size_t i=0; i<num_supported_commands; i++)
    {
      if(strcmp(argv[0], CMD_SupportedCommands[i].name) == 0)
      {	  
        found_command = 1;
        CMD_SupportedCommands[i].function(argc, argv);
      }
    }

  if(!found_command)
  {
    printf("Command not found\n");
  }

  CMD_CleanupArgcArgv(&argc, argv);
  CMD_ResetPrompt(command_line);
}

void CMD_PromptLoop()
{
  asm("sti");

  uint8_t exit_prompt = 0;
  CMD_ResetPrompt(command_line);
  CMD_PrintPrompt();

  while(true)
  {
    IPC_Message *msg; 
    msg = IPC_GetMessage();

    uint8_t ascii = (((PM_Notification *)msg)->wParam) & 0x00FF;

    // Handle the keypress.
    if(ascii == '\b' && command_line_position >= 1)
    {
      printf("\b", ascii);
      command_line[command_line_position] = 0;
      command_line_position -= 1;
    }
    else if(ascii == '\b')
    {
      /* at the beginning of a line, do nothing */
    }
    else if(ascii == '\n' && command_line_position >= 0)
    {
      printf("%c", ascii);
      printf("\n");

      command_line[command_line_position] = 0;
      CMD_Process(command_line);
      
      CMD_PrintPrompt();
    } 
    else if(ascii != 0)
    {
      printf("%c", ascii);
      command_line[command_line_position++] = ascii; 
    }
  }
}
