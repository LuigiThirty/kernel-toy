#pragma once

#include "procyon.h"
#include <string.h>
#include "arch/i386/low_level_io.h"

extern char serialbuffer[1024];

#define COM1_PRINTF(f_, ...) { sprintf(serialbuffer, f_, ##__VA_ARGS__); COM1.PutStr(serialbuffer); }

class SerialPort {

    public:
    typedef enum {
        COM1,
        COM2,
        COM3,
        COM4
    } COMPortNumber;

    typedef enum {
        BAUD_50     =   0x0900,
        BAUD_300    =   0x0180,
        BAUD_1200   =   0x0060,
        BAUD_2400   =   0x0030,
        BAUD_4800   =   0x0018,
        BAUD_9600   =   0x000C,
        BAUD_19200  =   0x0006,
        BAUD_38400  =   0x0003,
        BAUD_57600  =   0x0002,
        BAUD_115200 =   0x0001,
    } BaudRate;

    uint16_t io_port;

    bool is_transmit_empty() 
    {
        return _inb(io_port + 5) & 0x20;
    }

    void TransmitByte(uint8_t byte)
    {
        while(is_transmit_empty() == false) {};
        _outb(io_port, byte);
    }
    uint8_t ReadByte()
    {
        return _inb(io_port);
    }

    void SetBaudRate(BaudRate divisor)
    {
        uint8_t divisor_dll, divisor_dlm;
        divisor_dll = divisor & 0xFF;
        divisor_dlm = divisor >> 8;

        // Read the LCR
        uint8_t lcr = ReadLCR();
        // Set the DLAB bit
        lcr |= 0x80;
        SetLCR(lcr);

        // Write divisor
        _outb(io_port, divisor_dll);
        _outb(io_port+1, divisor_dlm);

        // Clear DLAB
        lcr &= 0x7F;
        SetLCR(lcr);
    }

    // FIFO Control
    void SetFCR(uint8_t byte)
    {
        _outb(io_port+2, byte);
    }
    uint8_t ReadFCR()
    {
        return _inb(io_port+2);
    }

    // Line Control Register
    void SetLCR(uint8_t byte)
    {
        _outb(io_port+3, byte);
    }
    uint8_t ReadLCR()
    {
        return _inb(io_port+3);
    }

    // Modem Control Register
    void SetMCR(uint8_t byte)
    {
        _outb(io_port+4,  byte);
    }
    uint8_t ReadMCR()
    {
        return _inb(io_port+4);
    }

    // Status registers - Line Status and Modem Status
    uint8_t ReadLSR()
    {
        return _inb(io_port+5);
    }
    uint8_t ReadMSR()
    {
        return _inb(io_port+6);
    }

    void PutStr(char *s)
    {
        for(int i=0; i<strlen(s); i++) { TransmitByte(s[i]); }
    }
};

extern SerialPort COM1;