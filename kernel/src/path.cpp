#include "path.h"

DRIVE_LETTER PATH_FindDriveIndex(CString path)
{
    // Is this a path with a drive letter?
    char letter = toupper(path[0]);
    if(path[1] != ':' || path[2] != '\\')
        return DRIVE_INVALID; // not a path with a drive letter

    if(letter >= 'A' && letter <= 'Z')
    {
        return (DRIVE_LETTER)(letter - 0x41);
    }

    return DRIVE_INVALID;
}

void PATH_GetDirectory(CString buffer, CString path, size_t max_length)
{
    for(int i=0; i<strlen(path); i++)
    {
        // Look for the first backslash in the path.
        if(path[i] == '\\')
        {
            strncpy(buffer, path+i, max_length);
            if(buffer[strlen(buffer)-1] != '\\')
            {
               buffer[strlen(buffer)] = '\\';
               buffer[strlen(buffer)+1] = 0; 
            }
            return;
        }
    }
}

void PATH_FindFileName(CString buffer, CString path)
{
    // Find the last backslash in the path.

    int length = strlen(path);
    int start_of_filename = 0;
    int filename_length = 0;

    for(int i=length; i>=0; i--)
    {
        if(path[i] == '\\')
        {
            start_of_filename = i+1;
            filename_length = strlen((const char *)path[start_of_filename]);
            break;
        }
    }
    

    memset(buffer, 0, 16);
    memcpy(buffer, path+start_of_filename, length-start_of_filename);
}