global _IDT_GPF
extern simple_printf

%define		int_ES		word [ebp+0]
%define		int_DS		word [ebp+4]
%define		int_EDI		dword [ebp+8]
%define		int_ESI		dword [ebp+12]
%define		int_EBP		dword [ebp+16]
%define		int_ESP		dword [ebp+20]
%define		int_EBX		dword [ebp+24]
%define		int_EDX		dword [ebp+28]
%define		int_ECX		dword [ebp+32]
%define		int_EAX		dword [ebp+36]
%define		int_AX		word [ebp+36]
%define		int_AL		byte [ebp+36]
%define		int_ERRCODE	dword [ebp+40]
%define		int_EIP		dword [ebp+44]
%define		int_EIP_w	word [ebp+44]
%define		int_CS		word [ebp+48]
%define		int_EFLAGS	dword [ebp+52]
%define		int_FLAGS	word [ebp+56]
; the following also exist if coming out of v86 mode
%define		int_v86_ESP	dword [ebp+60]
%define		int_v86_SS	word [ebp+64]

%define         FLAT_DATA_SEL   010h
%define         FLAT_CODE_SEL   008h

%macro GetGPFInstructionWord 0
	xor	eax,eax
	mov	ax,int_CS
	mov	ebx,int_EIP
	call	SegmentToLinear
	mov	dx,[eax]
%endmacro

	
_IDT_GPF:
	pusha
	push ds
	push es
	
	; The GPF handler.
	
	; Our data segment got cleared. Reset it to the right segment.
	mov	ax,010h
	mov	ds,ax

	lea	ebp,[esp]
	
	; Are we in virtual 8086 mode?
	test	int_EFLAGS,020000h ; In V86 mode?
	jnz	_V86Monitor	; yes, go to the V86 monitor

.protModeGPF:
	extern  _IDT_GPF_C
	call	_IDT_GPF_C

	IRET

	
_V86Monitor:
	; Yes, we're in virtual 8086 mode.
	; Get the CS:IP of the instruction that caused the interrupt.
	GetGPFInstructionWord
	
	mov	[gpfInstruction],dl
	mov	dx,int_CS
	mov	[gpfCS],dx
	mov	dx,int_EIP_w
	mov	[gpfIP],dx
	
	GetGPFInstructionWord
	; Is this an interrupt? opcode $CD
	cmp	dl,0CDh
	je	V86_IsInterrupt
	cmp	dl,0EEh
	je	V86_IsOutEE
	cmp	dl,0EFh
	je	V86_OutDXAX
	cmp	dl,0EDh
	je	V86_IsInED
	cmp	dl,0ECh
	je	V86_IsInEC
	cmp	dl,0CFh
	je	V86_Iret
	cmp	dl,0FAh
	je	V86_CLI
	cmp	dl,0FBh
	je	V86_SEI
	cmp	dl,0x9C
	je	V86_PUSHF
	cmp	dl,0x9D
	je	V86_POPF
	cmp	dl,0xCC
;	je	V86_Int3
	
	extern PrintGPFInstruction
	call	PrintGPFInstruction
	
.forever:
	jmp	.forever

V86_Int3:
	inc	int_EIP
	mov	eax,3

	call	V86_PushInterrupt
	jmp	VM_EmulateIRET
	
V86_OutDXAX:
	inc	int_EIP
	mov	edx,int_EDX
	and	edx,0FFFFh
	mov	eax,int_EAX
	and	eax,0FFFFh
	out	dx,ax
	jmp	VM_EmulateIRET
	
V86_PUSHF:
	mov	bx,FLAT_DATA_SEL
	mov	es,bx
	sub	int_v86_ESP,2
	call	VMSSESPToLinear
	mov	bx,int_FLAGS
	mov	[es:eax],bx
	add	int_EIP,1
	jmp	VM_EmulateIRET
	
V86_POPF:
	mov	bx,FLAT_DATA_SEL
	mov	es,bx
	call	VMSSESPToLinear
	add	int_v86_ESP,2
	mov	bx,[es:eax]
	mov	int_FLAGS,bx
	add	int_EIP,1
	jmp	VM_EmulateIRET

V86_CLI:
	and	int_EFLAGS,~0x200
	inc	int_EIP
	jmp	VM_EmulateIRET

V86_SEI:
	or	int_EFLAGS,0x200
	inc	int_EIP
	jmp	VM_EmulateIRET
	
V86_IsInED:
	add	int_EIP,1
	mov	eax,int_EAX
	mov	edx,int_EDX
	in	ax,dx
	mov	int_AX,ax
	jmp	VM_EmulateIRET

V86_IsOutEE:
	add	int_EIP,1
	mov	edx,int_EDX
	mov	eax,int_EAX
	out	dx,al
	jmp	VM_EmulateIRET

V86_IsInEC:
	add	int_EIP,1
	mov	edx,int_EDX
	mov	eax,int_EAX
	in	al,dx
	mov	int_AL,al
	jmp	VM_EmulateIRET

V86_Iret:
	mov	ax,FLAT_DATA_SEL
	mov	es,ax
	call	VMSSESPToLinear
	
	add	int_v86_ESP,6
	xor	ebx,ebx
	mov	bx,[es:eax+0]
	mov	int_EIP,ebx
	mov	bx,[es:eax+2]
	mov	int_CS,bx
	mov	bx,[es:eax+4]
	mov	int_FLAGS,bx
	
	jmp	VM_EmulateIRET
	
V86_IsInterrupt:
;	mov	eax,010h		; testing: just INT 10h
	mov	bx,FLAT_DATA_SEL
	mov	es,bx

	call	V86_CSEIPToLinear
	mov	eax,[es:eax]
	movzx	eax,ah

	; Intercept INT 21 and redirect to our exit routine.
	cmp	al,021h
	je	V86_ReturnToPM
	
	add	int_EIP,2
	call V86_PushInterrupt
	jmp	VM_EmulateIRET
	
V86_PushInterrupt:
	mov	bx,FLAT_DATA_SEL
	mov	es,bx
	
	mov	ecx,[es:eax*4]
	sub	int_v86_ESP,6

	call	VMSSESPToLinear
	mov	ebx,int_EIP
	mov	word [es:eax+0],bx
	mov	bx,int_CS,
	mov	word [es:eax+2],bx
	mov	ebx,int_EFLAGS
	mov	word [es:eax+4],bx

	mov	int_EIP_w,cx
	shr	ecx,16
	mov	int_CS,cx

	ret
	
	; Point to the interrupt handler in the IVT.
	mov	int_EIP,027EDh
	mov	int_CS,0C000h

	jmp	VM_EmulateIRET
	
.forever:
	jmp	.forever

	extern _ExitV86
V86_ReturnToPM:
	; Exit our task and go back to protected mode.
	jmp	_ExitV86

regArea: resb 128
tempESP: dd 0

strV86GPF: db "V86 mode GPF",10,0
strInstructionIsUnhandled: db "Instruction unhandled at %04X:%04X. %04X",10,0
strInstructionIsInt: db "INT %02X",10,0

global gpfInstruction, gpfCS, gpfIP
gpfInstruction: db 0
gpfCS: dw 0
gpfIP: dw 0
	
VM_EmulateIRET:
	pop	es
	pop	ds
	popa
	add	esp,4		; toss out the error code
	IRET
	
VMSSESPToLinear:
	xor		eax,eax
	mov		ax,int_v86_SS
	shl		eax,4
	mov		ebx,int_v86_ESP
	and		ebx,0xFFFF
	add		eax,ebx
	ret	
	
SegmentToLinear:
	; eax is segment
	; ebx is offset
	; returns linear address in eax
	shl	eax,4
	add	eax,ebx
	RET

V86_CSEIPToLinear:
	xor		eax,eax
	mov		ax,int_CS
	shl		eax,4
	mov		ebx,int_EIP
	and		ebx,0xFFFF
	add		eax,ebx
	ret
