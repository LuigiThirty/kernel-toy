#pragma once

#include "procyon.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

extern uint8_t _inb(uint16_t port);
extern void _outb(uint16_t port, uint8_t value);
extern void _io_wait();

#ifdef __cplusplus
}
#endif