#include "pic.h"

void outb(unsigned short port, unsigned char val)
{
  asm volatile("outb %0, %1" : : "a"(val), "Nd"(port) );
}

static __inline unsigned char inb (unsigned short int port)
{
  unsigned char _v;
  __asm__ __volatile__ ("inb %w1,%0": "=a" (_v): "Nd"(port));
  return _v;
}

/* Reports that an IRQ routine has ended.
   Send this at the end of any IRQ handler. */
void PIC_SendEOI(uint8_t irq_level)
{
  // Cascade if IRQ 8 or higher
  if(irq_level >= 8)
    outb(PIC2_COMMAND, PIC_ENDOFINTERRUPT);

  outb(PIC1_COMMAND, PIC_ENDOFINTERRUPT);
}

/* Remaps the interrupt vectors for protected mode.
   offset1 -> master PIC offset
   offset2 -> slave PIC offset */
void PIC_Remap(int offset1, int offset2)
{
  outb(PIC1_COMMAND, 0x11);
  outb(PIC2_COMMAND, 0x11);

  outb(PIC1_DATA, 0x20);
  outb(PIC2_DATA, 0x28);

  outb(PIC1_DATA, 4);
  outb(PIC2_DATA, 2);

  outb(PIC1_DATA, 0x01);
  outb(PIC2_DATA, 0x01);

  outb(PIC1_DATA, 0xFF);
}

// Mask an individual IRQ level, disabling that IRQ.
void PIC_SetMask(uint8_t irq_level)
{
  uint16_t port;
  uint8_t value;

  // Check for cascade.
  if(irq_level < 8)
    port = PIC1_DATA;
  else
    {
      port = PIC2_DATA;
      irq_level -= 8;
    }

  value = inb(port) | (1 << irq_level);
  outb(port, value);
}

// Clears a mask, enabling that IRQ.
void PIC_ClearMask(uint8_t irq_level)
{
  uint16_t port;
  uint8_t value;

  // Check for cascade.
  if(irq_level < 8)
    port = PIC1_DATA;
  else
    {
      port = PIC2_DATA;
      irq_level -= 8;
    }

  value = inb(port) & ~(1 << irq_level);
  outb(port, value);
}

// IRR is a bitmask of which interrupts are currently raised --
// meaning they're at the PIC but *not necessarily* the CPU.
uint16_t PIC_GetIRR()
{
  return PIC_GetIRQReg(PIC_READ_IRR);
}

// ISR is a bitmask of which interrupts are being raised and
// being serviced by the CPU.
uint16_t PIC_GetISR()
{
  return PIC_GetIRQReg(PIC_READ_ISR);
}
