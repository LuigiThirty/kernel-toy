[BITS 32]
[CPU 386]

global _inb
global _outb
global _io_wait

; Follow the GCC cdecl convention, idiot.
; https://wiki.osdev.org/Calling_Conventions
_inb: 				; tested - works
	PUSH	EBP		; preserve EBP
	MOV	EBP, ESP 	; preserve ESP in EBP
	MOV	DX, [EBP+8]
	IN	AL, DX
	LEAVE
	RET

_outb:
	PUSH	EBP		; preserve EBP
	MOV	EBP, ESP	; set up SP
	MOV	DX, [EBP+8]	; EBP+8  = port
	MOV	AL, [EBP+12]	; EBP+12 = value
	OUT	DX, AL		; write AL to port DX
	LEAVE
	RET

; Adds a short artificial delay to I/O operations
; by writing to an unused I/O port.
_io_wait:
	push	ax
	mov	ax, 0
	push	ax
	mov	ax, 0x80
	push	ax
	call	_outb

	pop	ax
	pop	ax
	pop	ax
	ret
