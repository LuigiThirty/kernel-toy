	[BITS 32]
	[CPU 386]

	section .text

	global  PAGE_LoadPageDirectory
PAGE_LoadPageDirectory:
        PUSH    EBP
        MOV     EBP, ESP
        MOV     EAX, [ESP+8]
        MOV     CR3, EAX
        MOV     ESP, EBP
        POP     EBP
        RET

        global  PAGE_EnablePaging
PAGE_EnablePaging:
        PUSH    EBP
        MOV     EBP, ESP
        MOV     EAX, CR0
        OR      EAX, $80000000
        MOV     CR0, EAX
        MOV     ESP, EBP
        POP     EBP
        RET
