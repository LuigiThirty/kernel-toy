#pragma once

#include "procyon.h"
#include "low_level_io.h"

/* I/O base addresses */
#define PIC1 0x20
#define PIC2 0xA0
/* Command and data addresses */
#define PIC1_COMMAND PIC1
#define PIC1_DATA (PIC1 + 1)
#define PIC2_COMMAND PIC2
#define PIC2_DATA (PIC2+1)

#define PIC_ENDOFINTERRUPT 0x20

/* PIC commands */
#define PIC_READ_IRR 0x0A
#define PIC_READ_ISR 0x0B

#define ICW1_ICW4 0x01
#define ICW1_SINGLE 0x02
#define ICW1_INTERVAL4 0x04
#define ICW1_LEVEL 0x08
#define ICW1_INIT 0x10

#define ICW4_8086 0x01
#define ICW4_AUTO 0x02
#define ICW4_BUF_SLAVE 0x08
#define ICW4_BUF_MASTER 0x0C
#define ICW4_SFNM 0x10

#ifdef __cplusplus
extern "C" {
#endif

void PIC_SendEOI(uint8_t irq_level);
void PIC_Remap(int offset1, int offset2);

void PIC_SetMask(uint8_t irq_level);
void PIC_ClearMask(uint8_t irq_level);

// Helper
static uint16_t PIC_GetIRQReg(int ocw3)
{
  // OCW3 to PIC CMD to get the register values.
  _outb(PIC1_COMMAND, ocw3);
  _outb(PIC2_COMMAND, ocw3);

  return (_inb(PIC2_COMMAND) << 8) | _inb(PIC1_COMMAND);
}

uint16_t PIC_GetIRR();
uint16_t PIC_GetISR();

#ifdef __cplusplus
}
#endif