[CPU 386]
[BITS 32]

global PM_SaveRegisters
global PM_RestoreRegisters	
extern PM_SavedRegs
	
PM_SaveRegisters:
	PUSHF

	MOV [PM_SavedRegs+0], EAX
	MOV [PM_SavedRegs+4], EBX
	MOV [PM_SavedRegs+8], ECX
	MOV [PM_SavedRegs+12], EDX
	MOV [PM_SavedRegs+16], EBP
	MOV [PM_SavedRegs+20], ESP
	MOV [PM_SavedRegs+24], ESI
	MOV [PM_SavedRegs+28], EDI

	MOV [PM_SavedRegs+32], CS
	MOV [PM_SavedRegs+34], DS
	MOV [PM_SavedRegs+36], ES
	MOV [PM_SavedRegs+38], FS
	MOV [PM_SavedRegs+40], GS

	MOV EAX, [ESP+0x14]
	MOV [PM_SavedRegs+48], EAX
	
	POP EAX
	MOV [PM_SavedRegs+42], EAX

	RET

PM_RestoreRegisters:	
	MOV	EAX, [PM_SavedRegs+0]
	MOV	EBX, [PM_SavedRegs+4]
	MOV	ECX, [PM_SavedRegs+8]
	MOV	EDX, [PM_SavedRegs+12]
	
	MOV	EBP, [PM_SavedRegs+16]
	MOV	ESP, [PM_SavedRegs+20]
	MOV	ESI, [PM_SavedRegs+24]
	MOV	EDI, [PM_SavedRegs+28]

	; CS is loaded through RET.
	MOV	DS, [PM_SavedRegs+34]
	MOV	ES, [PM_SavedRegs+36]
	MOV	FS, [PM_SavedRegs+38]
	MOV	GS, [PM_SavedRegs+40]

	; Update the stack's return address.
	MOV	EAX, [PM_SavedRegs+48]
	MOV	[ESP+0x14], EAX
	
	MOV	EAX, [PM_SavedRegs+42]
	PUSH	EAX
	MOV	EAX, [PM_SavedRegs+0]

	POPF
	ADD	ESP, 0x14
	
	RET
