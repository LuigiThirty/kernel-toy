[CPU 386]
[BITS 32]

SECTION .text

%define		int_ES		word [ebp+0]
%define		int_DS		word [ebp+4]
%define		int_EDI		dword [ebp+8]
%define		int_ESI		dword [ebp+12]
%define		int_EBP		dword [ebp+16]
%define		int_ESP		dword [ebp+20]
%define		int_EBX		dword [ebp+24]
%define		int_EDX		dword [ebp+28]
%define		int_ECX		dword [ebp+32]
%define		int_EAX		dword [ebp+36]
%define		int_AX		word [ebp+36]
%define		int_AL		byte [ebp+36]
%define		int_ERRCODE	dword [ebp+40]
%define		int_EIP		dword [ebp+44]
%define		int_EIP_w	word [ebp+44]
%define		int_CS		word [ebp+48]
%define		int_EFLAGS	dword [ebp+52]
%define		int_FLAGS	word [ebp+56]
; the following also exist if coming out of v86 mode
%define		int_v86_ESP	dword [ebp+60]
%define		int_v86_SS	word [ebp+64]

extern _PIT_IRQHandler
extern _KB_IRQHandler
extern _FDC_IRQHandler
extern _Syscall_IRQHandler
	
	; IDT trampolines.	
%define SECTN_BASE 0x100000
	
%macro IDT_ENTRY 2
	dw	(SECTN_BASE + %1 - $$) & 0xFFFF	; low word of offset
	dw	%2		; Code segment selector
	dw	0x8E00		; type and attributes
	dw	(SECTN_BASE + %1 - $$) >> 16	; high word of offset
%endmacro

	global IDT_Install
IDT_Install:
	MOV		EAX, [ESP+4]
	LIDT	[IDT_Info]
	RET
	
IDT_Table:
	IDT_ENTRY __IDT_DivideByZero, 0x08 ; entry and code segment selector
	IDT_ENTRY __IDT_Debug, 0x08 ; entry and code segment selector
	IDT_ENTRY __IDT_NMI, 0x08 ; entry and code segment selector
	IDT_ENTRY __IDT_Breakpoint, 0x08 ; entry and code segment selector
	IDT_ENTRY __IDT_Overflow, 0x08 ; entry and code segment selector
	IDT_ENTRY __IDT_BoundRangeExceeded, 0x08 ; entry and code segment selector
	IDT_ENTRY __IDT_InvalidOpcode, 0x08 ; entry and code segment selector
	IDT_ENTRY __IDT_DeviceNotAvailable, 0x08 ; entry and code segment selector
	IDT_ENTRY __IDT_DoubleFault, 0x08 ; entry and code segment selector
	IDT_ENTRY __IDT_CoprocessorSegmentOverrun, 0x08 ; entry and code segment selector
	IDT_ENTRY __IDT_InvalidTSS, 0x08 ; entry and code segment selector
	IDT_ENTRY __IDT_SegmentNotPresent, 0x08 ; entry and code segment selector
	IDT_ENTRY __IDT_StackSegmentFault, 0x08 ; entry and code segment selector
	IDT_ENTRY __IDT_GPF, 0x08 ; entry and code segment selector
	IDT_ENTRY __IDT_PageFault, 0x08 ; entry and code segment selector
	IDT_ENTRY __IDT_Reserved , 0x08 ; entry and code segment selector
	; 16 empty spots...
	IDT_ENTRY __IDT_Reserved, 0x08
	IDT_ENTRY __IDT_Reserved, 0x08
	IDT_ENTRY __IDT_Reserved, 0x08
	IDT_ENTRY __IDT_Reserved, 0x08
	IDT_ENTRY __IDT_Reserved, 0x08
	IDT_ENTRY __IDT_Reserved, 0x08
	IDT_ENTRY __IDT_Reserved, 0x08
	IDT_ENTRY __IDT_Reserved, 0x08
	IDT_ENTRY __IDT_Reserved, 0x08
	IDT_ENTRY __IDT_Reserved, 0x08
	IDT_ENTRY __IDT_Reserved, 0x08
	IDT_ENTRY __IDT_Reserved, 0x08
	IDT_ENTRY __IDT_Reserved, 0x08
	IDT_ENTRY __IDT_Reserved, 0x08
	IDT_ENTRY __IDT_Reserved, 0x08
	IDT_ENTRY __IDT_Reserved, 0x08
		
	; IRQs 0-15 are mapped to 0x20 through 0x2F.
	IDT_ENTRY __IDT_TimerIRQ, 0x08		; IRQ 0
	IDT_ENTRY __IDT_KeyboardIRQ, 0x08	; IRQ 1
	dd 0,0					; IRQ 2
	dd 0,0					; IRQ 3
	dd 0,0					; IRQ 4
	dd 0,0					; IRQ 5
	IDT_ENTRY __IDT_FloppyIRQ, 0x08		; IRQ 6
	dd 0,0					; IRQ 7
	dd 0,0					; IRQ 8
	dd 0,0					; IRQ 9
	dd 0,0					; IRQ 10
	dd 0,0					; IRQ 11
	dd 0,0					; IRQ 12
	dd 0,0					; IRQ 13
	dd 0,0					; IRQ 14
	dd 0,0					; IRQ 15

	; Software interrupts start at 0x30.
	IDT_ENTRY __IDT_Syscall, 0x08 		; INT 0x30
	
IDT_TableEnd:

	global IDT_Info
IDT_Info:
	dw IDT_TableEnd-IDT_Table-1
	dd IDT_Table

__IDT_KeyboardIRQ:
	PUSHAD
	CALL	_KB_IRQHandler
	POPAD
	IRET

__IDT_TimerIRQ:
	PUSHAD
	CALL	_PIT_IRQHandler
	POPAD
	IRET

__IDT_FloppyIRQ:
	PUSHAD
	CALL	_FDC_IRQHandler
	POPAD
	IRET

__IDT_Syscall:
	PUSHAD
	
	PUSH	EDX
	PUSH	ECX
	PUSH	EBX
	PUSH	EAX

	CALL	_Syscall_IRQHandler

	POP		EDX
	POP		ECX
	POP		EBX
	POP		EAX
	
	POPAD
	IRET
	
%macro IDT_Trampoline 1
extern _IDT_%1
__IDT_%1:
	; Populate the exception info table.
	mov [07E00h], ESP
	jmp	_IDT_%1
%endmacro
	IDT_Trampoline DivideByZero
	IDT_Trampoline Debug
	IDT_Trampoline NMI
	IDT_Trampoline Breakpoint
	IDT_Trampoline Overflow
	IDT_Trampoline BoundRangeExceeded
	IDT_Trampoline InvalidOpcode
	IDT_Trampoline DeviceNotAvailable
	IDT_Trampoline DoubleFault
	IDT_Trampoline CoprocessorSegmentOverrun
	IDT_Trampoline InvalidTSS
	IDT_Trampoline SegmentNotPresent
	IDT_Trampoline StackSegmentFault
	IDT_Trampoline GPF
	IDT_Trampoline PageFault
	IDT_Trampoline Reserved
