%define KERNEL_TSS 07F00h
%define V86_TSS 07E00h

extern _IDT_GPF

global _EnterV86
_EnterV86: ; extern void _EnterV86(uint32_t ss, uint32_t esp, uint32_t cs, uint32_t eip);

   ; Store task info in the TSS.
   mov eax, KERNEL_TSS
   mov ebp, eax

   ; Store registers...
   mov   [ebp+40], eax
   mov   [ebp+44], edx
   mov   [ebp+48], ecx
   mov   [ebp+52], ebx
   mov   [ebp+56], esp
   mov   [ebp+64], esi
   mov   [ebp+68], edi
   pushfd
   mov   eax,[esp]
   mov   [ebp+36], eax
   popfd

   ; Create an interrupt stack frame.
   mov ebp, esp               ; save stack pointer

   push dword [ebp+4]         ; ss
   push dword [ebp+8]         ; esp
   pushfd                     ; eflags
   or dword [esp], (1 << 17)  ; set VM flags
   or dword [esp], (1 << 9)
   or dword [esp], (1 << 1)
   push dword [ebp+12]        ; cs
   push dword [ebp+16]        ; eip

   iret

global _ExitV86
_ExitV86:
   ; Return to protected mode.

   mov   eax,KERNEL_TSS
   mov   ebp,eax

   mov   eax, [ebp+36]
   push  dword eax
   popfd 

   mov   eax, [ebp+40]
   mov   edx, [ebp+44]
   mov   ecx, [ebp+48]
   mov   ebx, [ebp+52]
   mov   esp, [ebp+56]
   mov   esi, [ebp+64]
   mov   edi, [ebp+68]
   ret

.forever:
   jmp .forever