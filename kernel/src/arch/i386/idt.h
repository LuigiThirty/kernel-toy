#pragma once

#include <stdio.h>
#include "procyon.h"

#ifdef __cplusplus
extern "C" {
#endif

uint16_t exception_cs, exception_ds, exception_es, exception_fs, exception_gs;

extern uint8_t *IDT_Info;
extern void IDT_Install(uint8_t *idt);

void _IDT_DivideByZero();
void _IDT_Debug();
void _IDT_NMI();
void _IDT_Breakpoint();
void _IDT_Overflow();
void _IDT_BoundRangeExceeded();
void _IDT_InvalidOpcode();
void _IDT_DeviceNotAvailable();
void _IDT_DoubleFault();
void _IDT_CoprocessorSegmentOverrun();
void _IDT_InvalidTSS();
void _IDT_SegmentNotPresent();
void _IDT_StackSegmentFault();
extern void _IDT_GPF();
void _IDT_PageFault();
void _IDT_Reserved();

void _IDT_GPF_C();

#ifdef __cplusplus
}
#endif