#include "idt.h"
#include "low_level_io.h"

/*
#pragma pack (push,1)
typedef struct {
  uint32_t es;
  uint32_t ds;
  uint32_t edi;
  uint32_t esi;
  uint32_t ebp;
  uint32_t esp;
  uint32_t ebx;
  uint32_t edx;
  uint32_t ecx;
  uint32_t eax;
  uint32_t errcode;
  uint32_t eip;
  uint32_t cs;
  uint32_t eflags;
  uint32_t flags;
} ExceptionDump;
#pragma pop
*/

char buf[512];

void DumpExceptionInfo()
{
  uint32_t exception_stack_frame = MMIO32(0x7E00);
  printf("(esp is %08X)\n", exception_stack_frame);
  sprintf(buf, "Exception occurred at CS:EIP %04X:%08X (EFLAGS %08X)\n",
    MMIO16(exception_stack_frame+4), 
    MMIO32(exception_stack_frame+0),
    MMIO32(exception_stack_frame+8));
  printf(buf);
}

void CheckReboot()
{
  for(int i=0; i<100000; i++) {};
  // Empty the key buffer.
  while(_inb(0x64) & 1)
  {
    _inb(0x60); // get the scancode
  }
  
  // Wait for a keypress.
  if((_inb(0x64) & 1) == true)
  {
    // Get the keystroke. If it's F12, reboot.
    uint8_t scancode = _inb(0x60); // get the scancode
    if(scancode == 0x58)
    {
      printf("Rebooting...");
      _outb(0x64, 0xFE);
      asm("hlt");
    }
  }
}

void _IDT_DivideByZero()
{
  DumpExceptionInfo();
  printf("Fatal exception 00: Divide by zero. Halting system.\n");
  while(true) { CheckReboot(); };
}

void _IDT_Debug()
{
  DumpExceptionInfo();
  printf("Fatal exception 01: Debug trap. Halting system.\n");
  while(true) {};
}

void _IDT_NMI()
{
  DumpExceptionInfo();
  printf("Fatal exception 02: NMI. Halting system.\n");
  while(true) { CheckReboot(); };
}

void _IDT_Breakpoint()
{
  DumpExceptionInfo();
  printf("Fatal exception 03: Breakpoint. Halting system.\n");
  while(true) { CheckReboot(); };
}

void _IDT_Overflow()
{
  DumpExceptionInfo();
  printf("Fatal exception 04: Overflow. Halting system.\n");
  while(true) { CheckReboot(); };
}

void _IDT_BoundRangeExceeded()
{
  DumpExceptionInfo();
  printf("Fatal exception 05: BOUND range exceeded. Halting system.\n");
  while(true) { CheckReboot(); };
}

void _IDT_InvalidOpcode()
{
  DumpExceptionInfo();
  printf("Fatal exception 06: Invalid instruction. Halting system.\n");
  while(true) { CheckReboot(); };
}

void _IDT_DeviceNotAvailable()
{
  DumpExceptionInfo();
  printf("Fatal exception 07: Device not available. Halting system.\n");
  while(true) { CheckReboot(); };
}

void _IDT_DoubleFault()
{
  DumpExceptionInfo();
  printf("Fatal exception 08: Double fault. Halting system.\n");
  while(true) { CheckReboot(); };
}

void _IDT_CoprocessorSegmentOverrun()
{
  DumpExceptionInfo();
  printf("Fatal exception 09: Coprocessor segment overrun. Halting system.\n");
  while(true) { CheckReboot(); };
}

void _IDT_InvalidTSS()
{
  DumpExceptionInfo();
  printf("Fatal exception 0A: Invalid TSS. Halting system.\n");
  while(true) { CheckReboot(); };
}

void _IDT_SegmentNotPresent()
{
  DumpExceptionInfo();
  printf("Fatal exception 0B: Segment not present. Halting system.\n");
  while(true) { CheckReboot(); };
}

void _IDT_StackSegmentFault()
{
  DumpExceptionInfo();
  printf("Fatal exception 0C: Stack segment fault. Halting system.\n");
  while(true) { CheckReboot(); };
}

/*
void _IDT_GPF()
{
  // Make sure that we are in the correct data selector...
  asm("mov $0x10,%ax");
  asm("mov %ax,%ds");

  //MMIO16(0xB8000) = 0xFFFF;

  printf("Congratulations, your Virtual 8086 monitor has GPFed.\n");

  printf("Fatal exception 0D: General Protection Fault. Halting system.\n");
  while(true) {};
}
*/

void _IDT_GPF_C()
{
  DumpExceptionInfo();
  printf("Fatal exception 0D: General Protection Fault. Halting system.\n");
  while(true) { CheckReboot(); }
}

void _IDT_PageFault()
{
  // TODO: Grab address of fault from CR2

  DumpExceptionInfo();
  printf("Fatal exception 0E: Page fault. Halting system.\n");
  while(true) { CheckReboot(); };
}

void _IDT_Reserved()
{
  DumpExceptionInfo();
  printf("Fatal exception 0F: Reserved. Halting system.\n");
  while(true) { CheckReboot(); };
}

extern uint8_t gpfInstruction;
extern uint16_t gpfIP, gpfCS;
void PrintGPFInstruction()
{
  printf("Instruction that caused GPF is %04X:%04X %02X\n", gpfCS, gpfIP, gpfInstruction);
}