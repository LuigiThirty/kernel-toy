#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <stdio.h>

#include "procyon.h"

#include "vga/vgatext.h"
#include "memmgr.h"
#include "fat12.h"
#include "pic.h"
#include "low_level_io.h"
#include "prompt.h"
#include "fileio.h"
#include "drivers/manager.h"
#include "globals.h"
#include "kerlib.h"

extern "C"
{
    void Kernel_PutChar(unsigned char c);
    void _PModeStart();
}
