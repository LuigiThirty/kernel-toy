#include "virtual86.h"

/** Drop from protected mode to Virtual 8086 mode.
 *  
 *  @param fn
 *  Pointer to the function to copy to conventional memory and execute in V86 mode.
 *  Function must take no arguments and return nothing.
 */
void GoV86(void (*fn)())
{
    memcpy((void *)0x10000, (void *)fn, 1024);
    _EnterV86(0x1200, 0xFFFF, 0x1000, 0x0000); // Jump to 0x1000:0000 (0x10000 linear)
}