#pragma once

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "process.h"
#include "messages.h"
#include "error.h"
#include "drivers/manager.h"

#ifdef __cplusplus
extern "C" {
#endif

// The file IO subsystem.
#define FD_STDIN 0
#define FD_STDOUT 1
#define FD_STDERR 2

#define FILES_LIMIT 255
#define DRIVES_LIMIT 26

typedef uint8_t FileDescriptor;

typedef enum drive_letter_t {
  DRIVE_INVALID = -1,
  DRIVE_A, DRIVE_B, DRIVE_C, DRIVE_D, DRIVE_E,
  DRIVE_F, DRIVE_G, DRIVE_H, DRIVE_I, DRIVE_J,
  DRIVE_K, DRIVE_L, DRIVE_M, DRIVE_N, DRIVE_O,
  DRIVE_P, DRIVE_Q, DRIVE_R, DRIVE_S, DRIVE_T,
  DRIVE_U, DRIVE_V, DRIVE_W, DRIVE_X, DRIVE_Y,
  DRIVE_Z,
} DRIVE_LETTER;
#define DRIVE_LETTER_TO_DRIVE_INDEX(X) ( X - 0x41 )
#define DRIVE_INDEX_TO_DRIVE_LETTER(X) ( X + 0x41 )

typedef enum {
	      DRIVE_TYPE_NONE,
	      DRIVE_TYPE_CHARSPECIAL,
	      DRIVE_TYPE_BLOCKSPECIAL,
	      DRIVE_TYPE_FLOPPY,
	      DRIVE_TYPE_RAM,
	      DRIVE_TYPE_ROM,
} DRIVE_TYPE;

typedef enum {
	      DRIVE_FS_UNMOUNTED,
	      DRIVE_FS_RAW,
	      DRIVE_FS_FAT12,
} DRIVE_FS;

// TODO: ? this seems wrong
// What device driver do we use to access this file?
typedef enum {
		   FILE_TYPE_NOT_IN_USE,
		   FILE_TYPE_SPECIAL,
		   FILE_TYPE_FAT12,
		   FILE_TYPE_STDIN,
		   FILE_TYPE_STDOUT,
		   FILE_TYPE_STDERR
} FD_FILE_TYPE;

// Bitflags, of course.
typedef enum {
	      FILE_MODE_INVALID = 0x8000,
	      FILE_MODE_READ = 0x1,
	      FILE_MODE_WRITE = 0x2
} FD_FILE_MODE;

typedef struct {
  // File type: special (stdin/stdout/stderr) or FAT12 file
  FD_FILE_TYPE type;
  FD_FILE_MODE mode;
  uint32_t starting_cluster;
  uint32_t current_offset;
  uint32_t length;
} FD_ENTRY;

typedef struct {
  DRIVE_TYPE type;     // The drive's type - special, floppy, RAM disk, ROM, etc.
  int index;           // Index corresponding to the drive type
                       //   e.g. FLOPPY/0 = floppy controller, drive 0
  DRIVE_FS filesystem; // The drive's filesystem driver.
} DRIVE_ENTRY;

// System-wide drive letter table
extern DRIVE_ENTRY system_drive_table[26];

// System-wide file table
extern FD_ENTRY system_file_descriptor_table[256];

void FD_InitIO();
void FD_InitSystemFDTable();
void FD_InitSystemDriveTable();

void FD_RegisterDrive(DRIVE_LETTER letter, DRIVE_TYPE type, int index, DRIVE_FS filesystem);

/**
 * @brief Opens a file and returns a file descriptor.
 * 
 * If the file cannot be opened, 0 is returned instead of a file descriptor.
 * 
 * @param path The path to the file that is to be opened.
 **/
FileDescriptor FD_Open(const char *path);

/**
 * @brief Closes an open file descriptor.
 * 
 * If the file is not open, undefined behavior may occur.
 * 
 * @param file The file descriptor to close.
 **/
void FD_Close(FileDescriptor file);

/**
 * @brief Reads data from a file into a buffer.
 * 
 * @param file A file descriptor for an open file.
 * @param buffer A pointer to the memory location that will receive the data.
 * @param length The number of bytes to read from the file.
 **/
size_t FD_Read(FileDescriptor file, uint8_t *buffer, uint32_t length);

/**
 * @brief Writes data from a buffer to a file.
 * 
 * @param file A file descriptor for an open file.
 * @param buffer A pointer to the memory location that contains the data to write.
 * @param length The number of bytes to write to the file.
 **/
size_t FD_Write(FileDescriptor file, const char *buffer, uint32_t length);

uint32_t FD_Length(FileDescriptor file);

/* System call entry points */
size_t __SYS_write(FileDescriptor fd, const char *buffer, size_t length);
size_t __SYS_read(FileDescriptor fd, void *buffer, size_t length);

#ifdef __cplusplus
}
#endif