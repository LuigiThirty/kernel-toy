#pragma once

#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include "memmgr.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct minnode_t
{
  struct minnode_t *mln_Succ;
  struct minnode_t *mln_Pred;
} MinNode;

typedef struct node_t
{
  struct node_t    *ln_Succ;
  struct node_t    *ln_Pred;
  uint8_t  ln_Type;
  int8_t   ln_Pri;
  char *ln_Name;
} Node;

typedef struct
{
  MinNode *mlh_Head;
  MinNode *mlh_Tail;
  MinNode *mlh_TailPred;
} MinList;

typedef struct
{
  Node   *lh_Head;
  Node   *lh_Tail;
  Node   *lh_TailPred;
  uint8_t lh_Type;
  uint8_t lh_Pad;
} List;

typedef enum  {
	       LT_Process,
	       LT_Message,
	       LT_Other
} ListType;

List * LIST_CreateList(ListType type);
Node * LIST_AllocNode();

//void LIST_Init(List *list, ListType type);
void LIST_AddHead(List *list, Node *node);
void LIST_AddTail(List *list, Node *node);
void LIST_Insert(List *list, Node *newNode, Node *listNode);

Node *LIST_RemHead(List *list);
Node *LIST_RemTail(List *list);
void LIST_Remove(Node *node);

bool LIST_IsEmpty(List *list);

#ifdef __cplusplus
}
#endif