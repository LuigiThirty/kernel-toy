#include "reboot.h"

int CMD_reboot_EntryPoint(int argc, char *argv[])
{
    printf("Rebooting...\n");
    // reset the CPU with the KBC.
    _outb(0x64, 0xFE);
    asm("hlt");
}
      