#include "type.h"
#include "fat12.h"
#include "globals.h"
#include "process.h"

#include "drivers/manager.h"
#include "drivers/public/floppydisk.h"
#include "fileio.h"
#include "path.h"
#include "serial.h"
#include <unistd.h>

extern size_t fread(unsigned char fd, void *buffer, unsigned long length);

int CMD_type_EntryPoint(int argc, char *argv[])
{
    if(argc == 1)
    {
        printf("Usage: type filename\n");
        return 1;
    }

    //printf("Drive index: %d\n", PATH_FindDriveIndex(argv[1]));

    char filename[16];
    PATH_FindFileName(filename, argv[1]);
    COM1_PRINTF("TYPE: File name portion of path: %s\r\n", filename);

    // TODO: convert an input path into current drive+filename
    FileDescriptor fd = FD_Open(argv[1]);

    if(fd == 0)
    {
        printf("%s: File not found\n", argv[1]);
        return ERROR_FILE_NOT_FOUND;
    }

    uint32_t file_length = FD_Length(fd);
    
    uint8_t output[file_length+1];
    memset(output, 0, file_length+1);
    //FD_Read(fd, output, file_length);
    uint32_t read_size = fread(fd, output, file_length);
    output[file_length] = 0;
    FD_Write(FD_STDOUT, (const char *)output, strlen((char *)output));
    FD_Close(fd);
    COM1_PRINTF("TYPE: Read %d bytes of the file\r\n", read_size);

    return 0;
  
}