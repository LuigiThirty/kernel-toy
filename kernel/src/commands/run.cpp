#include "run.h"
#include "serial.h"

int CMD_run_EntryPoint(int argc, char *argv[])
{
    //char filename[16] = "TEST.ELF";
    //COM1_PRINTF("RUN: File name portion of path: %s\r\n", filename);

    // FileDescriptor fd = FD_Open(filename);

    FileDescriptor fd = FD_Open(argv[1]);
    uint32_t file_length = FD_Length(fd);

    uint8_t *output = (uint8_t *)gMemoryManager.NewPtr(file_length, H_SYSHEAP);

    memset(output, 0, file_length);
    FD_Read(fd, output, file_length);

    printf("RUN: Read file into buffer\n");

    // char filename[16] = "PROCYON.DYN";
    // ELFManager::ELF_LoadDylib(filename);
    ELFManager::LoadExecutable((char *)output);

    return 1;
}