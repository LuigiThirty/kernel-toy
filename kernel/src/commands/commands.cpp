#include "commands.h"

struct CMD_SupportedCommand CMD_SupportedCommands[] =
  {
   { "dir", CMD_dir_EntryPoint },
   { "crash", CMD_crash_EntryPoint },
   { "type", CMD_type_EntryPoint },
   { "ver", CMD_ver_EntryPoint },
   { "reboot", CMD_reboot_EntryPoint },
   { "run", CMD_run_EntryPoint },
  };

size_t num_supported_commands = sizeof(CMD_SupportedCommands) / sizeof(CMD_SupportedCommands[0]);

