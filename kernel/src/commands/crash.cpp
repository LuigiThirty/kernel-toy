#include "crash.h"
#include "svga.h"

int CMD_crash_EntryPoint(int argc, char *argv[])
{
  printf("Crashing the system!\n");
  uint8_t x;

	asm("movl $0x01, %%eax\n\t"
      "movl $0x41, %%ebx\n\t"
      "int $0x30"
      :                 /* output */
      :                 /* input */
      : "%eax", "%ebx"  /* clobbered */
      );
  
  return 0;
}

