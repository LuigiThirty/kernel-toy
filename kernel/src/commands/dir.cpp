#include "dir.h"

#include "drivers/public/floppydisk.h"
#include "drivers/manager.h"
#include "fat12.h"
#include "globals.h"
#include "process.h"
#include "path.h"
#include "kerlib.h"

int CMD_dir_EntryPoint(int argc __attribute__((unused)), char *argv[] __attribute__((unused)))
{
  // just read out the directory of the current drive if it's a floppy.

  // TODO: Use the current directory as opposed to just "\"
  char path[512];

  if(system_drive_table[current_drive].type != DRIVE_TYPE_FLOPPY)
  {
    printf("Error: Drive %c is not a floppy disk\n", DRIVE_INDEX_TO_DRIVE_LETTER(DRIVE_A));
    return 1;
  }

  if(argc == 1)
  {
    // If there is no directory path argument, use the current directory of the shell process.
    __SYS_getcwd(path, 256);
  }
  else
  {
    strncpy(path, argv[1], 512);
  }

  uint8_t directory_sector[512];
  auto floppy_driver = gProcessManager.FindProcessByName("floppydisk");
  if(floppy_driver == NULL)
  {
    printf("Error: FDD driver is not running\n");
    return 1;
  }
  gFAT12Manager.GetDirectorySector(directory_sector, DRIVE_A, path);

  // Now print the directory.
  // Each entry is 32 bytes. 0x00 and 0xE5 are special filename[0] bytes that
  // mean different status information.
  FAT12::RootDirectoryEntry entry;

  printf("Directory of %s\n", path);

  bool more = true;
  uint16_t index = 0;

  while(more)
  {
    gFAT12Manager.GetDirectoryEntryWithIndex(&entry, index++, directory_sector);
    if(entry.filename[0] == 0x00) break;          // Out of directory entries.
    else if(entry.filename[0] == 0xE5) continue;  // Deleted entry.
    else if(entry.attributes & FAT12::FILE_ATTR_LFN) continue;    // LFN entry.

    if(entry.attributes & FAT12::FILE_ATTR_DIRECTORY)
    {
      printf("%s.%s", entry.filename, entry.extension);
      printf("     <DIR>  ");
      printf("%03X", entry.first_cluster);
      printf("\n");
    }
    else
    {
      printf("%s.%s", entry.filename, entry.extension);
      printf("  ");
      printf("%8d", entry.size_bytes);
      printf("  ");
      printf("%03X", entry.first_cluster);
      printf("\n");
    }
  }

  printf("\n");
  printf("\n");

  return 0;
}
