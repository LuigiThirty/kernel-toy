#pragma once

// Commands
#include "crash.h"
#include "dir.h"
#include "type.h"
#include "ver.h"
#include "reboot.h"
#include "run.h"

/* stuff... */

typedef int (*CMD_CommandFunction)(int argc, char *argv[]);

struct CMD_SupportedCommand {
  const char *name;
  CMD_CommandFunction function;
};

extern struct CMD_SupportedCommand CMD_SupportedCommands[];
extern size_t num_supported_commands;
