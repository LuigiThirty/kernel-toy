#pragma once

#include "low_level_io.h"
#include <stdio.h>
#include "procyon.h"

#include "elf.h"
#include "fileio.h"
#include "path.h"

int CMD_run_EntryPoint(int argc, char *argv[]);