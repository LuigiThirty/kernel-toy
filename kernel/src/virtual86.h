#include <procyon.h>
#include <string.h>

extern "C"
{
    extern void _EnterV86(uint32_t ss, uint32_t esp, uint32_t cs, uint32_t eip);
}

void GoV86(void (*fn)());