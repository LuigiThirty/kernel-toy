#pragma once

#include <stdio.h>
#include <dylib.h>

#include "syscallid.h"
#include "pic.h"

extern "C"
{
    void _Syscall_IRQHandler(uint32_t id, uint32_t parm1, uint32_t parm2, uint32_t parm3);
}