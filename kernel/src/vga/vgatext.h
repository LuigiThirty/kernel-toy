#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "string.h"

#ifdef __cplusplus
extern "C" {
#endif

extern uint16_t * VGABase;

typedef enum vga_color_t {
			  VGA_COLOR_BLACK,
			  VGA_COLOR_BLUE,
			  VGA_COLOR_GREEN,
			  VGA_COLOR_CYAN,
			  VGA_COLOR_RED,
			  VGA_COLOR_MAGENTA,
			  VGA_COLOR_BROWN,
			  VGA_COLOR_LTGRAY,
			  VGA_COLOR_DKGRAY,
			  VGA_COLOR_LTBLUE,
			  VGA_COLOR_LTGREEN,
			  VGA_COLOR_LTCYAN,
			  VGA_COLOR_LTRED,
			  VGA_COLOR_LTMAGENTA,
			  VGA_COLOR_YELLOW,
			  VGA_COLOR_WHITE
} VGAColor;

typedef struct vga_text_cursor_t {
  uint8_t x;
  uint8_t y;
} VGATextCursor;
extern VGATextCursor vgaTextCursor;

// VGA_TM = VGA Text Mode
void VGA_TM_CursorLF();
void VGA_TM_AdvanceCursor();
void VGA_TM_ClearScreen(VGAColor color);
void VGA_TM_Backspace();
void VGA_TM_PutChar(int c);
void VGA_TM_PutStringAtCursor(char *, VGAColor, VGAColor);
//void VGA_TM_PutString(uint8_t row, uint8_t col, char *str, VGAColor fore, VGAColor back);

#ifdef __cplusplus
}
#endif