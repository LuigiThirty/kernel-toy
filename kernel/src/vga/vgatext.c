#include "vga/vgatext.h"
#include "procyon.h"

uint16_t * VGABase = (uint16_t *)VGA_TEXT_AREA;
VGATextCursor vgaTextCursor = { .x=0, .y=0 };

#define VGA_TEXT_COLS 80
#define VGA_TEXT_ROWS 25

void VGA_TM_ScrollUp(int lines)
{
  for(int i=0; i<lines; i++)
    {
      for(int c=0; c<VGA_TEXT_COLS; c++)
	{
	  for(int r=1; r<VGA_TEXT_ROWS; r++)
	    {
	      VGABase[(r-1)*VGA_TEXT_COLS + c] = VGABase[r*VGA_TEXT_COLS + c];
	    }
	}

      for(int c=0; c<VGA_TEXT_COLS; c++)
	{
	      VGABase[24*VGA_TEXT_COLS + c] = 0;	  
	}
    }
}

void VGA_TM_MoveCursor(int row, int col)
{
  vgaTextCursor.x = col;
  vgaTextCursor.y = row;
}

void VGA_TM_Backspace()
{
  if(vgaTextCursor.x > 0)
    {
      vgaTextCursor.x--;
      VGA_TM_PutChar(' ');
      vgaTextCursor.x--;
    }
}

void VGA_TM_AdvanceCursor()
{
  vgaTextCursor.x++;

  if(vgaTextCursor.x >= VGA_TEXT_COLS)
    {
      vgaTextCursor.x = 0;
      vgaTextCursor.y++;

      if(vgaTextCursor.y >= VGA_TEXT_ROWS)
	{
	  vgaTextCursor.y--;
	  VGA_TM_ScrollUp(1);
	}
    }
}

void VGA_TM_CursorLF()
{
  vgaTextCursor.x = 0;
  vgaTextCursor.y++;

  if(vgaTextCursor.y >= VGA_TEXT_ROWS)
    {
      vgaTextCursor.y--;
      VGA_TM_ScrollUp(1);
    }
}

void VGA_TM_ClearScreen(VGAColor color)
{
  uint16_t offset = 0;
  for(int i=0; i<25*80; i++)
    {
      VGABase[offset] = (color << 12);
      offset++;
    }
}

void VGA_TM_PutChar(int c)
{
  uint16_t offset = 0;
  offset += vgaTextCursor.x;
  offset += vgaTextCursor.y * 80;

  uint8_t ascii = c & 0xFF;

  switch(ascii)
  {
    case '\n':
    {
      VGA_TM_CursorLF();
      break;
    }
    case '\b':
    {
      VGA_TM_Backspace();
      break;
    }
    default:
    {
      VGABase[offset] = 0x0F00 | c;
      VGA_TM_AdvanceCursor();
    }
  }
}

void VGA_TM_PutStringAtCursor(char *str, VGAColor fore, VGAColor back)
{
  size_t len = strlen(str);

  for(size_t i=0; i<len; i++)
    {
      VGA_TM_PutChar( (back << 12) | (fore << 8) | str[i] );
    }
}
