#pragma once

#include "procyon.h"
#include <stdio.h>

#include "memmgr.h"
#include "nodelist.h"

class Process;

typedef struct {
  List *message_queue;
  char name[64];
} IPC_MessagePort;

typedef struct {
  Node node;
  IPC_MessagePort *reply_port; // If we need to reply to this message,
                               //   this is where to direct replies.
  uint16_t length;             // Length of this message - set with sizeof(derived)
} IPC_Message;

IPC_Message *IPC_CreateMessage(uint16_t length, IPC_MessagePort *reply_to);
void IPC_SendMessage(IPC_Message *message, IPC_MessagePort *destination);
IPC_Message *IPC_GetMessage();
void IPC_DisposeMessage(IPC_Message *message);

IPC_MessagePort *IPC_CreatePort(const char *name);
