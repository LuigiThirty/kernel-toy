#pragma once

#include "procyon.h"
#include "fileio.h"

#define VT_ESC \022

extern DRIVE_LETTER current_drive;
extern uint8_t current_path[256];
extern uint8_t svga_console_active;

#define PATHSTRING_MAX 300

// TODO: find a better place for this
void ConstructPathString(char *output);
void DoSplashScreen();
