// Path functions.

#pragma once

#include <stdio.h>

#include "fileio.h"
#include "procyon.h"
#include "error.h"

#ifdef __cplusplus
extern "C" {
#endif

/** @brief Examines the input path and converts the drive letter to a DRIVE_LETTER.
 * 
 * @param path The path to examine
 * @return The drive index as a DRIVE_LETTER. Returns -1 if error.
 */
DRIVE_LETTER PATH_FindDriveIndex(CString path);

/** @brief Examines the input path. Returns the file name only.
 * 
 * @param buffer The buffer to return the filename in.
 * @param path The path to examine.
 */
void PATH_FindFileName(CString buffer, CString path);

/** @brief Examines the input path. Returns the directory and filename.
 */
void PATH_GetDirectory(CString buffer, CString path, size_t max_length);

#ifdef __cplusplus
}
#endif