#include <kerlib.h>
#include <memmgr.h>

KerlibManager kerlibManager;
Kerlib kernel_lib;

void TestFunc()
{
    printf("Test!\n");
}

KerlibFP kernel_functions[MAX_FNTABLE] = {
    { "TestFunc",       (void *)TestFunc },
    { "NewPtr",         (void *)_NewPtr },
    { "DisposePtr",     (void *)_DisposePtr },
};

void KerlibManager::InitKerlibManager()
{
    printf("Initializing Kerlib Manager\n");
    for(int i=0; i<MAX_KERLIBS; i++)
    {
        strcpy((char *)present_libraries[i].name, "");
        present_libraries[i].base = 0;
    }

    /* Set up the kernel lib */    
    strcpy((char *)present_libraries[0].name, "kernel");
    present_libraries[0].fp_table = kernel_functions;
}

uint32_t __SYS_dlsym(char *libname, char *fnname)
{
    return (uint32_t)kerlibManager.GetFnAddress(libname, fnname);
}

Handle __SYS_dlopen(const char *library_name)
{
    /* Resolve the function pointers... */
    return 0;
}