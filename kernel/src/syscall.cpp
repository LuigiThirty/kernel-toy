#include "syscall.h"
#include "vga/vgatext.h"
#include "fileio.h"
#include "globals.h"
#include "process.h"
#include "kerlib.h"

void call_dlopen()
{
    syscall_return_block->retval = (uint32_t)__SYS_dlopen((const char *)syscall_params_block->param1);
}

void _Syscall_IRQHandler(uint32_t id, uint32_t parm1, uint32_t parm2, uint32_t parm3)
{  
    // TODO: convert this to an actual dispatcher and not just a big dumbass switch statement
    switch(id)
    {
        case SYSCALL_write:
        {
            // parm1: fd | parm2: const char *buffer | parm3: length
            syscall_return_block->retval = __SYS_write((FileDescriptor)parm1, (const char *)parm2, (size_t)parm3);
            break;
        }
        case SYSCALL_read:
        {
            // parm1: fd | parm2: char *buffer | parm3: length
            syscall_return_block->retval = __SYS_read((FileDescriptor)parm1, (void *)parm2, (size_t)parm3);
            break;
        }
        case SYSCALL_dlopen:
            // parm1 : const char *library_name
            // retval: pointer to library struct
            call_dlopen();
            break;
        case SYSCALL_getcwd:
            __SYS_getcwd((char *)parm1, (size_t)parm2);
            break;
        case SYSCALL_dlsym:
            syscall_return_block->retval = __SYS_dlsym((char *)parm1, (char *)parm2);
            break;
    }
}