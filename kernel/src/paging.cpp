#include "paging.h"

uint32_t page_directory[1024] __attribute__((aligned(4096)));
uint32_t first_page_table[1024] __attribute__((aligned(4096)));

void PAGE_InitPageDirectory()
{
  for(int i=0; i<1024; i++)
    {
      // Page flags:
      // Kernel page
      // Read/write enabled
      // Page not present
      page_directory[i] = 0x00000002;
    }
  
  // holds the physical address where we want to start mapping these pages to.
  // in this case, we want to map these pages to the very beginning of memory.
  
  // this will fill 1024 entries in the table, mapping 4 megabytes
  for(int i = 0; i < 1024; i++)
    {
      // As the address is page aligned, it will always leave 12 bits zeroed.
      // Those bits are used by the attributes ;)

      // attributes: supervisor level, read/write, present
      uint32_t address = i << 12;
      first_page_table[i] = address | 3; 
    }

  // attributes: supervisor level, read/write, present
  page_directory[0] = ((unsigned int)first_page_table) | 3;

  printf("page_directory: %08X\n", page_directory);
  printf("first_page_table: %08X\n", first_page_table);

  PAGE_LoadPageDirectory((unsigned int *)page_directory);
}

CPTR PAGE_CreateXMSPage(uint32_t size)
{
  // Returns some XMS for processes other than the kernel.
  // The kernel and its heap and stack live in conventional memory (for now)
  // so other processes will have unique areas above 1MB.

  // TODO: paging... just set up a page at 0x100000
  
  CPTR p = (CPTR)0x100000;

  printf("Allocated an XMS page: %08X\n", p);
  return p;
}
