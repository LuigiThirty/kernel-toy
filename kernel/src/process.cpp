#include "process.h"
#include "serial.h"

ProcessManager gProcessManager;

Process::Regs386 PM_SavedRegs;

void ProcessManager::Initialize()
{
  gProcessManager.AllProcesses = (List *)PROCESS_LIST_AREA;
  gProcessManager.AllProcesses->lh_Head = NULL;
  gProcessManager.AllProcesses->lh_Tail = NULL;
  gProcessManager.AllProcesses->lh_Type = LT_Process;

  gProcessManager.RunningProcess = NULL;
}

void ProcessManager::QuantumExpired()
{
  // First, save the registers.
  PM_SaveRegisters();

  // printf("Registers saved to %08X\n", &PM_SavedRegs);
  if(gProcessManager.RunningProcess == NULL)
  {
    printf("RunningProcess is NULL.");
    
    gProcessManager.RunningProcess = (Process *)gProcessManager.AllProcesses->lh_Head;

    PM_SavedRegs.EAX = gProcessManager.RunningProcess->registers.EAX;
    PM_SavedRegs.EBX = gProcessManager.RunningProcess->registers.EBX;
    PM_SavedRegs.ECX = gProcessManager.RunningProcess->registers.ECX;
    PM_SavedRegs.EDX = gProcessManager.RunningProcess->registers.EDX;

    PM_SavedRegs.EBP = gProcessManager.RunningProcess->registers.EBP;
    PM_SavedRegs.ESP = gProcessManager.RunningProcess->registers.ESP;
    PM_SavedRegs.ESI = gProcessManager.RunningProcess->registers.ESI;
    PM_SavedRegs.EDI = gProcessManager.RunningProcess->registers.EDI;

    PM_SavedRegs.CS = gProcessManager.RunningProcess->registers.CS;
    PM_SavedRegs.DS = gProcessManager.RunningProcess->registers.DS;
    PM_SavedRegs.ES = gProcessManager.RunningProcess->registers.ES;
    PM_SavedRegs.FS = gProcessManager.RunningProcess->registers.FS;
    PM_SavedRegs.GS = gProcessManager.RunningProcess->registers.GS;

    PM_SavedRegs.EFLAGS = gProcessManager.RunningProcess->registers.EFLAGS;
    PM_SavedRegs.ENTRY_POINT = gProcessManager.RunningProcess->registers.ENTRY_POINT;

    // Change AppHeap to the new process' heap base.
    
    PM_RestoreRegisters();
    return;
  }

  // Then, copy the saved registers to process->registers.
  gProcessManager.RunningProcess->registers.EAX = PM_SavedRegs.EAX;
  gProcessManager.RunningProcess->registers.EBX = PM_SavedRegs.EBX;
  gProcessManager.RunningProcess->registers.ECX = PM_SavedRegs.ECX;
  gProcessManager.RunningProcess->registers.EDX = PM_SavedRegs.EDX;
  gProcessManager.RunningProcess->registers.EBP = PM_SavedRegs.EBP;
  gProcessManager.RunningProcess->registers.ESP = PM_SavedRegs.ESP;
  gProcessManager.RunningProcess->registers.ESI = PM_SavedRegs.ESI;
  gProcessManager.RunningProcess->registers.EDI = PM_SavedRegs.EDI;
  
  gProcessManager.RunningProcess->registers.CS = PM_SavedRegs.CS;
  gProcessManager.RunningProcess->registers.DS = PM_SavedRegs.DS;
  gProcessManager.RunningProcess->registers.ES = PM_SavedRegs.ES;
  gProcessManager.RunningProcess->registers.FS = PM_SavedRegs.FS;
  gProcessManager.RunningProcess->registers.GS = PM_SavedRegs.GS;

  gProcessManager.RunningProcess->registers.EFLAGS = PM_SavedRegs.EFLAGS;
  gProcessManager.RunningProcess->registers.ENTRY_POINT = PM_SavedRegs.ENTRY_POINT;

  // Do stuff...
  
  // Anything in the process list that's ready?
  //printf("Scheduler: Swapping to next process.\n");
  Process *current = (Process *)gProcessManager.RunningProcess->node.ln_Succ;
  bool foundProcessToRun = false;

  gProcessManager.RunningProcess->status = Process::PROCESS_READY;
  //printf("Switching process %08X to READY\n", gProcessManager.RunningProcess);
  
  while(foundProcessToRun == false)
  {
    
    if(current == NULL)
    {
      // If we ran out of processes, wrap to the beginning of the list.
      current = (Process *)gProcessManager.AllProcesses->lh_Head;
      continue;
    }

    // If a process is ready, swap to it.
    else if(current->status == Process::PROCESS_READY)
    {
      current->status = Process::PROCESS_RUNNING;
      gProcessManager.RunningProcess = current;

      foundProcessToRun = true;
    }

    // If we didn't find a process, advance to the next process in the list.
    current = (Process *)current->node.ln_Succ;

    // Worst case: We swap back to the original process.
  }
  
  // Restore PM_SavedRegs from gProcessManager.RunningProcess.
  PM_SavedRegs.EAX = gProcessManager.RunningProcess->registers.EAX;
  PM_SavedRegs.EBX = gProcessManager.RunningProcess->registers.EBX;
  PM_SavedRegs.ECX = gProcessManager.RunningProcess->registers.ECX;
  PM_SavedRegs.EDX = gProcessManager.RunningProcess->registers.EDX;

  PM_SavedRegs.EBP = gProcessManager.RunningProcess->registers.EBP;
  PM_SavedRegs.ESP = gProcessManager.RunningProcess->registers.ESP;
  PM_SavedRegs.ESI = gProcessManager.RunningProcess->registers.ESI;
  PM_SavedRegs.EDI = gProcessManager.RunningProcess->registers.EDI;

  PM_SavedRegs.CS = gProcessManager.RunningProcess->registers.CS;
  PM_SavedRegs.DS = gProcessManager.RunningProcess->registers.DS;
  PM_SavedRegs.ES = gProcessManager.RunningProcess->registers.ES;
  PM_SavedRegs.FS = gProcessManager.RunningProcess->registers.FS;
  PM_SavedRegs.GS = gProcessManager.RunningProcess->registers.GS;

  PM_SavedRegs.EFLAGS = gProcessManager.RunningProcess->registers.EFLAGS;
  PM_SavedRegs.ENTRY_POINT = gProcessManager.RunningProcess->registers.ENTRY_POINT;

  // Change AppHeap to the new process' heap base.
  
  PM_RestoreRegisters();
  
  return;
}

Pid ProcessManager::CreateProcess(const char *name,
        const char *run_directory,
			  Process::EntryPoint entry_point,
			  CPTR stack_pointer,
        uint32_t memory_region_start,
        uint32_t memory_region_size)
{
  // TODO: Dynamically assign stack areas and memory regions based on size requirements
  // rather than specifying pointers literally in the function call

  static int pid = 1;
  
  Handle hProcess = gMemoryManager.NewHandle(sizeof(Process), H_SYSHEAP);
  Process *process = (Process *)*hProcess; //(Process *)gMemoryManager.NewPtr(sizeof(Process), H_SYSHEAP);
  
  process->pid = ++pid;
  process->status = Process::PROCESS_READY;
  strcpy(process->name, name);
  strcpy(process->working_directory, run_directory);

  process->port = IPC_CreatePort(name);
  
  process->registers.ENTRY_POINT = (CPTR)entry_point;

  process->registers.EAX = 0;
  process->registers.EBX = 0;
  process->registers.ECX = 0;
  process->registers.EDX = 0;

  process->registers.EBP = 0;
  process->registers.ESP = (uint32_t)stack_pointer;
  process->registers.ESI = 0;
  process->registers.EDI = 0;

  process->registers.CS = FLAT_CODE_SELECTOR;
  process->registers.DS = FLAT_DATA_SELECTOR;
  process->registers.ES = 0;
  process->registers.FS = 0;
  process->registers.GS = 0;

  process->registers.EFLAGS = 0x200;

  gMemoryManager.InitHeap(&(process->app_heap), (CPTR)memory_region_start, (CPTR)(memory_region_start+memory_region_size));

  // TODO: Use process handles instead of process pointers
  LIST_AddHead(gProcessManager.AllProcesses, (Node *)process);

  char buf[512];
  sprintf(buf, "Created process %08X '%s' (PID %d)\r\n", process, process->name, process->pid);
  COM1.PutStr(buf);
  
  return pid;
}

bool ProcessManager::SetForegroundProcess(Pid pid)
{
  ReceivingIOProcess = FindProcessByPID(pid);
  printf("Set foreground process to PID %d. Its message port is %08X\n", pid, ReceivingIOProcess->port);
  return 1;
}

Process * ProcessManager::FindProcessByName(char *name)
{
  Process *current = (Process *)gProcessManager.AllProcesses->lh_Head;
  
  while(current != NULL)
    {
      if(strcmp(current->name, name) == 0)
	      return current;

      current = (Process *)current->node.ln_Succ;
    }

  return NULL;
}

Process * ProcessManager::FindProcessByPID(Pid pid)
{
  Process *current = (Process *)gProcessManager.AllProcesses->lh_Head;
  
  while(current != NULL)
    {
      if(current->pid == pid)
	      return current;

      current = (Process *)current->node.ln_Succ;
    }

  return NULL;
}

IPC_MessagePort * ProcessManager::FindMessagePortByPID(Pid pid)
{

  Process *current = (Process *)gProcessManager.AllProcesses->lh_Head;
  
  while(current != NULL)
    {
      if(current->pid == pid)
      {
	      return current->port;
      }

      current = (Process *)current->node.ln_Succ;
    }

  return NULL;
}

bool ProcessManager::SendSignal(uint32_t pid, Signal signal)
{
  Process *p = ProcessManager::FindProcessByPID(pid);

  if(p != NULL)
    {
      p->signals.received |= signal;
      return true;
    }

  return false;
}

void __SYS_getcwd(char *buf, size_t size)
{
  auto process = gProcessManager.GetRunningProcess();
  strncpy(buf, process->working_directory, size);
}
