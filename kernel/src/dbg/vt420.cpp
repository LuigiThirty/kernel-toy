#include "dbg/vt420.h"

void VT420::CursorUp(uint8_t lines)
{
    COM1_PRINTF("%c%dA", CSI, lines);
}

void VT420::CursorDown(uint8_t lines)
{
    COM1_PRINTF("%c%dB", CSI, lines);
}

void VT420::CursorHome()
{
    COM1_PRINTF("%c1;1H", CSI);
}

void VT420::SaveCursorPosition()
{
    COM1_PRINTF("%c7", ESC);
}

void VT420::RestoreCursorPosition()
{
    COM1_PRINTF("%c8", ESC);
}

void VT420::HVPos(uint8_t x, uint8_t y)
{
    COM1_PRINTF("%c%d;%dH", CSI, y, x);
}

void VT420::SetTopAndBottomMargins(uint8_t top, uint8_t bottom)
{
    // DECSTBM
    COM1_PRINTF("%c%d;%dr", CSI, top, bottom);
}

void VT420::EnableLineDrawingChars()
{
    COM1_PRINTF("%c)0", CSI);   // DEC Special Graphic set is now G1
    COM1_PRINTF("%c~", ESC);    // G1 is now in GR
}

void VT420::DrawHorizontalLine(uint8_t row)
{
    SaveCursorPosition();
    HVPos(0, row);
    COM1_PRINTF("%c(0", ESC);   // Lowercase ASCII -> box-drawing chars
    for(int i=0; i<80; i++)
    {
        COM1_PRINTF("%c", 0x71);
    }
    COM1_PRINTF("%c(B", ESC);   // Restore lowercase ASCII
    RestoreCursorPosition();
}