#pragma once

#include "globals.h"
#include "stdio.h"
#include "serial.h"

class VT420
{
    private:
    // 8-bit control characters
    static const uint8_t HT    = 0x09; // Horizontal Tab
    static const uint8_t LF    = 0x0A; // Line Feed
    static const uint8_t VT    = 0x0B; // Vertical Tab
    static const uint8_t FF    = 0x0C; // Form Feed
    static const uint8_t CR    = 0x0D; // Carriage Return
    static const uint8_t SO    = 0x0E; // Shift Out
    static const uint8_t SI    = 0x0F; // Shift In
    static const uint8_t DC1   = 0x11; // XON
    static const uint8_t DC3   = 0x13; // XOFF
    static const uint8_t DC4   = 0x14; // SSU Command Follows
    static const uint8_t CAN   = 0x18; // Cancel Sequence
    static const uint8_t SUB   = 0x1A; // Cancel Sequence
    static const uint8_t ESC   = 0x1B; // Escape Sequence
    static const uint8_t DEL   = 0x7F; // Unused

    static const uint8_t IND   = 0x84; // Index
    static const uint8_t NEL   = 0x85; // Next Line
    static const uint8_t HTS   = 0x88; // Set Horizontal Tab
    static const uint8_t RI    = 0x8D; // Reverse Index
    static const uint8_t SS2   = 0x8E; // Single Shift 2
    static const uint8_t SS3   = 0x8F; // Single Shift 3
    static const uint8_t DCS   = 0x90; // Device Control String
    static const uint8_t SOS   = 0x98; // Unused
    static const uint8_t DECID = 0x9A; // DEC Private Identification
    static const uint8_t CSI   = 0x9B; // Control Sequence Introducer
    static const uint8_t ST    = 0x9C; // String Terminator
    static const uint8_t OSC   = 0x9D; // OS Command
    static const uint8_t PM    = 0x9E; // Privacy Message
    static const uint8_t APC   = 0x9F; // Application Program Command

    public:
    static void CursorUp(uint8_t lines);
    static void CursorDown(uint8_t lines);
    static void CursorHome();
    static void SaveCursorPosition();
    static void RestoreCursorPosition();

    static void HVPos(uint8_t x, uint8_t y);

    static void SetTopAndBottomMargins(uint8_t top, uint8_t bottom);

    static void EnableLineDrawingChars();

    static void DrawHorizontalLine(uint8_t row);
};
