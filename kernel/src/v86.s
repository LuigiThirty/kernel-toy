[CPU 386]
[BITS 32]

[BITS 16]

global _Cirrus_VGAType
_Cirrus_VGAType:
   mov ah, 012h
   mov al, 0
   mov bl, 080h
   int 10h

   mov dx, 0
   mov es, dx
   mov dx, 08BFFh
   mov di, dx
   mov [es:di], ax

   int 021h

global _VBE_GetControllerInfo
_VBE_GetControllerInfo:
   ; Performs a GetControllerInfo call on a VESA-compliant video card.
   ; The table will be stored in 08C00h.
   mov ax, 04F00h
   mov cx, 05Fh

   mov dx, 0
   mov es, dx
   mov di, 8C00h

   mov bx, 'V'
   mov [es:di], bx
   mov bx, 'B'
   mov [es:di + 1], bx
   mov bx, 'E'
   mov [es:di + 2], bx
   mov bx, '2'
   mov [es:di + 3], bx

   int 10h  ; 0:8C00h will now have a VESA mode info block

   ; Get VESA info
   mov ax, 04F01h
   mov cx, 05Fh

   mov dx, 0
   mov es, dx
   mov di, 8800h
   int 10h  ; 0:8800h will now have a VESA mode info block

   int 021h

global _SVGA_Mode_640x480x256
_SVGA_Mode_640x480x256:
   ; Switch to 640x480 256-color SVGA
   mov ax, 04F01h
   mov cx, 05Fh

   mov dx, 0
   mov es, dx
   mov di, 8800h
   int 10h  ; 0:8800h will now have a VESA mode info block

   mov ax, 04F01h
	mov ax, 05Fh
	int 10h

	int 021h  ; Exit

global _SVGA_Mode_80Col
_SVGA_Mode_80Col:
   ; Get VESA info
   mov ax, 04F01h
   mov cx, 05Fh

   mov dx, 0
   mov es, dx
   mov di, 8800h
   int 10h  ; 0:8800h will now have a VESA mode info block

   ; Switch to 640x480 256-color SVGA
	mov ax, 03h
	int 10h

	int 021h  ; Exit

global _SVGA_Mode_13h
_SVGA_Mode_13h:
   ; Get VESA info
   mov ax, 04F01h
   mov cx, 05Fh

   mov dx, 0
   mov es, dx
   mov di, 8800h
   int 10h  ; 0:8800h will now have a VESA mode info block

   ; Switch to 640x480 256-color SVGA
	mov ax, 13h
	int 10h

	int 021h  ; Exit

.forever:
	jmp .forever
	