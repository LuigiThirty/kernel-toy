/* Shared libraries managed by the kernel. */

#include <procyon.h>
#include <stdio.h>
#include <string.h>

#define MAX_KERLIBS 4
#define MAX_FNTABLE 4

typedef struct kerlib_fp_t
{
    char        name[32];
    void *      fp;
}   KerlibFP;

typedef struct kerlib_t
{
    char        name[32];       /* Library name */
    void *      base;           /* Base address */
    KerlibFP *  fp_table;      /* Library's table of function pointers */
} Kerlib;

extern Kerlib kernel_lib;

class KerlibManager
{
    Kerlib present_libraries[MAX_KERLIBS];

    public:
    void    InitKerlibManager();
    void *  GetFnAddress(char *libname, char *fnname)
    {
        Kerlib lib = present_libraries[0]; // TODO
        for(int i=0; i<MAX_FNTABLE; i++)
        {
            KerlibFP *table = lib.fp_table;

            if(strcmp(table[i].name, fnname) == 0)
            {
                return table[i].fp;
            }
        }
        printf("Couldn't match %s\n", fnname);
        return NULL;
    }
};

extern KerlibManager kerlibManager;

uint32_t __SYS_dlsym(char *libname, char *fnname);
Handle __SYS_dlopen(const char *library_name);