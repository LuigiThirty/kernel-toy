#ifndef ELF_H
#define ELF_H

#include <stdlib.h>
#include <stdio.h>

#include "memmgr.h"

#define ELF32_ST_BIND(INFO)	((INFO) >> 4)
#define ELF32_ST_TYPE(INFO)	((INFO) & 0x0F)

// Reserved section names
#define SHN_UNDEF        0x00
#define SHN_LORESERVE    0xFF00
#define SHN_LOPROC       0xFF00
#define SHN_HIPROC       0xFF1F
#define SHN_ABS          0xFFF1
#define SHN_COMMON       0xFFF2
#define SHN_HIRESERVE    0xFFFF

#define DO_386_32(S, A)	((S) + (A))
#define DO_386_PC32(S, A, P)	((S) + (A) - (P))
#define ELF_RELOC_ERR -1

#define ELF32_R_SYM(INFO)	((INFO) >> 8)
#define ELF32_R_TYPE(INFO)	((uint8_t)(INFO))

class ELFManager
{
  public:
  #pragma pack (push,1)
  enum RtT_Types {
    R_386_NONE		= 0, // No relocation
    R_386_32		  = 1, // Symbol + Offset
    R_386_PC32		= 2, // Symbol + Offset - Section Offset
    R_386_JMP_SLOT= 7, // Symbol
    R_386_RELATIVE= 8, // Base Addr + Addend
  };

  /* https://en.wikipedia.org/wiki/Executable_and_Linkable_Format */
  typedef struct elf_ident_t {
    uint8_t magic[4];
    uint8_t clazz;
    uint8_t data;
    uint8_t version;
    uint8_t osabi;
    uint8_t abi_version;
    uint8_t padding[7];
  } ELF_Ident;

  typedef struct elf_header_t {
    struct elf_ident_t e_ident;
    uint16_t e_type;
    uint16_t e_machine;
    uint32_t e_version;
    uint32_t e_entry;
    uint32_t e_phoff;
    uint32_t e_shoff;
    uint32_t e_flags;
    uint16_t e_ehsize;
    uint16_t e_phentsize;
    uint16_t e_phnum;
    uint16_t e_shentsize;
    uint16_t e_shnum;
    uint16_t e_shstrndx;
  } ELF_Header;

  typedef struct elf_program_header_t {
    uint32_t p_type;
    uint32_t p_offset;
    uint32_t p_vaddr;
    uint32_t p_paddr;
    uint32_t p_filesz;
    uint32_t p_memsz;
    uint32_t p_flags;
    uint32_t p_align;
  } ELF_ProgramHeader;

  typedef struct elf_section_header_t {
    uint32_t sh_name;
    uint32_t sh_type;
    uint32_t sh_flags;
    uint32_t sh_addr;
    uint32_t sh_offset;
    uint32_t sh_size;
    uint32_t sh_link;
    uint32_t sh_info;
    uint32_t sh_addralign;
    uint32_t sh_entsize;
  } ELF_SectionHeader;

  // Relocation
  typedef struct elf_rel_t {
    void *    r_offset;
    uint32_t  r_info;
  } ELF_REL;

  typedef struct elf_rela_t {
    void *r_offset;
    uint32_t r_info;
    int32_t r_addend;
  } ELF_RELA;

  typedef struct {
    uint32_t st_name;
    void *st_value;
    uint32_t st_size;
    uint8_t st_info;
    uint8_t st_other;
    uint16_t st_shndx;
  } Elf32_Sym;

  enum StT_Bindings {
    STB_LOCAL		= 0, // Local scope
    STB_GLOBAL	= 1, // Global scope
    STB_WEAK		= 2  // Weak, (ie. __attribute__((weak)))
  };
 
  enum StT_Types {
    STT_NOTYPE	= 0, // No type
    STT_OBJECT	= 1, // Variables, arrays, etc.
    STT_FUNC		= 2  // Methods or functions
  };

  typedef enum {
    SHT_NULL,
    SHT_PROGBITS,
    SHT_SYMTAB,
    SHT_STRTAB,
    SHT_RELA,
    SHT_HASH,
    SHT_DYNAMIC,
    SHT_NOTE,
    SHT_NOBITS,
    SHT_REL,
    SHT_SHLIB,
    SHT_DYNSIM,
  } ELF_SHTYPE;

  typedef enum {
    PT_NULL,
    PT_LOAD,
    PT_DYNAMIC,
    PT_INTERP,
    PT_NOTE,
    PT_SHLIB,
    PT_PHDR,
    PT_LOPROC,
    PT_HIPROC,
    PT_GNU_STACK,
  } ELF_PTYPE;

  // Functions
  private:
  static  void JSRProgram(void *);
  static  void Relocate(ELF_RELA relocation,
				  ELF_SectionHeader *sh_table,
				  void *load_addr);
  static  void RunProgram(void *entry);

  static  int Relocate_REL(void *load_base, ELF_Header *hdr, ELF_REL *relocation, ELF_SectionHeader *reltab);

  static ELF_SectionHeader * elf_sheader(ELF_Header *hdr);
  static ELF_SectionHeader * elf_section(void *load_base, ELF_Header *hdr, int idx);
  static int elf_get_symval(void *load_base, ELF_Header *hdr, int table, int idx);

  public:
  static int16_t LoadExecutable(char *buffer);
  static int ELF_LoadDylib(char *filename);
};

// CALL the entry point

// Errors
#define ELF_ERROR_BAD_MAGIC_NUMBER -1
#define ELF_ERROR_NOT_32_BIT       -2
#define ELF_ERROR_WRONG_ENDIANNESS -3


#pragma pack (pop)

#endif
