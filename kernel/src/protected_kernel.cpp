#include "protected_kernel.h"
#include "nodelist.h"

#include "idt.h"
#include "paging.h"
#include "process.h"
#include "drivers/public/floppydisk.h"
#include "svga.h"

#include "fastgraf/fastgraf.h"
#include "vga_font_8x16.h"
#include "svgaconsole.h"
#include "utility.h"
#include "serial.h"
#include "dbg/vt420.h"
#include <unistd.h>

uint32_t kernel_pid;

void TestProcessSwitching();
void KernelProcess();

Process::TSS_STRUCT *kernel_tss = (Process::TSS_STRUCT *)KERNEL_TSS_AREA;
Process::TSS_STRUCT *v86_tss = (Process::TSS_STRUCT *)V86_TSS_AREA;

void Kernel_PutChar(unsigned char c)
{
  if(svga_console_active)
    SCON_TeletypeWrite(&svga_console_state, c);
  else
  {
    VGA_TM_PutChar(c);
  }
}

void _PModeStart()
{
  FD_InitIO();

  COM1.io_port = 0x3F8;
  COM1.SetBaudRate(SerialPort::BAUD_9600);
  COM1.SetLCR(0x03); // 8N1
  COM1.SetFCR(0xC7); // FIFO on, 14-byte threshold
  COM1.SetMCR(0x03); // DTR/RTS on
  COM1.PutStr("\033[2J"); // clear screen
  COM1.PutStr("\033[H");  // reset cursor
  COM1.PutStr("COM 0x3F8\r\n");

  // Set up the VT420 for debugging
  VT420::SetTopAndBottomMargins(3, 34);
  VT420::EnableLineDrawingChars();
  VT420::DrawHorizontalLine(2);
  VT420::DrawHorizontalLine(35);

  VT420::HVPos(0, 36);
  COM1.PutStr("Procyon/386 Debug Console");
  VT420::HVPos(0, 0);
  COM1.PutStr("Hello, Twitter! - @LuigiThirty");
  VT420::HVPos(0, 3);

  svga_console_active = false;

  asm("cli");
  // Remap IRQs to vectors 0x20 to 0x2F
  PIC_Remap(0x20, 0x28);
  PIC_ClearMask(0);
  PIC_ClearMask(6);
  PIC_ClearMask(1);
  asm("sti");

  COM1.PutStr("Initializing memory manager... ");
  gMemoryManager.Initialize();
  COM1.PutStr("OK\r\n");

  VGA_TM_ClearScreen(VGA_COLOR_BLACK);
  
  memset((uint8_t *)V86_TSS_AREA, 0, 256);
  v86_tss->esp0   = 0x18FFF;
  v86_tss->ss0    = 0x10;     // SS is a data segment
  v86_tss->ss     = 0x10;
  v86_tss->eflags = 0x20202;
  v86_tss->cs     = 0x08;
  v86_tss->ds     = 0x10;

  //SVGA_DetectVideoCard();

  /*
  // Initialize the SVGA subsystem.
  COM1.PutStr("Initializing SVGA... ");
  SVGA_Init();
  COM1.PutStr("OK\r\n");

  COM1.PutStr("Clearing screen with blitter... ");
  fastgraf_state.outputBitmap.width = 640;
  fastgraf_state.outputBitmap.height = 480;
  fastgraf_state.outputBitmap.pixels = FB_DIRECT;

  FG_SetBackPenColor(15);
  FG_Blit(&fastgraf_state.outputBitmap, (FG_Point){0, 0}, NULL, (FG_Point){0, 0}, (FG_Point){640, 480}, ROP_CLEAR, BF_NONE);
  COM1.PutStr("OK\r\n");

  // FG_Rect bg;
  // FG_SetForePenColor(15);
  // FG_SetRect(&bg, 0, 0, 640, 480);
  // FG_PaintRect(&bg);

  FG_Bitmap test_image;
  FG_Font font;
  font.characters = IBM_VGA_8x16;
  font.glyph_height = 16;
  font.glyph_width = 8;

  SCON_Init(&svga_console_state, &font, &fastgraf_state.outputBitmap);

  COM1.PutStr("SVGA initialized\r\n");
  printf("SVGA console initialized\n");
  */

  COM1.PutStr("Printing splash screen...\r\n");
  DoSplashScreen();

  COM1.PutStr("Initializing driver manager...\r\n");
  printf("Initializing driver manager...\r\n");
  DRVMGR_InitializeDriverList();

  gFAT12Manager.ReadBPB(DRIVE_A, (uint8_t*)BPB_AREA);
  gFAT12Manager.PrintBPBInfo(DRIVE_A);

  // PAGE_InitPageDirectory();
  // PAGE_CreateXMSPage(1024*128);
  // PAGE_EnablePaging();

  gProcessManager.Initialize();

  FDC_Reset();

  /*
  FG_SetBackPenColor(11);
  FG_Blit(&fastgraf_state.outputBitmap, (FG_Point){40, 40}, NULL, (FG_Point){0, 0}, (FG_Point){50, 50}, ROP_CLEAR, BF_NONE);
  FG_SetBackPenColor(12);
  FG_Blit(&fastgraf_state.outputBitmap, (FG_Point){50, 50}, NULL, (FG_Point){0, 0}, (FG_Point){50, 50}, ROP_CLEAR, BF_NONE);
  FG_SetBackPenColor(13);
  FG_Blit(&fastgraf_state.outputBitmap, (FG_Point){60, 60}, NULL, (FG_Point){0, 0}, (FG_Point){50, 50}, ROP_CLEAR, BF_NONE);
  FG_SetBackPenColor(15);
  FG_BlitFont(&fastgraf_state.outputBitmap, POINT(100, 100), &font, 0x41);
  */

  // gd5446_blitter->BLT_Width = 100;
  // gd5446_blitter->BLT_Height = 100;
  // gd5446_blitter->BLT_SourcePitch = 640;
  // gd5446_blitter->BLT_DestPitch = 640;
  // gd5446_blitter->BLT_DestAddress = 0;
  // gd5446_blitter->BLT_SourceAddress0 = 0;
  // gd5446_blitter->BLT_SourceAddress1 = 0;
  // gd5446_blitter->BLT_SourceAddress2 = 0;
  // gd5446_blitter->BLT_Mode = 0;
  // gd5446_blitter->BLT_RasterOP = 0x0;
  // gd5446_blitter->BLT_ModeExtensions = 0;
  // gd5446_blitter->BLT_Ctrl = 0x02;

  FG_SetForePenColor(0);

  kerlibManager.InitKerlibManager();

  printf("Starting kernel task\n");
  kernel_pid = gProcessManager.CreateProcess("kernel_task", "A:\\", KernelProcess, (CPTR)0x22FFFF, 0x220000, 0x8000);

  printf("Created two processes. Calling PM_QuantumExpired!\n");
  gProcessManager.QuantumExpired();

  //printf("Paging is enabled - lower 4MB is mapped to its physical address.\n");
  //printf("Writing above 4MB - this should cause a page fault.\n");
  //MMIO8(0x400000) = 0xFF;
  
  printf("Halting system.\n"); 
  return;
}

void KernelProcess()
{
  asm("sti");
  printf("Beginning kernel process...\n");

  printf("Initializing globals...\n");
  current_drive = DRIVE_A;

  printf("Loading keyboard driver... ");
  DeviceDriverListEntry *kb_driver = DRVMGR_FindDriver("pc_keyboard");
  auto kb_pid = gProcessManager.CreateProcess("pc_keyboard", "A:\\", kb_driver->entryPoint, (CPTR)0x210000, 0x200000, 0x8000);
  printf("OK. PID is %d\n", kb_pid);

  printf("Loading floppy driver... ");
  DeviceDriverListEntry *fdd_driver = DRVMGR_FindDriver("floppydisk");
  auto fdd_pid = gProcessManager.CreateProcess("floppydisk", "A:\\", fdd_driver->entryPoint, (CPTR)0x220000, 0x210000, 0x8000);
  printf("OK. PID is %d\n", fdd_pid);

  printf("Loading shell... ");
  Pid shell_pid = gProcessManager.CreateProcess("shell", "A:\\", CMD_PromptLoop, (CPTR)0x230000, 0x220000, 0x8000);
  gProcessManager.SetForegroundProcess(shell_pid);
  printf("OK. PID is %d\n", shell_pid);

  while(true)
  {
    // Do kernelly things here.
    gProcessManager.QuantumExpired();
  }

}