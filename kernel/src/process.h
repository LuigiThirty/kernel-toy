#pragma once

// Mnemonic: PM == Process Manager

#include "procyon.h"
#include "nodelist.h"
#include "messages.h"

#include "signal.h"

typedef uint32_t Pid;

#define FLAT_CODE_SELECTOR 0x8
#define FLAT_DATA_SELECTOR 0x10

class Process {

  public:

  #pragma pack(push,1)
  typedef struct {
    uint32_t EAX;
    uint32_t EBX;
    uint32_t ECX;
    uint32_t EDX;
    uint32_t EBP;
    uint32_t ESP;
    uint32_t ESI;
    uint32_t EDI;

    uint16_t CS;
    uint16_t SS;
    uint16_t DS;
    uint16_t ES;
    uint16_t FS;
    uint16_t GS;

    uint32_t EFLAGS;

    // The return address in the stack is set to this value.
    CPTR ENTRY_POINT;
    
  } Regs386;

  struct TSS_STRUCT {
    uint32_t back_link;
    uint32_t esp0, ss0;
    uint32_t esp1, ss1;
    uint32_t esp2, ss2;
    uint32_t cr3;
    uint32_t eip;
    uint32_t eflags;
    uint32_t eax,ecx,edx,ebx;
    uint32_t esp, ebp;
    uint32_t esi, edi;
    uint32_t es, cs, ss, ds, fs, gs;
    uint32_t ldt;
    uint32_t trace_bitmap;
  };
  #pragma pack(pop)

  typedef void (*EntryPoint)();

  typedef enum {
	      PROCESS_STOPPED,
	      PROCESS_RUNNING,
	      PROCESS_READY,
	      PROCESS_BLOCKED
  } Status;

  typedef struct io_streams_t {
    char *stdin;
    char *stdout;
    char *stderr;
  } io_streams;

  Node node;
  Regs386 registers;
  CPTR entry_point;
  CPTR stack_address;
  MemoryManager::Heap app_heap;
  Status status;
  char name[64];
  uint32_t pid;
  PM_SignalBlock signals;
  IPC_MessagePort *port;
  io_streams streams;
  char working_directory[64];
};

extern Process::Regs386 PM_SavedRegs; // where a process will save its registers on quantum expiration.

class ProcessManager {
  List * AllProcesses;
  Process * RunningProcess;

  typedef struct
  {
    bool in_use;

    private:
      Process data;
  } ProcessHandle;

  ProcessHandle availablePids[64];

  // Which process is receiving I/O messages? (Foreground process? Active process?)
  Process * ReceivingIOProcess;

  public:  
    void Initialize();

    // The scheduler timeslice expired.
    void QuantumExpired();
    void Yield() { QuantumExpired(); }

    Pid GetForegroundProcess() { return ReceivingIOProcess->pid; }
    bool SetForegroundProcess(Pid pid);

    // Signal functions.
    bool SendSignal(Pid pid, Signal signal);

    // Lookup functions.
    Process * FindProcessByPID(Pid pid);
    IPC_MessagePort *FindMessagePortByPID(Pid pid);
    Process * FindProcessByName(char *name);

    uint32_t CreateProcess(const char *name,
      const char *run_directory,
      Process::EntryPoint entry_point,
      CPTR stack_pointer,
      uint32_t memory_region_start,
      uint32_t memory_region_size);

    List * GetProcessList() { return AllProcesses; }
    Process * GetRunningProcess() { return RunningProcess; }
};
extern ProcessManager gProcessManager;

extern "C"
{
  // Save the registers to PM_SavedRegs.
  extern void PM_SaveRegisters();
  extern void PM_RestoreRegisters();

  void __SYS_getcwd(char *buf, size_t size);
}