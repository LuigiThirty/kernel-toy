#include "console.h"

void Console::Putchar(uint8_t c)
{
    if(svga_console_active)
        SCON_TeletypeWrite(&svga_console_state, c);
    else
    {
        VGA_TM_PutChar(c);
    }
}