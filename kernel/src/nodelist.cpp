#include "nodelist.h"


List * LIST_CreateList(ListType type)
{
  List *list = (List *)gMemoryManager.NewPtr(sizeof(List), H_SYSHEAP);

  list->lh_Head = NULL;
  list->lh_Tail = NULL;
  list->lh_Type = type;
  
  return list;
}

Node * LIST_AllocNode()
{
  return (Node *)malloc(sizeof(Node));
}

void LIST_Init(List *list, ListType type)
{
  list->lh_Head = NULL;
  list->lh_Tail = NULL;
  list->lh_Type = type;
}

void LIST_AddHead(List *list, Node *node)
{
  if(list->lh_Head != NULL) node->ln_Succ = list->lh_Head;
  node->ln_Pred = NULL;
  list->lh_Head = node;
}

void LIST_AddTail(List *list, Node *node)
{
  if(list->lh_Head != NULL)
  {
    Node *current = list->lh_Head;

    while(current->ln_Succ != NULL)
      current = current->ln_Succ;
    
    node->ln_Pred = current;
    node->ln_Succ = NULL;
  }
  else
  {
    LIST_AddHead(list, node);
  }
}

Node *LIST_RemHead(List *list)
{
  Node *head = list->lh_Head;
  list->lh_Head = list->lh_Head->ln_Succ;
  return head;
}

bool LIST_IsEmpty(List *list)
{
  return list->lh_Head == NULL;
}

/*
void LIST_Init(List *list, ListType type)
{
  // An empty list loops on itself.
  list->lh_Head = (Node *)((long)list + 4);
  list->lh_Tail = NULL;
  list->lh_TailPred = (Node *)((long)list);
  list->lh_Type = type;
}
*/

/*
void LIST_AddHead(List *list, Node *node)
{
  node->ln_Succ = list->lh_Head;
  node->ln_Succ->ln_Pred = node;
  list->lh_Head = node;
}

void LIST_AddTail(List *list, Node *node)
{
  Node *oldTail = list->lh_TailPred->ln_Succ;

  oldTail->ln_Succ = node;
  node->ln_Pred = oldTail;
  node->ln_Succ = &(list->lh_Tail);
  list->lh_TailPred = node;

  printf("ln_Succ: %X\n", node->ln_Succ);
}

void LIST_Insert(List *list, Node *newNode, Node *listNode)
{
  listNode->ln_Succ->ln_Pred = newNode;
  listNode->ln_Pred->ln_Succ = newNode;
}

Node * LIST_RemHead(List *list)
{
  printf("list head: %X\n", list->lh_Head);
  printf("list head succ: %X\n", list->lh_Head->ln_Succ);
  printf("list head succ succ: %X\n", list->lh_Head->ln_Succ->ln_Succ);
  printf("list tail pred: %X\n", list->lh_Tail->ln_Pred);
  printf("list tail: %X\n", list->lh_Tail);
  printf("list head succ pred: %X\n", list->lh_Head->ln_Succ->ln_Pred);

  Node *node = list->lh_Head->ln_Succ;

  list->lh_Head->ln_Succ = list->lh_Head->ln_Succ->ln_Succ;

  printf("RemHead complete\n");

  printf("list head: %X\n", list->lh_Head);
  printf("list head succ: %X\n", list->lh_Head->ln_Succ);
  printf("list tail pred: %X\n", list->lh_TailPred);
  printf("list head succ pred: %X\n", list->lh_Head->ln_Succ->ln_Pred);


  return node;
}

Node * LIST_RemTail(List *list)
{
  Node *node = list->lh_TailPred;
  list->lh_TailPred->ln_Pred->ln_Succ = list->lh_TailPred->ln_Succ;
  list->lh_TailPred = list->lh_TailPred->ln_Pred;

  return node;
}

void LIST_Remove(Node *node)
{
  node->ln_Pred->ln_Succ = node->ln_Succ;
  node->ln_Succ->ln_Pred = node->ln_Pred;
}

bool LIST_IsEmpty(List *list)
{
  printf("List is empty? lh_TailPred == %x, list == %x\n", list->lh_TailPred, list);
  return ((struct Node *)list->lh_TailPred == (struct Node *)list);
}
*/
