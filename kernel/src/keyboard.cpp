#include "keyboard.h"
#include "pic.h"
#include <stdio.h>

#include "messages.h"
#include "drivers/types.h"
#include "drivers/manager.h"

void _KB_DrainBuffer()
{
  _inb(0x60); // get the scancode, ignore it
}

void _KB_IRQHandler()
{
  // Create a keypress waiting message with the destination of the foreground process.
  // Dispatch it to the keyboard driver.
  //printf("kb irq!\n");

  //printf("Dispatching message to port %08X of process %08X\n", gProcessManager.FindMessagePortByPID(gProcessManager.GetForegroundProcess()), gProcessManager.GetForegroundProcess());
  if(gProcessManager.GetForegroundProcess() == NULL)
  {
    _KB_DrainBuffer();
    PIC_SendEOI(1);
    return;
  }
  
  IPC_MessagePort *fg_port = gProcessManager.FindMessagePortByPID(gProcessManager.GetForegroundProcess());

  if(fg_port == NULL)
  {
    _KB_DrainBuffer();
    PIC_SendEOI(1);
    return;
  }

  IORequest *kbd_request = (IORequest *)IPC_CreateMessage(sizeof(IORequest), fg_port);
  kbd_request->command = IOCMD_EXTENDED1; // HW interrupt: keypress waiting. Deliver it to the active process.
  kbd_request->data = NULL;
  kbd_request->length = sizeof(IORequest);
  IPC_SendMessage((IPC_Message *)kbd_request, gProcessManager.FindProcessByName("pc_keyboard")->port);

  PIC_SendEOI(1);
}
