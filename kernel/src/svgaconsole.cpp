#include "svgaconsole.h"

SCON_State svga_console_state;

void SCON_Init(SCON_State *state, FG_Font *font, FG_Bitmap *backing)
{
    state->cursor_X = 0;
    state->cursor_Y = 0;
    
    state->text_buffer = (uint8_t *)gMemoryManager.NewPtr(8192, H_SYSHEAP);

    state->cursor_max_Y = 30;
    state->cursor_max_X = 80;

    state->font = font;
    state->backing_bitmap = backing;
}

void SCON_RedrawTextCell(SCON_State *state, uint16_t x, uint16_t y)
{
    FG_BlitFont(state->backing_bitmap, POINT(x*8, y*16), state->font, state->text_buffer[x + (y*state->cursor_max_X)]);
}

void SCON_Redraw(SCON_State *state)
{
    for(int y=0; y<state->cursor_max_Y; y++)
    {
        for(int x=0; x<state->cursor_max_X; x++)
        {
            // TODO: use the hardware blitter for this
            FG_BlitFont(state->backing_bitmap, POINT(x*8, y*16), state->font, state->text_buffer[x + (y*state->cursor_max_X)]);
        }
    }
}

void SCON_WriteString(SCON_State *state, CString str)
{
    for(size_t i=0; i<strlen(str); i++)
    {
        SCON_TeletypeWrite(state, str[i]);
    }
}

void SCON_ScrollUp(SCON_State *state, int scrolls)
{
    for(int i=0; i<scrolls; i++){
        memcpy(state->text_buffer, state->text_buffer+(state->cursor_max_X), state->cursor_max_X*(state->cursor_max_Y-1));
    }

    memset(state->text_buffer + (state->cursor_max_X * (state->cursor_max_Y - 1)), 0, state->cursor_max_X);
    memset(state->backing_bitmap->pixels, 15, 640*480);

    SCON_Redraw(state);
}

void SCON_CursorLF(SCON_State *state)
{
  state->cursor_X = 0;
  state->cursor_Y++;

  if(state->cursor_Y >= state->cursor_max_Y)
    {
      state->cursor_Y--;
      SCON_ScrollUp(state, 1);
    }
}

void SCON_TeletypeBackspace(SCON_State *state)
{
    state->cursor_X -= 1;
    SCON_TeletypeWrite(state, 0x20);
    state->cursor_X -= 1;
    for(int i=0; i<state->cursor_max_X; i++)
    {
        SCON_RedrawTextCell(state, i, state->cursor_Y);
    }
}

void SCON_TeletypeHandleControlChars(SCON_State *state, char c)
{
    switch(c)
    {
        case '\b': SCON_TeletypeBackspace(state); return;
        case '\n': SCON_CursorLF(state); return;

        default: return;
    }
}

void SCON_TeletypeWrite(SCON_State *state, char c)
{
    if(c <= 0x1F)
    {
        SCON_TeletypeHandleControlChars(state, c);
    }
    else
    {
        state->text_buffer[state->cursor_X + (state->cursor_Y * state->cursor_max_X)] = c;
        SCON_RedrawTextCell(state, state->cursor_X, state->cursor_Y);
        SCON_AdvanceCursor(state);
    }
}

// void SCON_AdvanceCursor(SCON_State *state)
// {
//     state->cursor_X += 1;
    
//     if(state->cursor_X == state->cursor_max_X)
//     {
//         state->cursor_X = 0;
//         state->cursor_Y += 1;
//     }
// }