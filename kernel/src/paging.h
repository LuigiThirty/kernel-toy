#pragma once

#include "procyon.h"
#include <stdio.h>

extern uint32_t page_directory[1024] __attribute__((aligned(4096)));
extern uint32_t first_page_table[1024] __attribute__((aligned(4096)));

extern "C"
{
    extern void PAGE_LoadPageDirectory(unsigned int * page_directory);
    extern void PAGE_EnablePaging();
}

void PAGE_InitPageDirectory();
CPTR PAGE_CreateXMSPage(uint32_t size);
