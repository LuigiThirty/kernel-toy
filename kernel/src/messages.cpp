#include "messages.h"

#include "process.h"

IPC_Message * IPC_CreateMessage(uint16_t _length, IPC_MessagePort *_reply_to)
{
  IPC_Message *msg = (IPC_Message *)gMemoryManager.NewPtr(_length, H_SYSHEAP); // TODO: app heap?

  msg->length = _length;
  msg->reply_port = _reply_to;

  return msg; // Caller needs to fill in the struct they have created.
}

void IPC_SendMessage(IPC_Message *message, IPC_MessagePort *destination)
{
  LIST_AddTail(destination->message_queue, (Node *)message);
}

void IPC_DisposeMessage(IPC_Message *message)
{
  //printf("Freeing: %08X\n", message);
  gMemoryManager.DisposePtr(message);

  // TODO: free the data ptr if one exists
}

void IPC_MsgTest(IPC_Message *msg)
{
  printf("Message is at %08X", msg);  
}

IPC_Message *IPC_GetMessage()
{
  // Pop the first message in the running task's queue.  
  bool empty = LIST_IsEmpty(gProcessManager.GetRunningProcess()->port->message_queue);

  while(empty)
    {
      //printf("IPC_GetMessage: Message queue for process %08X is empty, yielding CPU\n", gProcessManager.GetRunningProcess());
      
      // ...yield.
      //printf("PID %d has no messages waiting\n", gProcessManager.GetRunningProcess()->pid);
      gProcessManager.QuantumExpired();

      // See if there's a message yet.
      empty = LIST_IsEmpty(gProcessManager.GetRunningProcess()->port->message_queue);

      // If not...
    }

  //printf("PID %d got a message\n", gProcessManager.GetRunningProcess()->pid);
  
  auto message = (IPC_Message *)LIST_RemHead(gProcessManager.GetRunningProcess()->port->message_queue);
  gProcessManager.GetRunningProcess()->signals.received &= ~SIG_MESSAGE;

  //IPC_MsgTest(message);

  return message;
}

IPC_MessagePort *IPC_CreatePort(const char *name)
{
  IPC_MessagePort *port = (IPC_MessagePort *)gMemoryManager.NewPtr(sizeof(IPC_MessagePort), H_SYSHEAP);
  port->message_queue = LIST_CreateList(LT_Message);
  strcpy(port->name, name);

  return port;
}
