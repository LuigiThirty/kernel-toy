#pragma once

#include <stdio.h>
#include <string.h>

#include "procyon.h"
#include "globals.h"
#include "error.h"
#include "low_level_io.h"
#include "serial.h"

#define ISA_VGA_IOBASE 0x0
#define VGA_IO_ADDRESS(ADDR) ( ISA_VGA_IOBASE + (ADDR) )

namespace PCVideo {
    typedef enum {
        TSENG_ET4000,
        CIRRUS_GD54XX,
        VBE_100,
        VBE_200,
        VBE_300,
        GENERIC_VGA,
    } VideoCard;

    typedef enum {
        TEXT_40x25,
        TEXT_80x25,
        VGA_MODE_13H,
        SVGA_640x480x256,
    } VideoMode;

    struct vbe_mode_info_structure_t {
        uint16_t attributes;      // deprecated, only bit 7 should be of interest to you, and it indicates the mode supports a linear frame buffer.
        uint8_t window_a;         // deprecated
        uint8_t window_b;         // deprecated
        uint16_t granularity;      // deprecated; used while calculating bank numbers
        uint16_t window_size;
        uint16_t segment_a;
        uint16_t segment_b;
        uint32_t win_func_ptr;      // deprecated; used to switch banks from protected mode without returning to real mode
        uint16_t pitch;         // number of bytes per horizontal line
        uint16_t width;         // width in pixels
        uint16_t height;         // height in pixels
        uint8_t w_char;         // unused...
        uint8_t y_char;         // ...
        uint8_t planes;
        uint8_t bpp;         // bits per pixel in this mode
        uint8_t banks;         // deprecated; total number of banks in this mode
        uint8_t memory_model;
        uint8_t bank_size;      // deprecated; size of a bank, almost always 64 KB but may be 16 KB...
        uint8_t image_pages;
        uint8_t reserved0;

        uint8_t red_mask;
        uint8_t red_position;
        uint8_t green_mask;
        uint8_t green_position;
        uint8_t blue_mask;
        uint8_t blue_position;
        uint8_t reserved_mask;
        uint8_t reserved_position;
        uint8_t direct_color_attributes;

        uint32_t framebuffer;      // physical address of the linear frame buffer; write here to draw to the screen
        uint32_t off_screen_mem_off;
        uint16_t off_screen_mem_size;   // size of memory in the framebuffer but not being displayed on the screen
        uint8_t reserved1[206];
    } __attribute__ ((packed));

    struct vbe_info_block_t {
        char VbeSignature[4];       // == "VESA"
        uint16_t VbeVersion;        // == 0x0300 for VBE 3.0
        char *OemStringPtr;         // isa vbeFarPtr
        uint8_t Capabilities[4];
        uint16_t VideoModePtr[2];   // isa vbeFarPtr
        uint16_t TotalMemory;       // as # of 64KB blocks

        // VBE 2.0
        uint16_t OEMVersion;
        char *VendorName;
        char *ProductName;
    } __attribute__((packed));
};

extern struct PCVideo::vbe_mode_info_structure_t VESAMode5F_info;
extern struct PCVideo::vbe_info_block_t VESA_CardInfo;

class SVGAManager {
    
};

PCVideo::VideoCard SVGA_DetectVideoCard();

// typedef enum {
//     BLACKNESS = 0x00,
//     NOTSRCERASE = 0x90,
//     NOTSRCCOPY = 0xD0,
//     SRCERASE = 0x09,
//     DSTINVERT = 0x0B,
//     PATINVERT = 0x59,
//     SRCINVERT = 0x59,
//     SRCAND = 0x05,
//     MERGEPAINT = 0x06,
//     SRCCOPY = 0x0D,
//     SRCPAINT = 0x6D,
//     PATCOPY = 0x0D,
//     WHITENESS = 0x0E,
// } BLT_ROP_MSNAME;

typedef struct {
    uint32_t BG_Color;          // 0x00
    uint32_t FG_Color;          // 0x04

    uint16_t BLT_Width;         // 0x08
    uint16_t BLT_Height;        // 0x0A
    
    uint16_t BLT_DestPitch;     // 0x0C
    uint16_t BLT_SourcePitch;   // 0x0E
    
    uint32_t BLT_DestAddress;   // 0x10
    
    uint8_t BLT_SourceAddress0; // 0x14
    uint8_t BLT_SourceAddress1; // 0x15
    uint8_t BLT_SourceAddress2; // 0x16

    uint8_t Dest_WriteMask;     // 0x17
    uint8_t BLT_Mode;           // 0x18
    uint8_t reserved1;
    uint8_t BLT_RasterOP;       // 0x1A
    uint8_t BLT_ModeExtensions; // 0x1B

    uint16_t Transparency;      // 0x1C
    uint16_t reserved2;         // 0x1E
    uint8_t reserved3[32];      // 0x20
    uint8_t BLT_Ctrl;           // 0x40

} __attribute__ ((packed)) GD5446_Blitter;
extern GD5446_Blitter *gd5446_blitter;

void SVGA_Init();
extern PCVideo::VideoCard installed_card;

extern "C"
{
    extern void _EnterV86(uint32_t ss, uint32_t esp, uint32_t cs, uint32_t eip);
    extern void _SVGA_Mode_640x480x256();
    extern void _SVGA_Mode_80Col();
    extern void _SVGA_Mode_13h();
    extern void _VBE_GetControllerInfo();
    extern void _Cirrus_VGAType();
}