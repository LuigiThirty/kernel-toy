#pragma once

#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <procyon.h>
#include <types/memmgr.h>

#define SYSTEM_HEAP_SIZE 262144
#define APPLICATION_HEAP_SIZE 131072

#define MEMMGR_BLOCK_HEADER_SIZE      20
#define MEMMGR_BLOCK_OFFSET_NEXT     -16
#define MEMMGR_BLOCK_OFFSET_PREVIOUS -12
#define MEMMGR_BLOCK_OFFSET_SIZE     -8
#define MEMMGR_BLOCK_OFFSET_FLAGS    -4

#define MASTER_POINTER_COUNT          256

#define PAGE_COUNT_PER_MB           1024768 / 4

#ifdef __cplusplus
extern "C" {
#endif

// C -> C++ trampoline
CPTR  _NewPtr(uint32_t requested_size, MEMMGR_MALLOC_FLAGS flags);
int   _DisposePtr(CPTR p);

#ifdef __cplusplus
}
#endif

class MemoryManager {

  public:
  typedef enum {
    ERR_NONE = 0,
    NULL_HANDLE = -1,
    BLOCK_IS_FREE = -2,
  } Error;

  typedef struct memmgr_block_t
  {
    struct memmgr_block_t *next;
    struct memmgr_block_t *previous;
    uint32_t size;
    uint32_t flags;
    CPTR destination;
  } Block;

  typedef struct memmgr_heap_t
  {
    struct memmgr_heap_t *next;
    struct memmgr_heap_t *previous;
    CPTR *master_pointers;
    uint32_t size; 
    struct memmgr_block_t *blocks;
  } Heap;

  typedef enum memmgr_block_flags_t {
    MEMMGR_BLOCK_FLAG_NONE   = 0x00,
    MEMMGR_BLOCK_FLAG_FIXED  = 0x01,
    MEMMGR_BLOCK_FLAG_PURGE  = 0x02,
    MEMMGR_BLOCK_FLAG_FREE   = 0x04,
    MEMMGR_BLOCK_FLAG_LOCKED = 0x08,
  } BlockFlags;

  // XMS
  CPTR    CreateXMSPage(uint32_t size);

  void    TestCTOR() { printf("XMSEnd: %x\n", XMSEnd); }

  private:
  Heap    heap_system;       // MacOS: SysZone
  Heap    active_heap;       // MacOS: TheZone

  CPTR    system_pointer_list[MASTER_POINTER_COUNT];
  CPTR    application_pointer_list[MASTER_POINTER_COUNT];
  CPTR    xms_pointer_list[MASTER_POINTER_COUNT];

  uint32_t RAMBase;
  uint32_t RAMEnd;

  uint32_t XMSBase;
  uint32_t XMSEnd = 0x3FFFFF;

  // Blocks
  CPTR    GetUnusedMasterPointer(Heap *heap);

  // Heap compaction
  void    CombineFreeBlocks(Heap *heap);
  void    CompactHeap(Heap *heap);

  ////
  /* Public Memory Manager API */
  public:
  void    Initialize();

  // Set up a heap
  void    InitHeap(Heap *heap, CPTR start, CPTR end);

  // Handles
  Handle  NewHandle(uint32_t size, MEMMGR_MALLOC_FLAGS flags);
  int     DisposeHandle(Handle h);
  int     LockHandle(Handle h);
  int     UnlockHandle(Handle h);

  // Pointers
  CPTR    NewPtr(uint32_t requested_size, MEMMGR_MALLOC_FLAGS flags);
  int     DisposePtr(CPTR p);

  // Blocks
  CPTR    AllocateBlock(Heap *heap, uint32_t requested_size, BlockFlags flags);
  void    FreeBlock(CPTR block);
  CPTR    GetBlockForHandle(Handle h);

  // Debug
  void    DumpHeapBlocks();
  void    UnrecoverableError(char *msg);
};

extern MemoryManager gMemoryManager;
