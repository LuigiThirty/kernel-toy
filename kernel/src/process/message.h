#pragma once

#include "messages.h"

// Process Manager notifications

typedef enum {
    // Input notifications
    IM_CHAR,   
} PM_NotificationType;

typedef struct
{
    IPC_Message message;        // The message node for passing to message ports.
    PM_NotificationType command;// The command for the driver to execute.
    CPTR data;                  // A data buffer for input or output.
    uint16_t length;            // How much data do we expect to read or write?
    uint16_t actual;            // The actual length of data written or read.
    
    uint16_t wParam;            // 16 bits of data.
    uint16_t lParam;            // 32 bits of data.
} PM_Notification;