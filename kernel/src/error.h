#pragma once

typedef int16_t OSError;

// File I/O errors start at -1
#define ERROR_PATH_HAS_NO_DRIVE_LETTER 		-1
#define ERROR_PATH_IS_MALFORMED				-2
#define ERROR_PATH_HAS_INVALID_DRIVE_LETTER	-3
#define ERROR_FILE_NOT_FOUND				-4

// Video errors start at -100
#define VIDEO_MODE_NOT_SUPPORTED            -101