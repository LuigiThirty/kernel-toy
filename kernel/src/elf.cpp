#include "elf.h"
#include "fileio.h"
#include "serial.h"

int16_t ELFManager::LoadExecutable(char *buffer)
{
  COM1_PRINTF("Reading ELF header for file at $%06X\r\n", buffer);

  ELF_Header header;
  memcpy(&header, buffer, sizeof(ELF_Header));

  if(header.e_ident.magic[0] != 0x7F || header.e_ident.magic[1] != 0x45 ||
	 header.e_ident.magic[2] != 0x4C || header.e_ident.magic[3] != 0x46)
	{
	  printf("ELF: ELF header is invalid, abort\n");
	  return ELF_ERROR_BAD_MAGIC_NUMBER;
	}

  printf("Found a valid ELF magic number, processing header\n");

  // Make sure we can execute this file.
  if(header.e_ident.clazz != 0x01)
	{
	  printf("ELF: Header says this is not a 32-bit executable.\n");
	  return ELF_ERROR_NOT_32_BIT;
	}

  if(header.e_ident.data != 0x01)
	{
	  printf("ELF: Header says this is big-endian.\n");
	  return ELF_ERROR_WRONG_ENDIANNESS;
	}

  // look for a program header
  COM1_PRINTF("ELF: file has %d program headers\r\n", header.e_phnum);

  ELF_ProgramHeader *ph_table = (ELF_ProgramHeader *)((uint32_t)buffer + (uint32_t)header.e_phoff);
  ELF_ProgramHeader *current_ph = ph_table;

  for(int i=0; i<header.e_phnum; i++)
	{
	  COM1_PRINTF("*** PROGRAM HEADER: type %d, image offset %06X, %d bytes\r\n",
			 current_ph[i].p_type,
			 current_ph[i].p_offset,
			 current_ph[i].p_filesz);
	}

  // look for section headers
  COM1_PRINTF("ELF: file has %d section headers\r\n", header.e_shnum);

  ELF_SectionHeader *sh_table = (ELF_SectionHeader *)((uint32_t)buffer + (uint32_t)header.e_shoff);
  ELF_SectionHeader *current_section = sh_table;
  
  char *section_names = current_section[header.e_shstrndx].sh_offset + buffer;
	COM1_PRINTF("section_names is at $%06X (%06X + %06X)\n",
		   section_names,
		   current_section[header.e_shstrndx].sh_offset,
		   buffer);

  for(int i=0; i<header.e_shnum; i++)
	{ 
    if(current_section[i].sh_flags & 0x2)
    {
      // If a section has the ALLOC flag, load it into memory.
      COM1_PRINTF("Loading section '%s' into %08X (offset in ELF is %08X, size is %08X\r\n", section_names + current_section[i].sh_name, current_section[i].sh_addr, current_section[i].sh_offset, current_section[i].sh_size);
      memcpy((void *)current_section[i].sh_addr, buffer + current_section[i].sh_offset, current_section[i].sh_size);
    }
	}

	COM1_PRINTF("Entry point is %08X\r\n", header.e_entry);
	RunProgram((void *)header.e_entry);

 	return 0;
}

void ELFManager::RunProgram(void *entry)
{
  COM1_PRINTF("Running program...\r\n===============\r\n");
  printf("\n");
  JSRProgram(entry);
  COM1_PRINTF("===============\r\nProgram complete...\r\n");
  COM1_PRINTF("(0x333333 is 0x%08X)\r\n", MMIO32(0x333333));
}

void ELFManager::JSRProgram(void *entry)
{
  asm (
       "PUSHA; CALL *%0; POPA;" : : "b"(entry) :
       );
}

/*
int ELFManager::ELF_LoadDylib(char *filename)
{
  FileDescriptor fd = FD_Open(filename);

  uint32_t file_length = FD_Length(fd);

  //uint8_t *buffer = (uint8_t *)gMemoryManager.NewPtr(file_length, H_SYSHEAP);
  uint8_t *buffer = (uint8_t *)0x280000;

  memset(buffer, 0, file_length);
  FD_Read(fd, buffer, file_length);

  printf("Reading ELF header for file at $%06X\r\n", buffer);

  ELF_Header *header = (ELF_Header *)buffer;

  if(header->e_ident.magic[0] != 0x7F || header->e_ident.magic[1] != 0x45 ||
	 header->e_ident.magic[2] != 0x4C || header->e_ident.magic[3] != 0x46)
	{
	  printf("ELF: ELF header is invalid, abort\r\n");
	  return ELF_ERROR_BAD_MAGIC_NUMBER;
	}

  printf("Found a valid ELF magic number, processing header\r\n");

  // Make sure we can execute this file.
  if(header->e_ident.clazz != 0x01)
	{
	  printf("ELF: Header says this is not a 32-bit executable.\r\n");
	  return ELF_ERROR_NOT_32_BIT;
	}

  if(header->e_ident.data != 0x01)
	{
	  printf("ELF: Header says this is big-endian.\r\n");
	  return ELF_ERROR_WRONG_ENDIANNESS;
	}

  // look for a program header
  COM1_PRINTF("ELF: file has %d program headers\r\n", header->e_phnum);

  ELF_ProgramHeader *ph_table = (ELF_ProgramHeader *)((uint32_t)buffer + (uint32_t)header->e_phoff);
  ELF_ProgramHeader *current_ph = ph_table;

  for(int i=0; i<header->e_phnum; i++)
	{
	  COM1_PRINTF("*** PROGRAM HEADER: type %d, image offset %06X, %d bytes\r\n",
			 current_ph[i].p_type,
			 current_ph[i].p_offset,
			 current_ph[i].p_filesz);

    if(current_ph[i].p_type == PT_LOAD)
    {
      COM1_PRINTF("Loading LOAD section to 0x2C0000\r\n");
      memcpy((void *)0x2C0000, buffer + current_ph[i].p_offset, current_ph[i].p_filesz);
    }
	}

  // look for section headers
  COM1_PRINTF("ELF: file has %d section headers\r\n", header->e_shnum);

  ELF_SectionHeader *sh_table = (ELF_SectionHeader *)((uint32_t)buffer + (uint32_t)header->e_shoff);
  ELF_SectionHeader *current_section = sh_table;
  
  uint8_t *section_names = current_section[header->e_shstrndx].sh_offset + buffer;
	COM1_PRINTF("section_names is at $%06X (%06X + %06X)\r\n",
		   section_names,
		   current_section[header->e_shstrndx].sh_offset,
		   buffer);

  for(int i=0; i<header->e_shnum; i++)
	{ 
	  COM1_PRINTF("*** SECTION %d: header at %08X, name '%s', type %02X, offset $%06X ***\r\n",
			i,
      &(current_section[i]),
			section_names + current_section[i].sh_name,
			current_section[i].sh_type,
			current_section[i].sh_offset);

    if(current_section[i].sh_flags & 0x2)
    {
      // If a section has the ALLOC flag, load it into memory.
      COM1_PRINTF("Loading section '%s' into %08X (offset in ELF is %08X, size is %08X\r\n", section_names + current_section[i].sh_name, buffer + current_section[i].sh_addr, current_section[i].sh_offset, current_section[i].sh_size);
      memcpy((void *)current_section[i].sh_addr, buffer + current_section[i].sh_offset, current_section[i].sh_size);
    }
	}

  uint32_t relocation_count;

  printf("Searching for relocation table\n");
  for(uint32_t i=0; i<header->e_shnum; i++)
	{
	  if(current_section[i].sh_type == SHT_RELA)
		{
      ELF_RELA *relocation_table;
			printf("RELA found, entsize is %d\n", current_section[i].sh_entsize);
			relocation_table = (ELF_RELA *)(buffer + current_section[i].sh_offset);
			relocation_count = current_section[i].sh_size / current_section[i].sh_entsize; // TODO: PROCYON.DYN crashes here

			printf("Processing RELA table. Section is %d, offset $%06X, contains %d entries\n",
          i,
					relocation_table,
					relocation_count);

			for(uint32_t j=0; j<relocation_count; j++)
			{
				//ELF_Relocate_RELA(relocation_table[j], sh_table, buffer);
			}
		}
    else if(current_section[i].sh_type == SHT_REL)
		{
      ELF_REL *relocation_table;
			relocation_table = (ELF_REL *)(buffer + current_section[i].sh_offset);
			relocation_count = current_section[i].sh_size / current_section[i].sh_entsize; // TODO: PROCYON.DYN crashes here

			COM1_PRINTF("Processing REL table (section %d) at offset $%06X, contains %d entries\r\n",
          i,
					relocation_table,
					relocation_count);

			for(uint32_t j=0; j<relocation_count; j++)
			{
        Relocate_REL((void *)0x2C0000, header, &(relocation_table[j]), &(current_section[i]));
			}
		}
	}

  RunProgram((void *)0x2C0000);

  return 0;
}

#define TEXT_OFFSET 0x1000

ELFManager::ELF_SectionHeader * ELFManager::elf_sheader(ELF_Header *hdr) {
	return (ELF_SectionHeader *)((int)hdr + hdr->e_shoff);
}
 
ELFManager::ELF_SectionHeader * ELFManager::elf_section(void *load_base, ELF_Header *hdr, int idx) {
	ELF_SectionHeader *sh_table = (ELF_SectionHeader *)hdr->e_shoff;
  ELF_SectionHeader *current_section = sh_table;
  COM1_PRINTF("Section header for section %d is at %08X\r\n", idx, ((uint32_t)hdr + (uint32_t)(&current_section[idx])));
  return (ELF_SectionHeader *)((uint32_t)hdr + (uint32_t)(&current_section[idx]));
}

void *elf_lookup_symbol(const char *name)
{
  return NULL;
}

int ELFManager::elf_get_symval(void *load_base, ELF_Header *hdr, int table, int idx) {
  if(table == SHN_UNDEF || idx == SHN_UNDEF) return 0;

  ELF_SectionHeader *symbol_table = elf_section(load_base, hdr, table);
  uint32_t symtab_entries = symbol_table->sh_size / symbol_table->sh_entsize;

  int symaddr = (int)hdr + symbol_table->sh_offset;
	Elf32_Sym *symbol = &((Elf32_Sym *)symaddr)[idx];

  // printf("Symbol: %08X, shndx = %08X\n", symbol->st_value, symbol->st_shndx);

  if(symbol->st_shndx == SHN_UNDEF) {
		// External symbol, lookup value
		ELF_SectionHeader *strtab = elf_section(load_base, hdr, symbol_table->sh_link);
		const char *name = (const char *)hdr + strtab->sh_offset + symbol->st_name;
 
		void *target = elf_lookup_symbol(name);
 
		if(target == NULL) {
			// Extern symbol not found
			if(ELF32_ST_BIND(symbol->st_info) & STB_WEAK) {
				// Weak symbol initialized as 0
				return 0;
			} else {
				printf("Undefined External Symbol : %s.\r\n", name);
				return ELF_RELOC_ERR;
			}
		} else {
			return (int)target;
		}
  } else if(symbol->st_shndx == SHN_ABS) {
		// Absolute symbol
		return (uint32_t)symbol->st_value;
	} else {
		// Internally defined symbol
		ELF_SectionHeader *target = elf_section(load_base, hdr, symbol->st_shndx);
		return (uint32_t)symbol->st_value + target->sh_offset;
	}

  return 0;
}

int ELFManager::Relocate_REL(void *load_base, ELF_Header *hdr, ELF_REL *relocation, ELF_SectionHeader *reltab)
{
  ELF_SectionHeader *target = elf_section(load_base, hdr, reltab->sh_info);

  COM1_PRINTF("Relocation: info %04X, offset %08X, target section %d %08X, s_offset %08X\r\n", 
    relocation->r_info, relocation->r_offset, reltab->sh_info, target, target->sh_offset);
  
  uint32_t addr = (uint32_t)load_base + target->sh_offset;
	int *ref = (int *)(addr + (uint32_t)relocation->r_offset);
  uint32_t symval = 0;
  if(ELF32_R_SYM(relocation->r_info) != SHN_UNDEF)
  {
    symval = elf_get_symval(load_base, hdr, reltab->sh_link, ELF32_R_SYM(relocation->r_info));
  }

  // Relocate based on type
	switch(ELF32_R_TYPE(relocation->r_info)) {
		case R_386_NONE:
			// No relocation
			break;
    case R_386_RELATIVE:
      // Addend + Base Address
      COM1_PRINTF("R_386_RELATIVE: Symval %08X. Ref %08X, addend %X\r\n", 
        symval, ref, (uint32_t)load_base+symval, MMIO32(ref));
      MMIO32((uint32_t)ref) = MMIO32(ref);
      break;
		case R_386_32:
			// Symbol + Offset
      COM1_PRINTF("R_386_32: Symval %08X. Setting address %08X to %08X, addend %X\r\n", 
        symval, ref, (uint32_t)load_base+symval, MMIO32(ref));
      MMIO32((uint32_t)ref) = (uint32_t)DO_386_32(load_base+symval, MMIO32(ref));
			break;
		//case R_386_PC32:
			// Symbol + Offset - Section Offset
      //printf("Relocating R_386_PC32: %08X\n", symval);
      //MMIO32(ref) = DO_386_PC32(symval, ref, target->sh_offset);
		//	break;
    case R_386_JMP_SLOT:
      // Set the memory address to the symbol value.
      MMIO32(ref) = (uint32_t)load_base+(symval);
      break;
		default:
			// Relocation type not supported, display error and return
			COM1_PRINTF("Unsupported Relocation Type (%d).\r\n", ELF32_R_TYPE(relocation->r_info));
			return ELF_RELOC_ERR;
	}
	return symval;

  return 0;
}

/*
static inline ELF_SectionHeader *elf_sheader(ELF_Header *hdr) {
	return (ELF_SectionHeader *)((int)hdr + hdr->e_shoff);
}
 
static inline ELF_SectionHeader *elf_section(void *load_base, ELF_Header *hdr, int idx) {
	ELF_SectionHeader *sh_table = (ELF_SectionHeader *)hdr->e_shoff;
  ELF_SectionHeader *current_section = sh_table;
  COM1_PRINTF("Section header for section %d is at %08X\n", idx, ((uint32_t)hdr + (uint32_t)(&current_section[idx])));
  return (ELF_SectionHeader *)((uint32_t)hdr + (uint32_t)(&current_section[idx]));
}

void *elf_lookup_symbol(const char *name)
{
  return NULL;
}

static int elf_get_symval(void *load_base, ELF_Header *hdr, int table, int idx) {
  if(table == SHN_UNDEF || idx == SHN_UNDEF) return 0;

  ELF_SectionHeader *symbol_table = elf_section(load_base, hdr, table);
  uint32_t symtab_entries = symbol_table->sh_size / symbol_table->sh_entsize;

  int symaddr = (int)hdr + symbol_table->sh_offset;
	ELF_SYMBOL *symbol = &((ELF_SYMBOL *)symaddr)[idx];

  // printf("Symbol: %08X, shndx = %08X\n", symbol->st_value, symbol->st_shndx);

  if(symbol->st_shndx == SHN_UNDEF) {
		// External symbol, lookup value
		ELF_SectionHeader *strtab = elf_section(load_base, hdr, symbol_table->sh_link);
		const char *name = (const char *)hdr + strtab->sh_offset + symbol->st_name;
 
		void *target = elf_lookup_symbol(name);
 
		if(target == NULL) {
			// Extern symbol not found
			if(ELF32_ST_BIND(symbol->st_info) & STB_WEAK) {
				// Weak symbol initialized as 0
				return 0;
			} else {
				printf("Undefined External Symbol : %s.\n", name);
				return ELF_RELOC_ERR;
			}
		} else {
			return (int)target;
		}
  } else if(symbol->st_shndx == SHN_ABS) {
		// Absolute symbol
		return (uint32_t)symbol->st_value;
	} else {
		// Internally defined symbol
		ELF_SectionHeader *target = elf_section(load_base, hdr, symbol->st_shndx);
		return (uint32_t)symbol->st_value + target->sh_offset;
	}

  return 0;
}

#define TEXT_OFFSET 0x1000

int ELF_Relocate_REL(void *load_base, ELF_Header *hdr, ELF_REL *relocation, ELF_SectionHeader *reltab)
{
  ELF_SectionHeader *target = elf_section(load_base, hdr, reltab->sh_info);

  COM1_PRINTF("Relocation: info %04X, offset %08X, target section %d %08X, s_offset %08X\n", 
    relocation->r_info, relocation->r_offset, reltab->sh_info, target, target->sh_offset);
  
  uint32_t addr = 0x2C0000 + target->sh_offset;
	int *ref = (int *)(addr + (uint32_t)relocation->r_offset);
  uint32_t symval = 0;
  if(ELF32_R_SYM(relocation->r_info) != SHN_UNDEF)
  {
    symval = elf_get_symval(load_base, hdr, reltab->sh_link, ELF32_R_SYM(relocation->r_info));
  }

  // Relocate based on type
	switch(ELF32_R_TYPE(relocation->r_info)) {
		case R_386_NONE:
			// No relocation
			break;
    case R_386_RELATIVE:
      // Addend + Base Address
      COM1_PRINTF("R_386_RELATIVE: Symval %08X. Ref %08X, addend %X\n", 
        symval, ref, (uint32_t)0x2C0000+symval, MMIO32(ref));
      MMIO32((uint32_t)ref) = MMIO32(ref);
      break;
		case R_386_32:
			// Symbol + Offset
      COM1_PRINTF("R_386_32: Symval %08X. Setting address %08X to %08X, addend %X\n", 
        symval, ref, (uint32_t)0x2C0000+symval, MMIO32(ref));
      MMIO32((uint32_t)ref) = (uint32_t)DO_386_32(symval, MMIO32(ref));
			break;
		//case R_386_PC32:
			// Symbol + Offset - Section Offset
      //printf("Relocating R_386_PC32: %08X\n", symval);
      //MMIO32(ref) = DO_386_PC32(symval, ref, target->sh_offset);
		//	break;
    case R_386_JMP_SLOT:
      // Set the memory address to the symbol value.
      MMIO32(ref) = symval;
      break;
		default:
			// Relocation type not supported, display error and return
			COM1_PRINTF("Unsupported Relocation Type (%d).\n", ELF32_R_TYPE(relocation->r_info));
			return ELF_RELOC_ERR;
	}
	return symval;

  return 0;
}

int ELF_LoadDylib(char *filename)
{
  FileDescriptor fd = FD_Open(filename);

  uint32_t file_length = FD_Length(fd);

  //uint8_t *buffer = (uint8_t *)gMemoryManager.NewPtr(file_length, H_SYSHEAP);
  uint8_t *buffer = (uint8_t *)0x280000;

  memset(buffer, 0, file_length);
  FD_Read(fd, buffer, file_length);

	printf("Reading ELF header for file at $%06X\n", buffer);

  ELF_Header *header = (ELF_Header *)buffer;

  if(header->e_ident.magic[0] != 0x7F || header->e_ident.magic[1] != 0x45 ||
	 header->e_ident.magic[2] != 0x4C || header->e_ident.magic[3] != 0x46)
	{
	  printf("ELF: ELF header is invalid, abort\n");
	  return ELF_ERROR_BAD_MAGIC_NUMBER;
	}

  printf("Found a valid ELF magic number, processing header\n");

  // Make sure we can execute this file.
  if(header->e_ident.clazz != 0x01)
	{
	  printf("ELF: Header says this is not a 32-bit executable.\n");
	  return ELF_ERROR_NOT_32_BIT;
	}

  if(header->e_ident.data != 0x01)
	{
	  printf("ELF: Header says this is big-endian.\n");
	  return ELF_ERROR_WRONG_ENDIANNESS;
	}

  // look for a program header
  COM1_PRINTF("ELF: file has %d program headers\n", header->e_phnum);

  ELF_ProgramHeader *ph_table = (ELF_ProgramHeader *)((uint32_t)buffer + (uint32_t)header->e_phoff);
  ELF_ProgramHeader *current_ph = ph_table;

  for(int i=0; i<header->e_phnum; i++)
	{
	  COM1_PRINTF("*** PROGRAM HEADER: type %d, image offset %06X, %d bytes\n",
			 current_ph[i].p_type,
			 current_ph[i].p_offset,
			 current_ph[i].p_filesz);

    #define PT_LOAD 1
    if(current_ph[i].p_type == PT_LOAD)
    {
      COM1_PRINTF("Loading LOAD section to 0x2C0000\n");
      memcpy((void *)0x2C0000, buffer + current_ph[i].p_offset, current_ph[i].p_filesz);
    }
	}

  // look for section headers
  COM1_PRINTF("ELF: file has %d section headers\n", header->e_shnum);

  ELF_SectionHeader *sh_table = (ELF_SectionHeader *)((uint32_t)buffer + (uint32_t)header->e_shoff);
  ELF_SectionHeader *current_section = sh_table;
  
  uint8_t *section_names = current_section[header->e_shstrndx].sh_offset + buffer;
	COM1_PRINTF("section_names is at $%06X (%06X + %06X)\n",
		   section_names,
		   current_section[header->e_shstrndx].sh_offset,
		   buffer);

  for(int i=0; i<header->e_shnum; i++)
	{ 
	  COM1_PRINTF("*** SECTION %d: header at %08X, name '%s', type %02X, offset $%06X ***\n",
			i,
      &(current_section[i]),
			section_names + current_section[i].sh_name,
			current_section[i].sh_type,
			current_section[i].sh_offset);

    if(current_section[i].sh_flags & 0x2)
    {
      // If a section has the ALLOC flag, load it into memory.
      //COM1_PRINTF("Loading section '%s' into %08X (offset in ELF is %08X, size is %08X\n", section_names + current_section[i].sh_name, current_section[i].sh_addr, current_section[i].sh_offset, current_section[i].sh_size);
      //memcpy((void *)current_section[i].sh_addr, buffer + current_section[i].sh_offset, current_section[i].sh_size);
    }
	}

  uint32_t relocation_count;

  printf("Searching for relocation table\n");
  for(uint32_t i=0; i<header->e_shnum; i++)
	{
	  if(current_section[i].sh_type == SHT_RELA)
		{
      ELF_RELA *relocation_table;
			printf("RELA found, entsize is %d\n", current_section[i].sh_entsize);
			relocation_table = (ELF_RELA *)(buffer + current_section[i].sh_offset);
			relocation_count = current_section[i].sh_size / current_section[i].sh_entsize; // TODO: PROCYON.DYN crashes here

			printf("Processing RELA table. Section is %d, offset $%06X, contains %d entries\n",
          i,
					relocation_table,
					relocation_count);

			for(uint32_t j=0; j<relocation_count; j++)
			{
				//ELF_Relocate_RELA(relocation_table[j], sh_table, buffer);
			}
		}
    else if(current_section[i].sh_type == SHT_REL)
		{
      ELF_REL *relocation_table;
			relocation_table = (ELF_REL *)(buffer + current_section[i].sh_offset);
			relocation_count = current_section[i].sh_size / current_section[i].sh_entsize; // TODO: PROCYON.DYN crashes here

			COM1_PRINTF("Processing REL table (section %d) at offset $%06X, contains %d entries\n",
          i,
					relocation_table,
					relocation_count);

			for(uint32_t j=0; j<relocation_count; j++)
			{
        		ELF_Relocate_REL((void *)0x280000, header, &(relocation_table[j]), &(current_section[i]));
			}
		}
	}

  printf("Entry point is %08X\n", 0x2C0000 + header->e_entry);
  ELF_RunProgram((void *)0x2C0000 + header->e_entry);

  return 0;
}
*/