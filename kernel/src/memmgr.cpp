#include "memmgr.h"
#include "serial.h"

MemoryManager gMemoryManager;

uint32_t RAMBase = 0;
uint32_t RAMEnd = 0x3FFFFF; // TODO: determine this dynamically

uint32_t SysHeap;
uint32_t SysHeapEnd;

void MemoryManager::Initialize()
{ 
  RAMBase     = 0;

  SysHeap     = 0x140000;
  SysHeapEnd  = SysHeap + SYSTEM_HEAP_SIZE;

  RAMEnd      = 0x3FFFFF;
   
  InitHeap(&heap_system, (CPTR)SysHeap, (CPTR)SysHeapEnd);
}

void MemoryManager::InitHeap(Heap *heap, CPTR start, CPTR end)
{
  Block *master_block = (Block *)start;
  master_block->next = NULL;
  master_block->previous = NULL;
  master_block->size = (uint32_t)end - (uint32_t)start;
  master_block->flags = MEMMGR_BLOCK_FLAG_FREE;
  master_block->destination = (CPTR)((uint32_t)(heap)+sizeof(Block));
  heap->next = NULL;
  heap->previous = NULL;
  heap->master_pointers = application_pointer_list;
  heap->size = (uint32_t)end - (uint32_t)start;
  heap->blocks = master_block;
}

CPTR MemoryManager::NewPtr(uint32_t requested_size, MEMMGR_MALLOC_FLAGS flags)
{
  //printf("gMemoryManager.NewPtr: Requested a %d byte block\n", requested_size);

  BlockFlags new_block_flags = MEMMGR_BLOCK_FLAG_FIXED;

  Heap *heap = NULL;

  if(flags & H_SYSHEAP)
  {
    heap = &heap_system;
  }
  if(flags & H_CURHEAP)
  {
    //heap = &heap_application;
  }
  if(flags & H_CLEAR)
  {
    new_block_flags = (BlockFlags)(new_block_flags | H_CLEAR);
  }

  if(heap == NULL) return NULL; // No heap specified, call fails.
  
  CPTR block = AllocateBlock(heap, requested_size, new_block_flags);

  if(block == NULL)
  {
    printf("Unable to allocate %d bytes!\n");
    while(true) {};
  }

  // Return the block's data area
  return (char *)(block) + sizeof(Block);
}

int MemoryManager::DisposePtr(CPTR p)
{
  uint32_t *block = (uint32_t *)(((uint8_t *)p) - MEMMGR_BLOCK_HEADER_SIZE);
  FreeBlock(block);

  return 0;
}

Handle MemoryManager::NewHandle(uint32_t requested_size, MEMMGR_MALLOC_FLAGS flags)
{
  BlockFlags new_block_flags = MEMMGR_BLOCK_FLAG_NONE;

  MemoryManager::Heap *heap = NULL;

  if(flags & H_SYSHEAP)
  {
    heap = &heap_system;
  }
  if(flags & H_CURHEAP)
  {
    //heap = &heap_application;
  }
  if(flags & H_CLEAR)
  {
    new_block_flags = (BlockFlags)(new_block_flags | H_CLEAR);
  }

  if(heap == NULL) return NULL; // No heap specified, call fails.
  
  //printf("MEMMGR_NewHandle: Requested a %d byte block\n", requested_size);
  CPTR block = AllocateBlock(heap, requested_size, new_block_flags);
  //printf("MEMMGR_NewHandle: Got block $%06X\n", block);
  
  // Add the block to the master pointer list.
  CPTR master = GetUnusedMasterPointer(heap);
  //printf("MEMMGR_NewHandle: Using master pointer at %06X\n", master);
  MMIO32((uint32_t)master) = (uint32_t)block+MEMMGR_BLOCK_HEADER_SIZE;

  // Pointer to the master pointer list entry is a handle.
  return (Handle)master; 
}

CPTR MemoryManager::AllocateBlock(Heap *heap, uint32_t requested_size, BlockFlags flags)
{
  uint32_t adjusted_size = (requested_size + sizeof(Block));
  if(adjusted_size & 0x01) adjusted_size++; // Make sure all blocks have even lengths.
  
  Block *block = heap->blocks;

  // Find a free block that's big enough.
  while(block != NULL)
	{
	  if((uint32_t)block->previous > RAMEnd ||
		 (uint32_t)block->next > RAMEnd ||
		 (uint32_t)block->destination > RAMEnd)
		{
      char buf[100];

		  sprintf(buf, "MEMMGR_AllocateBlock: Heap is corrupt: Block pointer is beyond RAMEnd. \nHalting system.\n");
      COM1.PutStr(buf);
      printf(buf);
		  sprintf(buf, "Bad block is at $%x\n", block);
      COM1.PutStr(buf);
      printf(buf);
		  sprintf(buf, "block->previous: $%x\n", block->previous);
      COM1.PutStr(buf);
      printf(buf);
		  sprintf(buf, "block->next: $%x\n", block->next);
      COM1.PutStr(buf);
      printf(buf);
		  sprintf(buf, "block->destination: $%x\n", block->destination);
      printf(buf);
      COM1.PutStr(buf);
      
		  while(true) {};
		}

	  if((uint32_t)block->destination == 0x0)
		{
		  printf("MEMMGR_AllocateBlock: Heap is corrupt: block points to 0x000000.\nHalting system.\n");
		  while(true) {};
		}
			 
	  // TODO: put a new node in the middle of the list. that doesn't work atm.

	  // Either put a new node at the end of the list or re-use an existing node,
	  // as long as it is not over twice as large as the size we need.
	  if((block->flags & MEMMGR_BLOCK_FLAG_FREE) && ((block->next == NULL) || (block->size >= adjusted_size)))
		{
		  if(block->next == NULL)
      {
        // If this is an end node, create a new block that is a resized version of the old block.
        Block *new_block = (Block *)(((char *)block)+adjusted_size);
        //printf("Creating a new block at %08X based on the block at %08X\n", new_block, block);
        new_block->size = block->size - adjusted_size;
        if(block->next != NULL) block->next->previous = new_block;
        new_block->previous = block;
        new_block->flags = MEMMGR_BLOCK_FLAG_FREE;
        new_block->destination = (CPTR)((uint32_t)(new_block)+MEMMGR_BLOCK_HEADER_SIZE);
        
        block->next = new_block;
        block->size = adjusted_size;
        block->flags = flags;

        if(flags & H_CLEAR)
        {
          // TODO
          printf("TODO: H_CLEAR not implemented\n");
        }
        return block;
      }
		  else
      {
        // Reuse an existing node.
        block->flags = MEMMGR_BLOCK_FLAG_FIXED;
        return block;
      }
		}
	  else 
    {
		  block = block->next;
	  }
	}

  // No free block big enough.
  printf("MEMMGR: Failed to find free block of %d bytes\n", adjusted_size);
  DumpHeapBlocks();
  return NULL;
}

int MemoryManager::DisposeHandle(Handle h)
{
  uint32_t *block = (uint32_t *)(((uint8_t *)*h) - MEMMGR_BLOCK_HEADER_SIZE);
  printf("MEMMGR_DisposeHandle: freeing block at $%06X. block starts at $%06X\n", *h, block);
  block[3] = MEMMGR_BLOCK_FLAG_FREE;
  
  return 0;
}

void MemoryManager::FreeBlock(CPTR block)
{
  ((uint32_t *)block)[3] = MEMMGR_BLOCK_FLAG_FREE;
}

void MemoryManager::DumpHeapBlocks()
{
  Block *block = heap_system.blocks;

  printf("*** DumpSystemHeapBlocks ***\n");
  while(block != NULL)
	{
	  printf("BLOCK AT $%06X - size %d, ptr: $%06X, fixed: %d, free: %d\n",
			 block,
			 block->size,
			 block->destination,
			 (block->flags & MEMMGR_BLOCK_FLAG_FIXED) == MEMMGR_BLOCK_FLAG_FIXED,
			 (block->flags & MEMMGR_BLOCK_FLAG_FREE) == MEMMGR_BLOCK_FLAG_FREE);
	  block = block->next;
	}

  printf("*** DumpSystemHeapBlocks end ***\n");
}

CPTR MemoryManager::GetUnusedMasterPointer(Heap *heap)
{
   for(int i=0;i<MASTER_POINTER_COUNT;i++)
	{
	  if(heap->master_pointers[i] == NULL)
		return &(heap->master_pointers[i]);
	}

   return NULL; // all pointers in use, failed.
}

int MemoryManager::LockHandle(Handle h)
{
  // Is this handle valid?
  if(*h == NULL)
	return Error::NULL_HANDLE;

  uint32_t *block = (uint32_t *)GetBlockForHandle(h);

  // Is this a handle to a freed block?
  if((block[3] & MEMMGR_BLOCK_FLAG_FREE) == MEMMGR_BLOCK_FLAG_FREE)
	return Error::BLOCK_IS_FREE;

  block[3] |= MEMMGR_BLOCK_FLAG_LOCKED; // Lock the handle.

  return Error::ERR_NONE;
}

int MemoryManager::UnlockHandle(Handle h)
{
  // Is this handle valid?
  if(*h == NULL)
	return Error::NULL_HANDLE;

  int32_t *block = (int32_t *)GetBlockForHandle(h);

  // Is this a handle to a freed block?
  if((block[3] & MEMMGR_BLOCK_FLAG_FREE) == MEMMGR_BLOCK_FLAG_FREE)
	return Error::BLOCK_IS_FREE;

  block[3] = (block[3] & ~MEMMGR_BLOCK_FLAG_LOCKED); // Unlock if locked.

  return Error::ERR_NONE; // success
}

void * MemoryManager::GetBlockForHandle(Handle h)
{
  return (void *)(((char *)*h) - MEMMGR_BLOCK_HEADER_SIZE);
}

void MemoryManager::CombineFreeBlocks(Heap *heap)
{
  printf("MEMMGR: Looking for adjacent free blocks.\n");

  Block *block = heap->blocks;

  while(block != NULL)
	{
	  if((block->flags & MEMMGR_BLOCK_FLAG_FREE) == MEMMGR_BLOCK_FLAG_FREE)
		{
		  //This is a free block. Is the next block free?
		  if((block->next->flags & MEMMGR_BLOCK_FLAG_FREE) == MEMMGR_BLOCK_FLAG_FREE)
			{
			  //Yes. Combine the two blocks, then invalidate any handles to the disposed block.
			  printf("MEMMGR: Combining blocks $%06X and $%06X\n",
					 block,
					 block->next);
			  block->size = block->size + block->next->size;

			  //Remove the next block from the list.
			  block->next = block->next->next;
			  if(block->next != NULL)
				block->next->previous = block;
			}
		}
	  
	  block = block->next;
	}
}

void MemoryManager::CompactHeap(Heap *heap)
{
  // Make all relocatable blocks as contiguous as possible.

  // First, combine adjacent free blocks.
  CombineFreeBlocks(heap);
  
  Block *block = heap->blocks;

  while(block != NULL)
	{
	  if(block->flags & MEMMGR_BLOCK_FLAG_FIXED ||
		 block->flags & MEMMGR_BLOCK_FLAG_LOCKED ||
		 block->flags & MEMMGR_BLOCK_FLAG_FREE)
		{
		  continue; // do not relocate this block
		}

	  //TODO: compact the heap lol
	  block = block->next;
	}
}

void MEMMGR_UnrecoverableError(char *msg)
{
  printf(msg);

  while(true) {}
}

// Trampolines
CPTR _NewPtr(uint32_t requested_size, MEMMGR_MALLOC_FLAGS flags)
{
  return gMemoryManager.NewPtr(requested_size, flags);
}

int _DisposePtr(CPTR p)
{
  return gMemoryManager.DisposePtr(p);
}

/* Paging */


