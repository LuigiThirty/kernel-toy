#include "svga.h"

GD5446_Blitter *gd5446_blitter;
struct PCVideo::vbe_info_block_t VESA_CardInfo;
struct PCVideo::vbe_mode_info_structure_t VESAMode5F_info;
PCVideo::VideoCard installed_card;

PCVideo::VideoCard SVGA_DetectVideoCard()
{
    char buf[512];

    COM1.PutStr("SVGA_DetectVideoCard\r\n");

    // Is this a VESA-compliant video card?
    struct PCVideo::vbe_info_block_t *vbe_card_info = (struct PCVideo::vbe_info_block_t *)0x8C00;

    memcpy((void *)VM86_CODE_AREA, (void *)_VBE_GetControllerInfo, VM86_CODE_AREA_SIZE);
    _EnterV86(0x1200, 0xFFFF, 0x1000, 0x0000); // Jump to 0x1000:0000 (0x10000 linear)

    memcpy(&VESA_CardInfo, vbe_card_info, sizeof(struct PCVideo::vbe_info_block_t));

    sprintf(buf, "Signature: %c%c%c%c\r\n", vbe_card_info->VbeSignature[0], vbe_card_info->VbeSignature[1], vbe_card_info->VbeSignature[2],vbe_card_info->VbeSignature[3]);
    COM1.PutStr(buf);
    if(vbe_card_info->VbeSignature[0] == 'V' && vbe_card_info->VbeSignature[1] == 'E' && vbe_card_info->VbeSignature[2] == 'S' && vbe_card_info->VbeSignature[3] == 'A')
    {
        // This is a VESA card. What VBE version?
        sprintf(buf, "VBE version: %X\r\nVRAM %d KB\r\n", vbe_card_info->VbeVersion, (vbe_card_info->TotalMemory * 64));
        COM1.PutStr(buf);

        // Pointers are farptr, assume they're in $C0000
        sprintf(buf, "OEM name: %s\r\n", (char *)0xC0000 + ((uint32_t)vbe_card_info->OemStringPtr & 0xFFFF));
        COM1.PutStr(buf);

        if(vbe_card_info->VbeVersion >= 0x0200)
        {
            // Get VBE 2.0 info
            sprintf(buf, "Vendor name: %s\r\n", (char *)0xC0000 + ((uint32_t)vbe_card_info->VendorName & 0xFFFF));
            COM1.PutStr(buf);
            sprintf(buf, "Product name: %s\r\n", (char *)0xC0000 + ((uint32_t)vbe_card_info->ProductName & 0xFFFF));
            COM1.PutStr(buf);
        }
    }

    // OK, now we have our mode info.
    struct PCVideo::vbe_mode_info_structure_t *vbe_mode_info = (struct PCVideo::vbe_mode_info_structure_t *)0x8800;
    memcpy(&VESAMode5F_info, vbe_mode_info, sizeof(struct PCVideo::vbe_mode_info_structure_t));

    COM1.PutStr("SVGA: Populated the VBE mode info struct.\r\n");
    sprintf(buf, "SVGA mode 05Fh: %d x %d\r\n", VESAMode5F_info.height, VESAMode5F_info.width);
    COM1.PutStr(buf);

    memcpy((void *)VM86_CODE_AREA, (void *)_Cirrus_VGAType, VM86_CODE_AREA_SIZE);
    _EnterV86(0x1200, 0x0000, 0x1000, 0x0000); // Jump to 0x1000:0000 (0x10000 linear)

    sprintf(buf, "VGA type: $%02X\r\n", MMIO8(0x8BFF));
    COM1.PutStr(buf);

    // For now, assume this.
    return PCVideo::VideoCard::CIRRUS_GD54XX;
}

void SVGA_Init()
{
    uint8_t temp;

    installed_card = SVGA_DetectVideoCard();
    gd5446_blitter = (GD5446_Blitter *)0xB8000;

    COM1.PutStr("SVGA: Detected the video card, blitter struct is set.\r\n");

    memcpy((void *)VM86_CODE_AREA, (void *)_SVGA_Mode_640x480x256, VM86_CODE_AREA_SIZE);

    COM1.PutStr("SVGA: Entering V86 mode to set SVGA video mode.\r\n");
    _EnterV86(0x1200, 0xFFFF, 0x1000, 0x0000); // Jump to 0x1000:0000 (0x10000 linear)
    COM1.PutStr("SVGA: VESA mode 640x480x256.\r\n");

    // Enable linear addressing.
    _outb(0x3C4, 0x07);
    temp = _inb(0x3C5);
    temp |= 0x80;
    _outb(0x3C5, temp);

    COM1.PutStr("SVGA: Enabled linear framebuffer.\r\n");

    //printf("SR07: %02X\n", temp);

    // OK, now we have our mode info.
    struct PCVideo::vbe_mode_info_structure_t *vbe_mode_info = (struct PCVideo::vbe_mode_info_structure_t *)0x8800;
    memcpy(&VESAMode5F_info, vbe_mode_info, sizeof(struct PCVideo::vbe_mode_info_structure_t));

    COM1.PutStr("SVGA: Populated the VBE mode info struct.\r\n");

    svga_console_active = true;

    // Enable blitter memory-mapped I/O.
    _outb(0x3C4, 0x17);
    temp = _inb(0x3C5);
    temp |= 0x04;   // enable MMIO
    temp &= 0xBF;   // place it at B8000h
    _outb(0x3C5, temp);
    temp = 0;

    COM1.PutStr("SVGA: Enabled memory-mapped I/O, linear framebuffer, SVGA console\r\n");
}