#pragma once

#include "procyon.h"
#include <stdio.h>
#include "process.h"
#include "types.h"

#include "drivers/public/iorequest.h"
#include "drivers/public/floppydisk.h"

#include <stdio.h>
#include "low_level_io.h"
#include "procyon.h"

#ifdef __cplusplus
extern "C" {
#endif

// Standard IRQ is 6
// Standard DMA is 2

typedef struct fdd_controller_t
{
  uint8_t status_reg_a;       // R/- 0x3F0 SRA
  uint8_t status_reg_b;       // R/- 0x3F1 SRB
  uint8_t digital_output_reg; // R/W 0x3F2 DOR
  uint8_t tape_drive_reg;     // R/W 0x3F3 TDR
  uint8_t main_status_reg;    // R/- 0x3F4 MSR
  uint8_t datarate_select_reg;// -/W 0x3F4 DSR
  uint8_t data_fifo;          // R/W 0x3F5 
  uint8_t digital_input_reg;  // R/- 0x3F7 DIR
  uint8_t config_control_reg; // -/W 0x3F7 CCR
} FDDController;

#define FDC_SRA 0x3F0
#define FDC_SRB 0x3F1
#define FDC_DOR 0x3F2
#define FDC_TDR 0x3F3
#define FDC_MSR 0x3F4
#define FDC_DSR 0x3F4
#define FDC_FIFO 0x3F5
#define FDC_DIR 0x3F7
#define FDC_CCR 0x3F7

// DOR bitflags
#define DOR_MOTD  0x80 /* Start motor D */
#define DOR_MOTC  0x40 /* Start motor C */
#define DOR_MOTB  0x20 /* Start motor B */
#define DOR_MOTA  0x10 /* Start motor A */
#define DOR_IRQ   0x08 /* DMA and IRQ enabled */
#define DOR_RESET 0x04 /* 1 = controller enabled, 0 = controller reset */
#define DOR_DSEL1 0x02 /* Drive select bits */
#define DOR_DSEL2 0x01 /* 00 = A:, 01 = B: */

// MSR bitflags
#define MSR_RQM  0x80 /* FIFO ready? */
#define MSR_DIO  0x40 /* 1: FDC -> CPU */
                      /* 0: CPU -> FDC */
#define MSR_NDMA 0x20 /* 1: DMA mode */
		      /* 0: Polled mode */
#define MSR_CB   0x10 /* Busy? */
#define MSR_ACTD 0x08 /* Drive D in positioning mode */
#define MSR_ACTC 0x04 /* C */
#define MSR_ACTB 0x02 /* B */
#define MSR_ACTA 0x01 /* A */

enum FloppyDriveNum
{
  FDC_DRIVE0 = 0,
  FDC_DRIVE1 = 1,
  FDC_DRIVE2 = 2,
  FDC_DRIVE3 = 3
};

enum FloppyCommands
{
   FC_READ_TRACK =                 2,       // generates IRQ6
   FC_SPECIFY =                    3,       // * set drive parameters
   FC_SENSE_DRIVE_STATUS =         4,
   FC_WRITE_DATA =                 5,       // * write to the disk
   FC_READ_DATA =                  6,       // * read from the disk
   FC_RECALIBRATE =                7,       // * seek to cylinder 0
   FC_SENSE_INTERRUPT =            8,       // * ack IRQ6, get status of last command
   FC_WRITE_DELETED_DATA =         9,
   FC_READ_ID =                    10,	    // generates IRQ6
   FC_READ_DELETED_DATA =          12,
   FC_FORMAT_TRACK =               13,      // *
   FC_DUMPREG =                    14,
   FC_SEEK =                       15,      // * seek both heads to cylinder X
   FC_VERSION =                    16,	    // * used during initialization, once
   FC_SCAN_EQUAL =                 17,
   FC_PERPENDICULAR_MODE =         18,	    // * used during initialization, once, maybe
   FC_CONFIGURE =                  19,      // * set controller parameters
   FC_LOCK =                       20,      // * protect controller params from a reset
   FC_VERIFY =                     22,
   FC_SCAN_LOW_OR_EQUAL =          25,
   FC_SCAN_HIGH_OR_EQUAL =         29
};

// command option bits - OR with commands to modify them
#define FC_OPTION_MT 0x80 /* Multitrack mode */
#define FC_OPTION_MF 0x40 /* MFM - always on for read/write/format/verify */
#define FC_OPTION_SK 0x20 /* Skip mode - don't use this */

extern enum FloppyDriveNum currentlySelectedDrive;

void _FDC_IRQHandler();

void FDC_Reset();
void FDC_GetVersion();

bool FDC_WaitForFIFOReady();
void FDC_SendCommandByte(enum FloppyCommands command);
void FDC_WriteFIFO(uint8_t);

void FDC_MotorOn();
void FDC_MotorOff();

void FDC_ReadSector(uint32_t lba_sector, uint8_t *buffer);
extern volatile uint8_t irq_flag;

bool FDC_Calibrate();

void DRV_FDD_ProcessLoop();
void DRV_FDD_IORequestEntryPoint(IORequest_Floppy *request);

#ifdef __cplusplus
}
#endif