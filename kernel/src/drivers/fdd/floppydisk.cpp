#include "floppydisk.h"

#include "pic.h"
#include "serial.h"
#include "timer.h"

#define FLOPPY_144_SECTORS_PER_TRACK 18

enum FloppyDriveNum currentlySelectedDrive;

void lba_2_chs(uint32_t lba, uint16_t* cyl, uint16_t* head, uint16_t* sector)
{
    *cyl    = lba / (2 * FLOPPY_144_SECTORS_PER_TRACK);
    *head   = ((lba % (2 * FLOPPY_144_SECTORS_PER_TRACK)) / FLOPPY_144_SECTORS_PER_TRACK);
    *sector = ((lba % (2 * FLOPPY_144_SECTORS_PER_TRACK)) % FLOPPY_144_SECTORS_PER_TRACK + 1);
}

/* FIFO */
uint8_t FDC_ReadFIFO()
{
  auto ticks = PIT_GetTicks();
  auto timeout = ticks + PIT_TICKS_PER_SECOND;
  
  while(timeout > ticks)
  {
		if ((_inb(FDC_MSR) & 0x80) == 0x80)
			return _inb(FDC_FIFO);
  }

  COM1_PRINTF("FDC: FIFO read timed out\r\n");
  return 255;
}

void FDC_WriteFIFO(uint8_t val)
{
  auto ticks = PIT_GetTicks();
  auto timeout = ticks + PIT_TICKS_PER_SECOND;
  
  while(timeout > ticks)
  {
		if ((_inb(FDC_MSR) & 0x80) == 0x80)
    {
			_outb(FDC_FIFO, val);
      return;
    }
  }

  COM1_PRINTF("FDC: FIFO write timed out\r\n");  
}

/* DMA */
void FDC_DMAInit() {
 
	_outb(0x0a, 0x06);	//mask dma channel 2
	_outb(0xd8, 0xff);	//reset master flip-flop
	_outb(0x04, (FDD_SECTOR_AREA & 0xFF));     //address=0x1000 
	_outb(0x04, ((FDD_SECTOR_AREA & 0xFF00) >> 8));
	_outb(0xd8, 0xff);  //reset master flip-flop
	_outb(0x05, 0xff);  //count to 0x01ff (number of bytes in a 3.5" floppy disk sector)
	_outb(0x05, 0x01);
	_outb(0x81, 0);     //external page register = 0
	_outb(0x0a, 0x02);  //unmask dma channel 2
}

/****************************/

/* Interrupts */
void FDC_ResetINT6()
{
  irq_flag = false;
}

void FDC_WaitForINT6()
{
  while(irq_flag == false) {};
}

void FDC_SenseInterrupt(uint8_t *st0, uint8_t *cyl)
{
  FDC_SendCommandByte(FC_SENSE_INTERRUPT);
  *st0 = FDC_ReadFIFO();
  *cyl = FDC_ReadFIFO();
}

/**********************/

bool FDC_WaitForFIFOReady()
{
  // Waits for the MSR's high bit to be 1. This means that we're ready for another command.
  // Returns TRUE if the FDC accepted our command. Returns FALSE if the FDC timed out.

  int x = 1000; // TODO: precise timer
  while(x > 0)
  {
    if((_inb(FDC_MSR) & 0x80) == 0x80)
    {
      return 0;
    }
    else
    {
      x--;
    }
  }
  
  COM1_PRINTF("FDC timed out waiting for FIFO to become ready.\r\n");
  return 1;

}

volatile uint8_t irq_flag;

// Triggers on IRQ 6 (FDC).
void _FDC_IRQHandler()
{
  irq_flag = true;
  //printf("IRQ 6 handler!\n");
  PIC_SendEOI(6);
}

void FDC_Reset()
{
  irq_flag = false;

  COM1_PRINTF("FDC_Reset()\r\n");
  
  // Reset the floppy controller.
  //_outb(FDC_DOR, 0x00);
  //_outb(FDC_DOR, 0x0C);

  //while(irq_flag == false) {};

  FDC_Calibrate();

  // Specify DMA mode.
  COM1_PRINTF("FDC: Sending SPECIFY\r\n");
  FDC_SendCommandByte(FC_SPECIFY);
  FDC_WriteFIFO((8 << 4) | 0);
  FDC_WriteFIFO((5 << 1) | 0); // DMA mode

  // DMA mode
  COM1_PRINTF("FDC: Sending configuration\r\n");
  FDC_SendCommandByte(FC_CONFIGURE);
  FDC_WriteFIFO(0);
  FDC_WriteFIFO(0x70 | 8);
  FDC_WriteFIFO(0);

  FDC_Calibrate();

  COM1_PRINTF("FDC is reset.\r\n");
}

bool FDC_Seek(uint16_t lba)
{
  uint16_t cyl, head, sector;

  lba_2_chs(lba, &cyl, &head, &sector);

  // Seek to the correct cylinder/head.
  FDC_MotorOn();

  //COM1_PRINTF("FDC: Performing seek to track %d\r\n", cyl);
  FDC_ResetINT6();
  FDC_SendCommandByte(FC_SEEK);
  FDC_WriteFIFO(head << 2);  // First parameter byte: (head << 2) | selected_drive
  FDC_WriteFIFO((uint8_t)cyl);                                   // Second parameter byte: Cylinder number
  FDC_WaitForINT6();

  uint8_t st0, cylinder = 255;
  FDC_SenseInterrupt(&st0, &cylinder);

  if(st0 != 0x20)
  {
    COM1_PRINTF("FDC ERROR: Unexpected ST0 result byte. Should be $20 after a seek, was $%X\r\n", st0);
    return false;
  }

  FDC_MotorOff();

  //COM1_PRINTF("FDC: Floppy controller seeked to track %d\r\n", cylinder);

  return true;
}

void FDC_SendCommandByte(enum FloppyCommands command)
{
  // Send a command to the floppy drive FIFO register.
  _outb(FDC_FIFO, command);
}

uint8_t FDC_ReadResultByte()
{
  return _inb(FDC_FIFO);
}

void FDC_MotorOn()
{
  // Enables a drive motor.
  //COM1_PRINTF("FDC: Motor on\r\n");
  _outb(FDC_DOR, 0x1C);
}

void FDC_MotorOff()
{
  // Disables a drive motor.
  //COM1_PRINTF("FDC: Motor off\r\n");
  _outb(FDC_DOR, 0x0C);
}

void FDC_PrepareDMARead()
{
  _outb(0x0a, 0x06); //mask dma channel 2
	_outb(0x0b, 0x56); //single transfer, address increment, autoinit, read, channel 2
	_outb(0x0a, 0x02); //unmask dma channel 2
}

void FDC_ReadSector(uint32_t lba_sector, uint8_t *buffer)
{
  // Read a sector into a buffer. The sector number is given as an LBA sector number.
  uint16_t head, cyl, sector;

  lba_2_chs(lba_sector, &cyl, &head, &sector);
  //COM1_PRINTF("FDC: Requested LBA %d, CHS %d,%d,%d\r\n", lba_sector, cyl, head, sector);

  // Clear the DMA read buffer.
  for(int i=0;i<512;i++)
  {
    MMIO8(0x1000+i) = 0xAA;
  }

  FDC_DMAInit();
  FDC_PrepareDMARead();

  bool seek_success = FDC_Seek(lba_sector);
  if(!seek_success)
  {
    printf("FDC: Could not seek to sector %d.\n", lba_sector);
  }

  /*
  Read command = MT bit | MFM bit | 0x6
  or Write command = MT bit | MFM bit | 0x5
  First parameter byte = (head number << 2) | drive number (the drive number must match the currently selected drive!)
  Second parameter byte = cylinder number
  Third parameter byte = head number (yes, this is a repeat of the above value)
  Fourth parameter byte = starting sector number
  Fifth parameter byte = 2 (all floppy drives use 512bytes per sector)
  Sixth parameter byte = EOT (end of track, the last sector number on the track)
  Seventh parameter byte = 0x1b (GAP1 default size)
  Eighth parameter byte = 0xff (all floppy drives use 512bytes per sector)
  */  

  //COM1_PRINTF("FDC: Reading sector...\r\n");
  FDC_ResetINT6();
  FDC_SendCommandByte((FloppyCommands)(FC_OPTION_MT | FC_OPTION_MF | FC_READ_DATA));
  FDC_WriteFIFO(head << 2);
  FDC_WriteFIFO((uint8_t)cyl);
  FDC_WriteFIFO((uint8_t)head);
  FDC_WriteFIFO((uint8_t)sector);
  FDC_WriteFIFO(2);
  FDC_WriteFIFO(18);
  FDC_WriteFIFO(0x1B);
  FDC_WriteFIFO(0xFF);
  FDC_WaitForINT6();

  //COM1_PRINTF("FDC: Sector read complete.\r\n");
  _outb(0x0a, 0x06); //mask dma channel 2

  // for(int i=0; i<128; i++)
  // {
  //   // Swap endianness of the data we just read.
  //   uint32_t temp = MMIO32(0x1000+(i*4));

  //   MMIO8(0x1000 + (i*4) + 0) = (temp & 0xFF);
  //   MMIO8(0x1000 + (i*4) + 1) = (temp & 0xFF00) >> 8;
  //   MMIO8(0x1000 + (i*4) + 2) = (temp & 0xFF0000) >> 16;
  //   MMIO8(0x1000 + (i*4) + 3) = (temp & 0xFF000000) >> 24;
  // }

  memcpy(buffer, (void *)FDD_SECTOR_AREA, FDD_SECTOR_AREA_SIZE);

  //COM1_PRINTF("FDC: Reading result bytes\r\n");
  FDC_ReadResultByte();
  FDC_ReadResultByte();
  FDC_ReadResultByte();
  FDC_ReadResultByte();
  FDC_ReadResultByte();
  FDC_ReadResultByte();
  FDC_ReadResultByte();

  FDC_MotorOff();

}

void FDC_GetVersion()
{

  // Send version command.
  FDC_SendCommandByte(FC_VERSION);
  printf("Version byte: $%02X\n", FDC_ReadFIFO());
  
}

/* Driver-specific stuff */
void DRV_FDD_ProcessLoop()
{
  while(true)
  {
    gProcessManager.QuantumExpired(); // Don't do anything if we get called like a program.
  }
}

char debug_iocmd_fdd[64][32] =
{
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "IOCMD_FDD_READSECTOR",
    "IOCMD_FDD_?",
};

// Synchronous entry point.
void DRV_FDD_IORequestEntryPoint(IORequest_Floppy *request)
{
    //printf("FDD driver got a request: cmd %d (%s)\n", request->request.command, debug_iocmd_fdd[request->request.command]);
    uint16_t sector_count;

    switch(request->request.command)
    {
        case IOCMD_FDD_READSECTOR:
            sector_count = request->request.length / 512;
            for(int i=0; i<sector_count; i++)
            {
              FDC_ReadSector(request->sector_number + i, (uint8_t *)(request->request.data)+(512*i));
              request->request.actual = 512 * sector_count;
            }
            
            break;
    }
}

bool FDC_Calibrate()
{
	uint8_t st0, cyl = 255;

  COM1_PRINTF("FDC: Calibrating floppy drive\r\n");
 
	//! turn on the motor
  FDC_MotorOn();

  COM1_PRINTF("FDC: Calibrate: Motor on\r\n");
 
	for (int i = 0; i < 10; i++) {
 
		// Send command
    FDC_ResetINT6();
		FDC_SendCommandByte(FC_RECALIBRATE);
    FDC_WriteFIFO(0);
    printf("Waiting for IRQ\n");
    FDC_WaitForINT6();
		FDC_SenseInterrupt(&st0, &cyl);
 
		// Did we find cylinder 0? if so, we are done
		if (!cyl) {
      COM1_PRINTF("FDC: Calibration OK\r\n");
			FDC_MotorOff();
			return true;
		}
    else
    {
      COM1_PRINTF("FDC: Retrying calibration\r\n");
    }
    
	}
 
  COM1_PRINTF("FDC: Calibration failed\r\n");
	FDC_MotorOff();
	return false;
}