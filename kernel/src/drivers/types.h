#pragma once
#include "procyon.h"
#include "messages.h"

#include "drivers/public/iorequest.h"

typedef struct
{
    char name[16];
    DRIVER_ENTRYPOINT entryPoint;                   // The entry point used when the driver is switched to by the process manager.
    IOREQUEST_ENTRYPOINT synchronousEntryPoint;     // The entry point used for an immediate, synchronous request.
} DeviceDriverListEntry;
