#include "manager.h"

DeviceDriverListEntry deviceDriverList[64];
int deviceDriverCount;

void DRVMGR_InitializeDriverList()
{
    strcpy(deviceDriverList[0].name, "pc_keyboard");
    deviceDriverList[0].entryPoint = DRV_KBD_ProcessLoop;
    deviceDriverList[0].synchronousEntryPoint = DRV_KBD_IORequestEntryPoint;
    
    strcpy(deviceDriverList[1].name, "floppydisk");
    deviceDriverList[1].entryPoint = DRV_FDD_ProcessLoop;
    deviceDriverList[1].synchronousEntryPoint = (IOREQUEST_ENTRYPOINT)DRV_FDD_IORequestEntryPoint;

    deviceDriverCount = 2;
}

DeviceDriverListEntry *DRVMGR_FindDriver(char *driver_name)
{
    for(int i=0; i<deviceDriverCount; i++)
    {
        if(strcmp(deviceDriverList[i].name, driver_name) == 0)
        {
            return &deviceDriverList[i];
        }
    }

    return NULL;
}

void DRVMGR_DoIO(char *driver_name, IORequest *request)
{
    // Perform IORequest immediately.
    DeviceDriverListEntry *entry = DRVMGR_FindDriver(driver_name);
    if(entry != NULL)
        entry->synchronousEntryPoint(request);
    else
        printf("Fatal device error: Unable to find device %s", driver_name);
}