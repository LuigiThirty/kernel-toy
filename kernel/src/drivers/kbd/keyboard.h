#include "procyon.h"
#include <stdio.h>
#include "process.h"
#include "types.h"

#include "low_level_io.h"
#include "drivers/public/keyboard.h"
#include "drivers/public/iorequest.h"

void DRV_KBD_ProcessLoop();
void DRV_KBD_IORequestEntryPoint(IORequest *request);