#include "keyboard.h"
#include "types.h"
#include "process/message.h"

bool DRV_KB_KeyIsDownFlags[256];

uint8_t DRV_KB_GetKeypress()
{
  // Returns 0 if no keypress waiting. Returns a scancode otherwise.
  if((_inb(0x64) & 1) == false)
    return 0; // no keypress waiting

  uint8_t scancode = _inb(0x60); // get the scancode

  if(scancode & 0x80)
    {
      DRV_KB_KeyIsDownFlags[scancode & 0x7F] = false;
      return scancode;
    }
  else
    {
      if(DRV_KB_KeyIsDownFlags[scancode] == true)
	return 0; // already down - ignore
      else
	{
	  DRV_KB_KeyIsDownFlags[scancode] = true;
	  return scancode;
	}
    }
}

/* KBDUS means US Keyboard Layout. This is a scancode table
*  used to layout a standard US keyboard. I have left some
*  comments in to give you an idea of what key is what, even
*  though I set it's array index to 0. You can change that to
*  whatever you want using a macro, if you wish! */
unsigned char DRV_kbdus[128] =
{
    0,  27, '1', '2', '3', '4', '5', '6', '7', '8',	/* 9 */
  '9', '0', '-', '=', '\b',	/* Backspace */
  '\t',			/* Tab */
  'q', 'w', 'e', 'r',	/* 19 */
  't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\n',	/* Enter key */
    0,			/* 29   - Control */
  'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';',	/* 39 */
 '\'', '`',   0,		/* Left shift */
 '\\', 'z', 'x', 'c', 'v', 'b', 'n',			/* 49 */
  'm', ',', '.', '/',   0,				/* Right shift */
  '*',
    0,	/* Alt */
  ' ',	/* Space bar */
    0,	/* Caps lock */
    0,	/* 59 - F1 key ... > */
    0,   0,   0,   0,   0,   0,   0,   0,
    0,	/* < ... F10 */
    0,	/* 69 - Num lock*/
    0,	/* Scroll Lock */
    0,	/* Home key */
    0,	/* Up Arrow */
    0,	/* Page Up */
  '-',
    0,	/* Left Arrow */
    0,
    0,	/* Right Arrow */
  '+',
    0,	/* 79 - End key*/
    0,	/* Down Arrow */
    0,	/* Page Down */
    0,	/* Insert Key */
    0,	/* Delete Key */
    0,   0,   0,
    0,	/* F11 Key */
    0,	/* F12 Key */
    0,	/* All other keys are undefined */
};		

uint8_t DRV_KB_GetASCIIFromScancode(uint8_t scancode)
{
  // US keyboard layout
  uint8_t ascii = DRV_kbdus[scancode];

  // TODO: turn this into a table
  if(DRV_KB_KeyIsDownFlags[SC_shiftLeft] || DRV_KB_KeyIsDownFlags[SC_shiftRight])
    {
      if(ascii >= 0x61 && ascii <= 0x7A)
	      return ascii -= 0x20;
      else
      {
      switch(ascii) // if shift+key, return the shifted key
        {
        case 0x31:
          return 0x21;
        case 0x32:
          return 0x40;
        case 0x33:
          return 0x23;
        case 0x34:
          return 0x24;
        case 0x35:
          return 0x25;
        case 0x36:
          return 0x5E;
        case 0x37:
          return 0x26;
        case 0x38:
          return 0x2A;
        case 0x39:
          return 0x28;
        case 0x30:
          return 0x29;
        case 0x2D:
          return 0x5F;
        case 0x3D:
          return 0x2B;
        case ';':
          return ':';
        }
      }
    }
  
  return ascii;
}


void DRV_KBD_ProcessMessage(IPC_Message *msg)
{
  uint8_t scancode;

  if(msg->length != sizeof(IORequest))
  {
      printf("Message is not an IORequest!\n");
  }
  else
  {
    //printf("DRV_KBD_ProcessMessage\n");
    switch(((IORequest *)msg)->command)
    {
        case IOCMD_READ:
          // Read a scancode, write it to msg->data
          scancode = DRV_KB_GetKeypress();
          ((uint8_t *)((IORequest *)msg)->data)[0] = scancode;
          ((uint8_t *)((IORequest *)msg)->data)[1] = DRV_KB_GetASCIIFromScancode(scancode);
          ((IORequest *)msg)->actual = 2;
          return;
        case IOCMD_EXTENDED1:
        {
          IPC_DisposeMessage(msg);

          //printf("KB input\n");

          // Hardware interrupt: We got a keycode. Write it to the foreground process (should be the reply address)
          IPC_Message * fg_msg = IPC_CreateMessage(sizeof(PM_Notification), NULL);
          scancode = DRV_KB_GetKeypress();
          ((PM_Notification *)msg)->wParam = DRV_KB_GetASCIIFromScancode(scancode);
          // Forward the message to the foreground process.
          IPC_SendMessage(fg_msg, gProcessManager.FindMessagePortByPID(gProcessManager.GetForegroundProcess()));
          //printf("Sending keydown message to process %08X\n", gProcessManager.GetForegroundProcess());
        }

        default:
          // Keyboards can't do anything else
        return;
    }
  }
}

void DRV_KBD_IORequestEntryPoint(IORequest *request)
{
    DRV_KBD_ProcessMessage((IPC_Message *)request);
}

void DRV_KBD_ProcessLoop()
{
    //printf("Keyboard process loop\n");

    while(true)
    {
        IPC_Message *msg; 
        msg = IPC_GetMessage();

        //printf("keyboard got a message\n");

        // The keyboard driver got a message, process it if it's a command.
        DRV_KBD_ProcessMessage(msg);

    }
}
