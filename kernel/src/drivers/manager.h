#pragma once
/* The driver manager. Lets the kernel find drivers and their entry points. */

#include "procyon.h"
#include <stdio.h>
#include <string.h>

#include "drivers.h"
#include "messages.h"
#include "types.h"

#ifdef __cplusplus
extern "C" {
#endif

void DRVMGR_InitializeDriverList();
void DRVMGR_DoIO(char *driver_name, IORequest *request);
DeviceDriverListEntry *DRVMGR_FindDriver(char *driver_name);

extern DeviceDriverListEntry deviceDriverList[64];
extern int deviceDriverCount;


#ifdef __cplusplus
}
#endif