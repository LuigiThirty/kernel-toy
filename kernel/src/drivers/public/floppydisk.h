#pragma once

#include "procyon.h"
#include "drivers/public/iorequest.h"

#define IOCMD_FDD_READSECTOR IOCMD_EXTENDED1

typedef struct {
    IORequest request;
    uint32_t sector_number;
} IORequest_Floppy;
