// Public headers for keyboard driver.
#pragma once

#include "procyon.h"

typedef struct {
  uint8_t scancode;
  uint8_t ascii;
} CMD_KB_Response;
