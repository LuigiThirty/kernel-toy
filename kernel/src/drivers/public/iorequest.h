#pragma once

#include "procyon.h"
#include "messages.h"

typedef enum {
    IOCMD_INVALID,  // An uninitialized command that will be rejected.
    IOCMD_READ,     // Do whatever "read" is, using the ptr data as output.
    IOCMD_WRITE,    // Do whatever "write" is, using the ptr data as input.
    IOCMD_RESET,    // Reset the device to a known state.
    IOCMD_UPDATE,   // Flush all buffers, etc.
    IOCMD_STOP,     // Stop processing commands until a START arrives.
    IOCMD_START,    // Start processing commands again.
    IOCMD_EXTENDED, // Extended commands come after the rest of the commands. A device can support 255 commands.
                    // They are defined in the public header file for the device driver.
    IOCMD_EXTENDED1,
    IOCMD_EXTENDED2,
    IOCMD_EXTENDED3,
    IOCMD_EXTENDED4,
} IOCommand;

typedef struct
{
    IPC_Message message;        // The message node for passing to message ports.
    IOCommand command;          // The command for the driver to execute.
    CPTR data;                  // A data buffer for input or output.
    uint16_t imm16;             // A buffer for 16 bits of immediate data.
    uint16_t length;            // How much data do we expect to read or write?
    uint16_t actual;            // The actual length of data written or read.
} IORequest;

typedef void (*DRIVER_ENTRYPOINT)();
typedef void (*IOREQUEST_ENTRYPOINT)(IORequest *request);