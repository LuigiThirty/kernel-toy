#include "fileio.h"
#include "fat12.h"
#include "path.h"
#include "globals.h"
#include "serial.h"
#include "console.h"

DRIVE_ENTRY system_drive_table[26];
FD_ENTRY system_file_descriptor_table[256];

void FD_InitIO()
{
  FD_InitSystemDriveTable();
  FD_InitSystemFDTable();  
}

void FD_InitSystemDriveTable()
{
  for(int i=0; i<26; i++)
    {
      system_drive_table[i].type = DRIVE_TYPE_NONE;
      system_drive_table[i].index = -1;
    }

  // We booted from a floppy, so register drive A: as a floppy.
  FD_RegisterDrive(DRIVE_A, DRIVE_TYPE_FLOPPY, 0, DRIVE_FS_FAT12);
}

void FD_InitSystemFDTable()
{
  // Standard streams.
  system_file_descriptor_table[FD_STDIN].type = FILE_TYPE_STDIN;
  system_file_descriptor_table[FD_STDOUT].type = FILE_TYPE_STDOUT;
  system_file_descriptor_table[FD_STDERR].type = FILE_TYPE_STDERR;

  system_file_descriptor_table[FD_STDIN].mode = FILE_MODE_READ;
  system_file_descriptor_table[FD_STDOUT].mode = FILE_MODE_WRITE;
  system_file_descriptor_table[FD_STDERR].mode = FILE_MODE_WRITE;
  
  for(int i=3; i<255; i++)
    {
      system_file_descriptor_table[i].type = FILE_TYPE_NOT_IN_USE;
      system_file_descriptor_table[i].mode = FILE_MODE_INVALID;
    }
}

void FD_Close(FileDescriptor file)
{
  system_file_descriptor_table[file].type = FILE_TYPE_NOT_IN_USE;
  system_file_descriptor_table[file].mode = FILE_MODE_INVALID;
}

FileDescriptor FD_Open(const char *path)
{
  char *upper_path = (char *)malloc(strlen(path) + 1);
  strcpy(upper_path, path);
  
  // A path should start with a drive letter.
  for(size_t i=0; i<strlen(path); i++)
    {
      upper_path[i] = (char)toupper(path[i]);
    }

  int drive_index = PATH_FindDriveIndex(upper_path);
  if(drive_index == ERROR_PATH_IS_MALFORMED || drive_index == ERROR_PATH_HAS_INVALID_DRIVE_LETTER)
  {
    return 0;
  }
  else if(drive_index == ERROR_PATH_HAS_NO_DRIVE_LETTER)
  {
    drive_index = current_drive;
  }

  switch(system_drive_table[drive_index].type)
    {
      case DRIVE_TYPE_FLOPPY:
        printf("drive %c is a floppy drive\n", DRIVE_INDEX_TO_DRIVE_LETTER(drive_index));
        break;
      default: 
        printf("Don't know how to handle drive %c, this is going to end badly\n", DRIVE_INDEX_TO_DRIVE_LETTER(drive_index));
        break;
    }

  // Find the file on the correct drive, get its starting cluster,
  // and add it to the file descriptors table. Return the descriptor index.
  gFAT12Manager.UpdateBPB((DRIVE_LETTER)drive_index);
  FAT12::RootDirectoryEntry * entry = gFAT12Manager.GetDirectoryEntryWithFilename((DRIVE_LETTER)drive_index, upper_path);

  FileDescriptor fd_index = 0;
  for(int i=0; i<256; i++)
  {
    if(system_file_descriptor_table[i].type == FILE_TYPE_NOT_IN_USE)
    {
      fd_index = i;
      break;
    }
  }

  // TODO: Raise an error if we're out of file descriptors
  if(entry != NULL)
  {
    system_file_descriptor_table[fd_index].type = FILE_TYPE_FAT12;
    system_file_descriptor_table[fd_index].mode = FILE_MODE_READ; // TODO: accept r/w
    system_file_descriptor_table[fd_index].starting_cluster = entry->first_cluster;
    system_file_descriptor_table[fd_index].current_offset = 0;
    system_file_descriptor_table[fd_index].length = entry->size_bytes;
    gMemoryManager.DisposePtr(entry);
    return fd_index;
  }

  return 0;
}

void FD_RegisterDrive(DRIVE_LETTER letter, DRIVE_TYPE type, int index, DRIVE_FS filesystem)
{
  system_drive_table[letter].type = type;
  system_drive_table[letter].index = index;
  system_drive_table[letter].filesystem = filesystem;
}

uint32_t FD_ReadFromFloppy(FileDescriptor file, uint8_t *buffer, uint32_t length)
{
  uint16_t next_cluster = system_file_descriptor_table[file].starting_cluster;
  uint16_t file_cluster_count = gFAT12Manager.FileClustersRemaining(DRIVE_A, next_cluster);

  // How many sectors do we need to read?
  uint16_t sectors_to_read = (length / SECTOR_LENGTH) + 1;
  uint16_t sectors_successfully_read = 0;

  uint8_t sector_buffer[SECTOR_LENGTH];
  uint32_t read_length = 0;

  while(sectors_to_read)
  {
    COM1_PRINTF("FILEIO: %d floppy sectors remaining\r\n", sectors_to_read);
    gFAT12Manager.ReadCluster(sector_buffer, DRIVE_A, next_cluster);
    next_cluster = gFAT12Manager.FindNextClusterOfFile(DRIVE_A, next_cluster);
    
    if(length > SECTOR_LENGTH)
    {
      memcpy(buffer, sector_buffer, SECTOR_LENGTH);
      buffer = buffer+SECTOR_LENGTH;
      length = length-SECTOR_LENGTH;
      read_length += SECTOR_LENGTH;
    }
    else
    {
      // last sector
      memcpy(buffer, sector_buffer, length);
      read_length += length; 
    }

    sectors_to_read--;
    sectors_successfully_read++;
  }

  return read_length;
}

size_t FD_Read(FileDescriptor file, uint8_t *buffer, uint32_t length)
{
  printf("FD_Read: file descriptor %d, buffer is %08X, length is %d\n", file, buffer, length);

  uint32_t read_size = 0;

  switch(system_file_descriptor_table[file].type)
  {
    case FILE_TYPE_FAT12:
      read_size += FD_ReadFromFloppy(file, buffer, length);
      break;
    default:
      printf("File is on some other type of device we don't know how to FD_Read()\n");
      return 0;
  }

  system_file_descriptor_table[file].current_offset += read_size;
  return read_size;
}

size_t FD_Write(FileDescriptor file, const char *buffer, size_t length)
{
  //printf("FD_Write: file descriptor %d\n", file);

  switch(system_file_descriptor_table[file].type)
  {
    case FILE_TYPE_STDOUT:
      //printf("%s", buffer, strlen((char *)buffer));
      for(int i=0; i<length;i++)
      {
        Console::Putchar(buffer[i]);
      }
      return length; // console always writes
    default:
      printf("File is on some type of device we don't know how to FD_Write()\n");
      return 0;
  }
}

uint32_t FD_Length(FileDescriptor file)
{
  return system_file_descriptor_table[file].length;
}

size_t __SYS_write(FileDescriptor fd, const char *buffer, size_t length)
{
  asm("sti");	// safe to interrupt
  return FD_Write(fd, buffer, length);
}

size_t __SYS_read(FileDescriptor fd, void *buffer, size_t length)
{
  asm("sti");	// safe to interrupt
  return FD_Read(fd, (uint8_t *)buffer, length);
}