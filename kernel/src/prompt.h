#pragma once

#include <stdio.h>
#include <string.h>
#include "procyon.h"
#include "keyboard.h"
#include "vga/vgatext.h"
#include "messages.h"

#include "process.h"
#include "drivers/types.h"
#include "drivers/manager.h"

#include "memmgr.h"
#include "commands/commands.h"

#ifdef __cplusplus
extern "C" {
#endif

extern char command_line[256];
extern int command_line_position;

void CMD_PromptLoop();
void CMD_Process(char *command_line);
void CMD_ResetPrompt(char *command_line);

#ifdef __cplusplus
}
#endif
