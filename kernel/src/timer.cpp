#include "timer.h"

#include <stdio.h>

volatile uint32_t pit_ticks = 0;

void _PIT_IRQHandler()
{
    pit_ticks++;
    PIC_SendEOI(0);
}

uint32_t PIT_GetTicks()
{
    return pit_ticks;
}

void PIT_WaitTicks(uint16_t period)
{
    // One tick is 1/18 of a second.
    
    uint32_t ticks = PIT_GetTicks();
    uint32_t timeout = ticks+period;

    while(ticks < timeout)
    {
        ticks = PIT_GetTicks();
    }
}