#pragma once

#include "procyon.h"

typedef uint32_t signal_flags_t;

typedef enum {
	      SIG_ABORT = 1,      // Task should clean up and exit.
	      SIG_MESSAGE = 2,    // Task has received a message.
	      SIG_KILL = 4,       // Task must exit immediately.
} Signal;

// 64-bit structure describing a task's signal state.
typedef struct {
  uint32_t received; // Flags for signals that a task has recveived.
  uint32_t waiting;  // Flags for signals that a task is waiting for.
} PM_SignalBlock;
