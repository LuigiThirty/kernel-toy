#include "fat12.h"
#include "process.h"
#include "drivers/public/floppydisk.h"
#include "drivers/manager.h"
#include "serial.h"
#include "path.h"

FAT12Manager gFAT12Manager;

// Initialize the FAT12 driver...
void FAT12Manager::Initialize()
{
  for(int i=0; i<26; i++) drive_bpb[i] = NULL;
  
  for(int i=0;i<FILES_LIMIT;i++)
    {
      file_descriptor_table[i].id = i;
      file_descriptor_table[i].flags = FAT12::FILE_FLAG_UNUSED;
      memset(file_descriptor_table[i].path, 0x00, 128);
    }
}

void FAT12Manager::UpdateBPB(DRIVE_LETTER drive)
{
  uint8_t data_buffer[512];
  Pid kernel_pid = gProcessManager.FindProcessByName("floppydisk")->pid;
  IPC_MessagePort *kernel_port = gProcessManager.FindMessagePortByPID(kernel_pid);
  IORequest_Floppy *fdd_request = (IORequest_Floppy *)IPC_CreateMessage(sizeof(IORequest_Floppy), kernel_port);
  fdd_request->request.command = IOCMD_FDD_READSECTOR;
  fdd_request->request.data = data_buffer;
  fdd_request->request.length = SECTOR_LENGTH;
  fdd_request->sector_number = 0;
  DRVMGR_DoIO("floppydisk", (IORequest *)fdd_request);

  ReadBPB(drive, data_buffer);
}

void FAT12Manager::ReadBPB(DRIVE_LETTER drive, uint8_t *data)
{
  //printf("Updating BPB with data at %08X\n", data);
  if(drive_bpb[drive] != 0)
  {
    gMemoryManager.DisposePtr(drive_bpb[drive]); // dispose the old BPB if one exists
    drive_bpb[drive] = 0;
  }
  FAT12::BPB *bpb = (FAT12::BPB *)gMemoryManager.NewPtr(sizeof(FAT12::BPB), H_SYSHEAP);
  drive_bpb[drive] = bpb;

  // TODO: MMIO16 must change by endianness of architecture
  bpb->bytes_per_sector = MMIO16(data+11);
  bpb->sectors_per_cluster = MMIO8(data+13);
  bpb->reserved_sectors = MMIO16(data+14);
  bpb->fat_copies = MMIO8(data+16);
  bpb->root_directory_entries = MMIO16(data+17);
  bpb->total_sectors = MMIO16(data+19);
  bpb->media_descriptor_type = MMIO8(data+21);
  bpb->sectors_per_fat = MMIO16(data+22);
  bpb->sectors_per_track = MMIO16(data+24);
  bpb->heads = MMIO16(data+26);
  bpb->hidden_sectors = MMIO16(data+28);
  if(MMIO8(data+37) == 0x2A)
    { 
      bpb->extended_fields_present = 1;
      strncpy(bpb->volume_label, (char *)(data+43), 11);
      strncpy(bpb->filesystem_type, (char *)(data+54), 8);
    }
  else
    bpb->extended_fields_present = 0;
}

void FAT12Manager::PrintBPBInfo(DRIVE_LETTER letter)
{
  FAT12::BPB *bpb = drive_bpb[letter];

  printf("Dumping BPB of drive %x\n\n", letter);
  printf("Volume label is \"%s\"\n", bpb->volume_label);
  printf("BPB reports filesystem type is \"%s\"\n", bpb->filesystem_type);
  
  printf("Bytes per sector: %d\n", bpb->bytes_per_sector);
  printf("Media descriptor type is $%02X\n", bpb->media_descriptor_type);
  printf("Volume has %d sectors - calculated size is %d bytes\n", bpb->total_sectors, bpb->bytes_per_sector * bpb->total_sectors);
}

uint32_t FAT12Manager::GetRootDirectoryOffset(DRIVE_LETTER letter)
{
  FAT12::BPB *bpb = drive_bpb[letter];
  uint32_t first_fat_offset = bpb->reserved_sectors * bpb->bytes_per_sector;
  return first_fat_offset + (bpb->fat_copies * (bpb->sectors_per_fat * bpb->bytes_per_sector));
}

uint16_t FAT12Manager::BytesPerSector(DRIVE_LETTER letter) { return drive_bpb[letter]->bytes_per_sector; }

bool FAT12Manager::ReadCluster(uint8_t *buffer, DRIVE_LETTER letter, int cluster_number)
{
  uint32_t data_area = GetRootDirectoryOffset(letter) + (drive_bpb[letter]->root_directory_entries * 32);

  uint32_t offset = data_area + ((cluster_number-2) * drive_bpb[letter]->sectors_per_cluster) * drive_bpb[letter]->bytes_per_sector;

  Pid kernel_pid = gProcessManager.FindProcessByName("floppydisk")->pid;
  IPC_MessagePort *kernel_port = gProcessManager.FindMessagePortByPID(kernel_pid);
  IORequest_Floppy *fdd_request = (IORequest_Floppy *)IPC_CreateMessage(sizeof(IORequest_Floppy), kernel_port);
  fdd_request->request.command = IOCMD_FDD_READSECTOR;
  fdd_request->request.data = buffer;
  fdd_request->request.length = 512;
  fdd_request->sector_number = offset / drive_bpb[letter]->bytes_per_sector;
  DRVMGR_DoIO("floppydisk", (IORequest *)fdd_request);

  return 0;
}

uint16_t FAT12Manager::FindNextClusterOfFile(DRIVE_LETTER letter, uint16_t cluster_number)
{
  // Takes an input cluster number. The output is the next cluster in the chain.

  // Get the FAT.
  uint8_t fat[drive_bpb[letter]->sectors_per_fat * drive_bpb[letter]->bytes_per_sector];
  uint32_t fat_offset = drive_bpb[letter]->reserved_sectors * drive_bpb[letter]->bytes_per_sector;
  
  Pid kernel_pid = gProcessManager.FindProcessByName("floppydisk")->pid;
  IPC_MessagePort *kernel_port = gProcessManager.FindMessagePortByPID(kernel_pid);
  IORequest_Floppy *fdd_request = (IORequest_Floppy *)IPC_CreateMessage(sizeof(IORequest_Floppy), kernel_port);
  fdd_request->request.command = IOCMD_FDD_READSECTOR;
  fdd_request->request.data = fat;
  fdd_request->request.length = drive_bpb[letter]->bytes_per_sector * drive_bpb[letter]->sectors_per_fat;
  fdd_request->sector_number = fat_offset / drive_bpb[letter]->bytes_per_sector;
  DRVMGR_DoIO("floppydisk", (IORequest *)fdd_request);
  
  //COM1_PRINTF("FAT12: Looking for cluster index %d (%x) in FAT\r\n", cluster_number, cluster_number);

  // Treat two cluster IDs as a 24-bit number.
  // Which 24-bit number is our cluster in?
  uint16_t cluster24 = cluster_number / 2;

  if(cluster_number & 0x01) // Odd cluster number - our cluster is the beginning 3 nybbles
    return (((uint16_t)fat[cluster24*3 + 1] & 0xF0) >> 4) | ((uint16_t)fat[cluster24*3 + 2] << 4) | ((uint16_t)(fat[cluster24*3 + 2] & 0xF0) << 8);
  else  // Even cluster number - our cluster is the ending 3 nybbles
    return (((uint16_t)fat[cluster24*3] + (((uint16_t)fat[cluster24*3 + 1] & 0x0F) << 8)));
}

uint16_t FAT12Manager::FileClustersRemaining(DRIVE_LETTER letter, uint16_t starting_cluster)
{
  // Takes a cluster number. Returns the number of remaining clusters in the file.
  uint16_t clusters_remaining = 0;
  uint16_t current_cluster = starting_cluster;

  while(current_cluster <= 0xFF0)
  {
    current_cluster = FindNextClusterOfFile(letter, current_cluster);
    clusters_remaining++;
  }

  return clusters_remaining;
}

void FAT12Manager::GetDirectoryEntryWithIndex(FAT12::RootDirectoryEntry *entry, int index, uint8_t *root_directory_data)
{
  uint8_t *data_pointer = (root_directory_data + (index*32));
  
  memcpy(&(entry->filename), data_pointer, 8);
  entry->filename[8] = 0;

  memcpy(&(entry->extension), data_pointer+8, 3);
  entry->extension[3] = 0;

  // Fill in the rest of the info, not just attributes.
  memcpy(&(entry->attributes), data_pointer+11, 21);
  memcpy(&(entry->size_bytes), data_pointer+28, 4);
}

void FAT12Manager::GetRootDirectorySector(uint8_t *buffer, DRIVE_LETTER letter)
{
  uint32_t rootDirectoryOffset = gFAT12Manager.GetRootDirectoryOffset(letter);
  uint16_t bytesPerSector = gFAT12Manager.BytesPerSector(letter);

  // Find the directory on the disk and read it into memory.
  for(int i=0; i<512; i++) ((uint8_t *)buffer)[i] = 0xAA;

  auto floppy_driver = gProcessManager.FindProcessByName("floppydisk");
  IPC_MessagePort *floppy_port = gProcessManager.FindMessagePortByPID(floppy_driver->pid);
  IORequest_Floppy *fdd_request = (IORequest_Floppy *)IPC_CreateMessage(sizeof(IORequest_Floppy), floppy_port);
  fdd_request->request.command = IOCMD_FDD_READSECTOR;
  fdd_request->request.data = buffer;
  fdd_request->request.length = 512;
  fdd_request->sector_number = rootDirectoryOffset/bytesPerSector;
  DRVMGR_DoIO("floppydisk", (IORequest *)fdd_request);
  return;
}

void FAT12Manager::GetDirectorySector(uint8_t *buffer, DRIVE_LETTER letter, char *path)
{
  char directory[256];
  PATH_GetDirectory(directory, path, 256);

  // printf("Path to use: %s\n", directory);
  gFAT12Manager.UpdateBPB(letter);

  // We need this regardless
  uint8_t root_directory_data[512];
  GetRootDirectorySector(root_directory_data, letter);

  size_t directory_length = strlen(directory);
  
  if(directory_length == 1)
  {
    // The path is the root directory
    printf("Using root directory\n");
    memcpy(buffer, root_directory_data, 512);
    return;
  }

  size_t  previous_delimiter = 0,
          this_delimiter = 0;

  for(int c=1; c<directory_length; c++)
  {
    if(directory[c] == '\\')
    {
      char subdirectory[256];
      memset(subdirectory, 0, 256);

      previous_delimiter = this_delimiter;
      this_delimiter = c;

      int cur = 0;
      for(int i=previous_delimiter+1; i<this_delimiter; i++)
      {
        subdirectory[cur++] = directory[i];
      }

      printf("Searching for subdirectory with name %s\n", subdirectory);

      FAT12::RootDirectoryEntry entry;
      int index = 0;

      while(true)
      {
        gFAT12Manager.GetDirectoryEntryWithIndex(&entry, index++, root_directory_data);

        if(entry.filename[0] == 0x00) break;          // Out of directory entries.
        else if(entry.filename[0] == 0xE5) continue;  // Deleted entry.
        else if(entry.attributes & FAT12::FILE_ATTR_LFN) continue;    // LFN entry.
        
        char filename[10];
        memcpy(filename, entry.filename, 8);
        for(int i=0; i<8; i++) { if(filename[i] == 0x20) filename[i] = 0; }
        
        if(entry.attributes & FAT12::FILE_ATTR_DIRECTORY)
        {
          if(strcmp(filename, subdirectory) == 0)
          {
            printf("Found subdirectory %s at cluster ID %03X\n", filename, entry.first_cluster);
            gFAT12Manager.ReadCluster(buffer, DRIVE_A, entry.first_cluster);
            return;
          }
        }
      }
    }
  }  
}

FAT12::RootDirectoryEntry * FAT12Manager::GetDirectoryEntryWithFilename(DRIVE_LETTER letter, char *path)
{
  // TODO: Clean all this up. It's a mess.

  // Figure out the subdirectory from the path.

  char filename[16];
  PATH_FindFileName(filename, path);
  FAT12::RootDirectoryEntry *entry = (FAT12::RootDirectoryEntry *)gMemoryManager.NewPtr(sizeof(FAT12::RootDirectoryEntry), H_SYSHEAP);
  
  UpdateBPB(DRIVE_A);

  uint32_t rootDirectoryOffset = GetRootDirectoryOffset(letter);

  // Find the root directory on the disk and read it into memory.
  uint8_t rootDirectoryBuffer[512];
  for(int i=0; i<512; i++) rootDirectoryBuffer[i] = 0xAA;

  Pid kernel_pid = gProcessManager.FindProcessByName("floppydisk")->pid;
  IPC_MessagePort *kernel_port = gProcessManager.FindMessagePortByPID(kernel_pid);
  IORequest_Floppy *fdd_request = (IORequest_Floppy *)IPC_CreateMessage(sizeof(IORequest_Floppy), kernel_port);
  fdd_request->request.command = IOCMD_FDD_READSECTOR;
  fdd_request->request.data = rootDirectoryBuffer;
  fdd_request->request.length = 512;
  fdd_request->sector_number = rootDirectoryOffset / SECTOR_LENGTH;
  DRVMGR_DoIO("floppydisk", (IORequest *)fdd_request);

  bool more = true;
  uint16_t index = 0;
  uint8_t complete_filename[13];

  while(more)
  {
    memset(complete_filename, 0, 13);
    GetDirectoryEntryWithIndex(entry, index++, rootDirectoryBuffer);

    if(entry->filename[0] == 0x00) break;         // Out of directory entries.
    else if(entry->filename[0] == 0xE5) continue; // Deleted entry.
    else if(entry->attributes & 0x0F) continue;   // LFN entry, ignore it.

    int filename_chars;
    for(filename_chars=0; filename_chars<8; filename_chars++)
    {
      if(entry->filename[filename_chars] != 0x20)
        complete_filename[filename_chars] = entry->filename[filename_chars];
      else break;
    }

    complete_filename[filename_chars] = '.';

    int extension_chars;
    for(extension_chars=0; extension_chars<3; extension_chars++)
    {
      if(entry->extension[extension_chars] != 0x20)
        complete_filename[filename_chars+1+extension_chars] = entry->extension[extension_chars];
      else break;
    }

    printf("Matching %s to %s\n", path, complete_filename);
    if(strcmp(path, (CString)complete_filename) == 0) return entry;

  }

  // No match.
  printf("no match\n");
  gMemoryManager.DisposePtr(entry);
  return NULL;
}