In computing, the Executable and Linkable Format (ELF, formerly named 
Extensible Linking Format), is a common standard file format for executable 
files, object code, shared libraries, and core dumps. First published in the
specification for the application binary interface (ABI) of the Unix 
operating system version named System V Release 4 (SVR4),[2] and later in
the Tool Interface Standard,[1] it was quickly accepted among different vendors
of Unix systems. In 1999, it was chosen as the standard binary file format for
Unix and Unix-like systems on x86 processors by the 86open project.