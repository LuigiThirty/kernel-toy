;;;;;;;;;;;;;;;;;;;;;;
; String routines
;

SECTION .text

;; Hex number to print is in EAX.
;; It is output using regular INT 10h stuff.
;; Parameters:
;; EAX: 32-bit hex number to print
;; BX: Base to print number in.
PrintNumberWithBase:
	PUSHA

	MOV	SI, strBuffer+64
	MOV	CX, 0
	
.convert:
	ADD	CX, 1
	XOR	DX,DX 		; clear for division
	DIV	BX		; divide by 16
	ADD	DL, $30		; make an ASCII char
	CMP	DL, $39		; hex digit?
	JBE	.store		; no
	ADD	DL, $41-$30-10	; adjust it to be printable

.store:
	DEC	SI		; move back
	MOV	[SI], DL	; store
	AND	AX, AX		; BX/16 == 0?
	JNZ	.convert	; if no, keep going

	; Print the string.
.setSI:
	MOV	SI, strBuffer+64 ; calculate the SI
	SUB	SI, CX		; subtract number of digits
	CALL	PrintString	; and print
	
	POPA
	RET
	
PrintNL:
	PUSHA
	MOV	SI, strCRLF
	CALL	PrintString
	POPA
	RET
	
PrintString:
        ;; String address is [DS:SI]

.next:
        MOV     AL, [SI]        ; Get next character, store in AL

        INC     SI              ; Increment SI
        OR      AL,AL           ; Is AL a NUL?
        JZ      .done           ; yes, done
        CALL    PrintChar       ; no, print AL
        JMP     .next           ; continue

.done:
        RET

PrintChar:
	PUSHA

	MOV     AH, 0Eh
        MOV     BH, 0
        MOV     BL, 07h

        INT     10h

	POPA
        RET

strBuffer resb 64
