#include "unistd.h"
#include "syscallid.h"

size_t fread(unsigned char fd, void *buffer, unsigned long length)
{
	asm("movl $0x02, %%eax\n\t"	// SYS_read
		"movl %0, %%ebx\n\t"    // fd
		"movl %1, %%ecx\n\t"    // buffer
		"movl %2, %%edx\n\t"    // length
		"int $0x30"
		:                 				    /* output */
		: "r"((uint32_t)fd), "r"((void *)buffer), "r"(length) /* input */
		: "%eax", "%ebx", "%ecx", "%edx"    /* clobbered */
		);

    return syscall_return_block->retval;
}

void getcwd(char *buf, size_t size)
{
	asm("movl %0, %%eax\n\t"		// SYS_read
		"movl %1, %%ebx\n\t"    	// fd
		"movl %2, %%ecx\n\t"    	// buffer
		"int $0x30"
		:                 			// output
		: "r"(SYSCALL_getcwd) , "r"((char *)buf), "r"(size) // input
		: "%eax", "%ebx", "%ecx"	// clobbered
		);
}

uint32_t dlsym(char *libname, char *fnname)
{
	asm("movl %0, %%eax\n\t"		// SYS_dlsym
		"movl %1, %%ebx\n\t"    	// libname
		"movl %2, %%ecx\n\t"    	// fnname
		"int $0x30"
		:                 			// output
		: "r"(SYSCALL_dlsym) , "r"((char *)libname), "r"((char *)fnname) // input
		: "%eax", "%ebx", "%ecx"	// clobbered
		);

	return syscall_return_block->retval;
}