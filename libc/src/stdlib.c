#include "stdlib.h"
#include "stdio.h"

#include "procyon.h"

typedef enum {
				  H_CURHEAP = 0x01,
				  H_SYSHEAP = 0x02,
				  H_CLEAR   = 0x04,
				  H_XMS     = 0x08,
} MEMMGR_MALLOC_FLAGS;

extern CPTR _NewPtr(uint32_t requested_size, MEMMGR_MALLOC_FLAGS flags);
extern void _DisposePtr(void *ptr);

void swap(char *a, char *b)
{
  char c = *a;
  *a = *b;
  *b = c;
}

void reverse(char *str, int length)
{
  int start = 0;
  int end = length - 1;
  while(start < end)
	{
	  swap((str+start), (str+end));
	  start++;
	  end--;
	}
}

int atoi(char *str)
{
  int result = 0;
  int sign = 1;
  int i=0;

  if(str[0] == '-')
	{
	  sign = -1;
	  i++;
	}

  for(; str[i] != '\0'; ++i)
	result = result*10 + str[i] - '0';

  return result*sign;
}

char *itoa(int num, char *str, int base)
{
  int i=0;
  int isNegative = false;

  if(num == 0)
	{
	  str[i++] = '0';
	  str[i] = '\0';
	  return str;
	}

  if(num < 0 && base == 10)
	{
	  isNegative = 1;
	  num = -num;
	}

  while(num != 0)
	{
	  int rem = num % base;
	  str[i++] = (rem > 9)? (rem-10) + 'a' : rem + '0';
	  num = num/base;
	}

  if(isNegative)
	str[i++] = '-';

  str[i] = '\0';

  reverse(str, i);

  return str;
}

/* reroute malloc to NewPtr */
void *malloc(size_t size)
{
  return _NewPtr(size, H_SYSHEAP);
}

void free(void *block)
{
	_DisposePtr(block);
}