#include "ctype.h"

int islower(int c)
{
  if(BETWEEN(c, 0x61, 0x7A))
    return 1;
  else
    return 0;
}

int isupper(int c)
{
  if(BETWEEN(c, 0x41, 0x5A))
    return 1;
  else
    return 0;
  
}

int toupper(int c)
{
  if(BETWEEN(c, 0x61, 0x7A))
    return (c & 0xDF);
  else
    return c;
}

int tolower(int c)
{
  if(BETWEEN(c, 0x41, 0x5A))
    return (c | 0x20);
  else
    return c;
}
