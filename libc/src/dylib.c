#include "dylib.h"

volatile SYSCALL_RETURN_BLOCK *syscall_return_block = (SYSCALL_RETURN_BLOCK *)SYSCALL_AREA;
volatile SYSCALL_PARAMS_BLOCK *syscall_params_block = (SYSCALL_PARAMS_BLOCK *)SYSCALL_PARAMS_AREA;

Handle dlopen(const char *library_name)
{
	syscall_params_block->param1 = library_name;

	asm("mov $0x03, %%eax\n\t"	// SYS_dlopen
		"mov %0, %%ebx\n\t"    	// library_name
		"int $0x30"
		:                 	/* output */
		: "r"(library_name) /* input */
		: "%eax", "%ebx"    /* clobbered */
		);

    return (Handle)syscall_return_block->retval;
}