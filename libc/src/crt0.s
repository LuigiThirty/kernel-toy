[BITS 32]
[CPU 386]

global _start

extern main
extern _init

_start:
	mov	ebp, 0
	push ebp
	mov	ebp, esp
	call _init

	call main
	pop ebp
	ret