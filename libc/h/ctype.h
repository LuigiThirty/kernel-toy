#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#define BETWEEN(N, LO, HI) ( N >= LO && N <= HI )

// All functions take int, return int.

int islower(int c);
int isupper(int c);

int tolower(int c);
int toupper(int c);

#ifdef __cplusplus
}
#endif