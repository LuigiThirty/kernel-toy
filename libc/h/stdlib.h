#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#include "procyon.h"

#ifdef __cplusplus
extern "C" {
#endif

void swap(char *a, char *b);
void reverse(char *str, int length);

int atoi(char *str);
char *itoa(int num, char *str, int base);

void *malloc(size_t size);
void free(void *p);

size_t fread(unsigned char fd, void *buffer, unsigned long length);

#ifdef __cplusplus
}
#endif