#pragma once

#include "procyon.h"

typedef struct {
    char name[32];
    void *symbol_table;
} LibraryInfo;

#ifdef __cplusplus
extern "C" {
#endif

Handle dlopen(const char *library_name);

#ifdef __cplusplus
}
#endif
