#pragma once

#include "procyon.h"

#ifdef __cplusplus
extern "C" {
#endif
size_t fread(unsigned char fd, void *buffer, unsigned long length);
void getcwd(char *buf, size_t size);
uint32_t dlsym(char *libname, char *fnname);
#ifdef __cplusplus
}
#endif