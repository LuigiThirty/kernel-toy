#ifndef MMIO_H
#define MMIO_H

#define MMIO32(x)   (*(volatile unsigned long *)(x))
#define MMIO16(x)   (*(volatile unsigned short *)(x))
#define MMIO8(x)    (*(volatile unsigned char *)(x))

#endif

#include <stdio.h>
#include <unistd.h>
#include <kerlib/kernel.h>

void resolver()
{
	// TODO: dlopen to do this automatically
	TestFunc	= dlsym("kernel", "TestFunc");
	NewPtr 		= dlsym("kernel", "NewPtr");
	DisposePtr 	= dlsym("kernel", "DisposePtr");
}

int main()
{
	resolver();

	char cwd[64];

	printf("Hello World from A:\\TEST.ELF!\n");

	getcwd(cwd, 64);
	printf("getcwd returned %s\n", cwd);

  	TestFunc = dlsym("kernel", "TestFunc");
  	TestFunc();

	void *testPtr = NewPtr(sizeof(void *), H_SYSHEAP);
	printf("testPtr is %08X\n", testPtr);
	DisposePtr(testPtr);	

	MMIO32(0x333333) = 0x12345678;
	return 0;
}

void _init() {
	main();
}
